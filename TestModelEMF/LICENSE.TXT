------------------------------------

LICENSE.TXT:

Simula Simplified Academic License

The source, object, or executable code accompanying this license text is hereafter called "The Program".

The Program is copyrighted by Simula Research Laboratory AS (http://www.simula.no), hereafter called "Copyright Holder".

The Copyright Holder grants you, the licensee (hereafter “you") a non-exclusive license to use, copy, modify and distribute The Program for as long as you fulfil the following requirements:

- You must be a member of a non-commercial academic institution, such as a university. This license agreement expires as soon as you are no longer a member of such an institution.

- You must include an appropriate citation and acknowledgment of the author(s) of The Program in every published work that has relied on the Program or its output.

- If you distribute the Program or a work based on the Program, then these additional requirements must be fulfilled:

    - You must accompany the distribution with a verbatim copy of this license text and the complete machine-readable source code of the Program, including the modifications you have made to the source code. If including the source code is not feasible, then you must include a hyperlink to such source code and the source code must be publicly accessible at no cost.

    - Those parts of the whole work that are clearly derived works of The Program must be distributed under this license. Independently created parts can be distributed under terms of your choice.

If you do not comply with this agreement, then you are not allowed to use, copy, modify or distribute The Program.

The Copyright Holder provides The Program "as is" without warranty of any kind, either expressed or implied, including, but not limited to, the implied warranties of merchantability and fitness for a particular purpose. The entire risk as to the quality and performance of the program is with you. Should the program prove defective, you assume the cost of all necessary servicing, repair, or correction.

In no event will the Copyright Holder, or any other party who may modify and/or redistribute the program as permitted above, be liable to you for any damages arising out of the use or inability to use the program (including but not limited to loss of data or data being rendered inaccurate or losses sustained by you or third parties or a failure of the program to operate with any other programs), even if the Copyright Holder or other party has been advised of the possibility of such damages.

------------------------------------

COMMERCIAL_LICENSE.TXT:

UncerTest is copyrighted by Simula Research Laboratory AS (http://www.simula.no).

If you are interested in obtaining a commercial license for UncerText, please fill in this webform: https://goo.gl/4gB9gz

If the form cannot be accessed, send an email to ipr@simula.no with your inquiry.

------------------------------------