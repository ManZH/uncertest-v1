/*******************************************************************************
 * Copyright (c) 2016, 2017 Simula Research Laboratory. 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Simula license 
 * which accompanies this distribution, and is available at LICENSE.TXT
 *  
 *  Contributors:
 *        Man Zhang
 *******************************************************************************/
/**
 */
package no.simula.se.testmodel.TestModel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>UTest Case Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link no.simula.se.testmodel.TestModel.UTestCaseElement#getMeasuredValue <em>Measured Value</em>}</li>
 * </ul>
 *
 * @see no.simula.se.testmodel.TestModel.UTestModelPackage#getUTestCaseElement()
 * @model abstract="true"
 * @generated
 */
public interface UTestCaseElement extends EObject {
	/**
	 * Returns the value of the '<em><b>Measured Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Measured Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Measured Value</em>' attribute.
	 * @see #setMeasuredValue(double)
	 * @see no.simula.se.testmodel.TestModel.UTestModelPackage#getUTestCaseElement_MeasuredValue()
	 * @model
	 * @generated
	 */
	double getMeasuredValue();

	/**
	 * Sets the value of the '{@link no.simula.se.testmodel.TestModel.UTestCaseElement#getMeasuredValue <em>Measured Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Measured Value</em>' attribute.
	 * @see #getMeasuredValue()
	 * @generated
	 */
	void setMeasuredValue(double value);

} // UTestCaseElement
