/*******************************************************************************
 * Copyright (c) 2016, 2017 Simula Research Laboratory. 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Simula license 
 * which accompanies this distribution, and is available at LICENSE.TXT
 *  
 *  Contributors:
 *        Man Zhang
 *******************************************************************************/
/**
 */
package no.simula.se.testmodel.TestModel.impl;

import java.util.Collection;

import no.simula.se.testmodel.TestModel.UTestCase;
import no.simula.se.testmodel.TestModel.UTestModelPackage;
import no.simula.se.testmodel.TestModel.UTestParallel;
import no.simula.se.testmodel.TestModel.UTestPath;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EDataTypeEList;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>UTest Parallel</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link no.simula.se.testmodel.TestModel.impl.UTestParallelImpl#getUinfo <em>Uinfo</em>}</li>
 *   <li>{@link no.simula.se.testmodel.TestModel.impl.UTestParallelImpl#getLength <em>Length</em>}</li>
 *   <li>{@link no.simula.se.testmodel.TestModel.impl.UTestParallelImpl#getParalles <em>Paralles</em>}</li>
 *   <li>{@link no.simula.se.testmodel.TestModel.impl.UTestParallelImpl#getSize <em>Size</em>}</li>
 * </ul>
 *
 * @generated
 */
public class UTestParallelImpl extends UTestCaseElementImpl implements UTestParallel {
	/**
	 * The cached value of the '{@link #getUinfo() <em>Uinfo</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUinfo()
	 * @generated
	 * @ordered
	 */
	protected EList<Double> uinfo;

	/**
	 * The default value of the '{@link #getLength() <em>Length</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLength()
	 * @generated
	 * @ordered
	 */
	protected static final int LENGTH_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getLength() <em>Length</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLength()
	 * @generated
	 * @ordered
	 */
	protected int length = LENGTH_EDEFAULT;

	/**
	 * The cached value of the '{@link #getParalles() <em>Paralles</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParalles()
	 * @generated
	 * @ordered
	 */
	protected EList<UTestPath> paralles;

	/**
	 * The default value of the '{@link #getSize() <em>Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSize()
	 * @generated
	 * @ordered
	 */
	protected static final int SIZE_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getSize() <em>Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSize()
	 * @generated
	 * @ordered
	 */
	protected int size = SIZE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UTestParallelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return UTestModelPackage.Literals.UTEST_PARALLEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Double> getUinfo() {
		if (uinfo == null) {
			uinfo = new EDataTypeEList<Double>(Double.class, this, UTestModelPackage.UTEST_PARALLEL__UINFO);
		}
		return uinfo;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getLength() {
		return length;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLength(int newLength) {
		int oldLength = length;
		length = newLength;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UTestModelPackage.UTEST_PARALLEL__LENGTH, oldLength, length));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<UTestPath> getParalles() {
		if (paralles == null) {
			paralles = new EObjectContainmentEList<UTestPath>(UTestPath.class, this, UTestModelPackage.UTEST_PARALLEL__PARALLES);
		}
		return paralles;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getSize() {
		return size;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSize(int newSize) {
		int oldSize = size;
		size = newSize;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UTestModelPackage.UTEST_PARALLEL__SIZE, oldSize, size));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case UTestModelPackage.UTEST_PARALLEL__PARALLES:
				return ((InternalEList<?>)getParalles()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case UTestModelPackage.UTEST_PARALLEL__UINFO:
				return getUinfo();
			case UTestModelPackage.UTEST_PARALLEL__LENGTH:
				return getLength();
			case UTestModelPackage.UTEST_PARALLEL__PARALLES:
				return getParalles();
			case UTestModelPackage.UTEST_PARALLEL__SIZE:
				return getSize();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case UTestModelPackage.UTEST_PARALLEL__UINFO:
				getUinfo().clear();
				getUinfo().addAll((Collection<? extends Double>)newValue);
				return;
			case UTestModelPackage.UTEST_PARALLEL__LENGTH:
				setLength((Integer)newValue);
				return;
			case UTestModelPackage.UTEST_PARALLEL__PARALLES:
				getParalles().clear();
				getParalles().addAll((Collection<? extends UTestPath>)newValue);
				return;
			case UTestModelPackage.UTEST_PARALLEL__SIZE:
				setSize((Integer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case UTestModelPackage.UTEST_PARALLEL__UINFO:
				getUinfo().clear();
				return;
			case UTestModelPackage.UTEST_PARALLEL__LENGTH:
				setLength(LENGTH_EDEFAULT);
				return;
			case UTestModelPackage.UTEST_PARALLEL__PARALLES:
				getParalles().clear();
				return;
			case UTestModelPackage.UTEST_PARALLEL__SIZE:
				setSize(SIZE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case UTestModelPackage.UTEST_PARALLEL__UINFO:
				return uinfo != null && !uinfo.isEmpty();
			case UTestModelPackage.UTEST_PARALLEL__LENGTH:
				return length != LENGTH_EDEFAULT;
			case UTestModelPackage.UTEST_PARALLEL__PARALLES:
				return paralles != null && !paralles.isEmpty();
			case UTestModelPackage.UTEST_PARALLEL__SIZE:
				return size != SIZE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == UTestCase.class) {
			switch (derivedFeatureID) {
				case UTestModelPackage.UTEST_PARALLEL__UINFO: return UTestModelPackage.UTEST_CASE__UINFO;
				case UTestModelPackage.UTEST_PARALLEL__LENGTH: return UTestModelPackage.UTEST_CASE__LENGTH;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == UTestCase.class) {
			switch (baseFeatureID) {
				case UTestModelPackage.UTEST_CASE__UINFO: return UTestModelPackage.UTEST_PARALLEL__UINFO;
				case UTestModelPackage.UTEST_CASE__LENGTH: return UTestModelPackage.UTEST_PARALLEL__LENGTH;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (uinfo: ");
		result.append(uinfo);
		result.append(", length: ");
		result.append(length);
		result.append(", size: ");
		result.append(size);
		result.append(')');
		return result.toString();
	}

} //UTestParallelImpl
