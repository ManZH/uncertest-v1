/*******************************************************************************
 * Copyright (c) 2016, 2017 Simula Research Laboratory. 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Simula license 
 * which accompanies this distribution, and is available at LICENSE.TXT
 *  
 *  Contributors:
 *        Man Zhang
 *******************************************************************************/
/**
 */
package no.simula.se.testmodel.TestModel.impl;

import no.simula.se.testmodel.TestModel.UTestAction;
import no.simula.se.testmodel.TestModel.UTestModelPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>UTest Action</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link no.simula.se.testmodel.TestModel.impl.UTestActionImpl#getAttachedObject <em>Attached Object</em>}</li>
 *   <li>{@link no.simula.se.testmodel.TestModel.impl.UTestActionImpl#getInstanceoName <em>Instanceo Name</em>}</li>
 * </ul>
 *
 * @generated
 */
public class UTestActionImpl extends UTestCaseElementImpl implements UTestAction {
	/**
	 * The cached value of the '{@link #getAttachedObject() <em>Attached Object</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAttachedObject()
	 * @generated
	 * @ordered
	 */
	protected EObject attachedObject;

	/**
	 * The default value of the '{@link #getInstanceoName() <em>Instanceo Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInstanceoName()
	 * @generated
	 * @ordered
	 */
	protected static final String INSTANCEO_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getInstanceoName() <em>Instanceo Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInstanceoName()
	 * @generated
	 * @ordered
	 */
	protected String instanceoName = INSTANCEO_NAME_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UTestActionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return UTestModelPackage.Literals.UTEST_ACTION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EObject getAttachedObject() {
		if (attachedObject != null && attachedObject.eIsProxy()) {
			InternalEObject oldAttachedObject = (InternalEObject)attachedObject;
			attachedObject = eResolveProxy(oldAttachedObject);
			if (attachedObject != oldAttachedObject) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, UTestModelPackage.UTEST_ACTION__ATTACHED_OBJECT, oldAttachedObject, attachedObject));
			}
		}
		return attachedObject;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EObject basicGetAttachedObject() {
		return attachedObject;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAttachedObject(EObject newAttachedObject) {
		EObject oldAttachedObject = attachedObject;
		attachedObject = newAttachedObject;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UTestModelPackage.UTEST_ACTION__ATTACHED_OBJECT, oldAttachedObject, attachedObject));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getInstanceoName() {
		return instanceoName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInstanceoName(String newInstanceoName) {
		String oldInstanceoName = instanceoName;
		instanceoName = newInstanceoName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UTestModelPackage.UTEST_ACTION__INSTANCEO_NAME, oldInstanceoName, instanceoName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case UTestModelPackage.UTEST_ACTION__ATTACHED_OBJECT:
				if (resolve) return getAttachedObject();
				return basicGetAttachedObject();
			case UTestModelPackage.UTEST_ACTION__INSTANCEO_NAME:
				return getInstanceoName();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case UTestModelPackage.UTEST_ACTION__ATTACHED_OBJECT:
				setAttachedObject((EObject)newValue);
				return;
			case UTestModelPackage.UTEST_ACTION__INSTANCEO_NAME:
				setInstanceoName((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case UTestModelPackage.UTEST_ACTION__ATTACHED_OBJECT:
				setAttachedObject((EObject)null);
				return;
			case UTestModelPackage.UTEST_ACTION__INSTANCEO_NAME:
				setInstanceoName(INSTANCEO_NAME_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case UTestModelPackage.UTEST_ACTION__ATTACHED_OBJECT:
				return attachedObject != null;
			case UTestModelPackage.UTEST_ACTION__INSTANCEO_NAME:
				return INSTANCEO_NAME_EDEFAULT == null ? instanceoName != null : !INSTANCEO_NAME_EDEFAULT.equals(instanceoName);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (instanceoName: ");
		result.append(instanceoName);
		result.append(')');
		return result.toString();
	}

} //UTestActionImpl
