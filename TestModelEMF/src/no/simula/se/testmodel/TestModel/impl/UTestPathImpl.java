/*******************************************************************************
 * Copyright (c) 2016, 2017 Simula Research Laboratory. 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Simula license 
 * which accompanies this distribution, and is available at LICENSE.TXT
 *  
 *  Contributors:
 *        Man Zhang
 *******************************************************************************/
/**
 */
package no.simula.se.testmodel.TestModel.impl;

import java.util.Collection;

import no.simula.se.testmodel.TestModel.UTestCase;
import no.simula.se.testmodel.TestModel.UTestCaseElement;
import no.simula.se.testmodel.TestModel.UTestModelPackage;
import no.simula.se.testmodel.TestModel.UTestPath;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EDataTypeEList;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>UTest Path</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link no.simula.se.testmodel.TestModel.impl.UTestPathImpl#getUinfo <em>Uinfo</em>}</li>
 *   <li>{@link no.simula.se.testmodel.TestModel.impl.UTestPathImpl#getLength <em>Length</em>}</li>
 *   <li>{@link no.simula.se.testmodel.TestModel.impl.UTestPathImpl#getSequence <em>Sequence</em>}</li>
 *   <li>{@link no.simula.se.testmodel.TestModel.impl.UTestPathImpl#getMeasuredValues <em>Measured Values</em>}</li>
 *   <li>{@link no.simula.se.testmodel.TestModel.impl.UTestPathImpl#getSize <em>Size</em>}</li>
 * </ul>
 *
 * @generated
 */
public class UTestPathImpl extends UTestCaseElementImpl implements UTestPath {
	/**
	 * The cached value of the '{@link #getUinfo() <em>Uinfo</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUinfo()
	 * @generated
	 * @ordered
	 */
	protected EList<Double> uinfo;

	/**
	 * The default value of the '{@link #getLength() <em>Length</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLength()
	 * @generated
	 * @ordered
	 */
	protected static final int LENGTH_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getLength() <em>Length</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLength()
	 * @generated
	 * @ordered
	 */
	protected int length = LENGTH_EDEFAULT;

	/**
	 * The cached value of the '{@link #getSequence() <em>Sequence</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSequence()
	 * @generated
	 * @ordered
	 */
	protected EList<UTestCaseElement> sequence;

	/**
	 * The cached value of the '{@link #getMeasuredValues() <em>Measured Values</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMeasuredValues()
	 * @generated
	 * @ordered
	 */
	protected EList<Double> measuredValues;

	/**
	 * The default value of the '{@link #getSize() <em>Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSize()
	 * @generated
	 * @ordered
	 */
	protected static final int SIZE_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getSize() <em>Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSize()
	 * @generated
	 * @ordered
	 */
	protected int size = SIZE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UTestPathImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return UTestModelPackage.Literals.UTEST_PATH;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Double> getUinfo() {
		if (uinfo == null) {
			uinfo = new EDataTypeEList<Double>(Double.class, this, UTestModelPackage.UTEST_PATH__UINFO);
		}
		return uinfo;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getLength() {
		return length;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLength(int newLength) {
		int oldLength = length;
		length = newLength;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UTestModelPackage.UTEST_PATH__LENGTH, oldLength, length));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<UTestCaseElement> getSequence() {
		if (sequence == null) {
			sequence = new EObjectContainmentEList<UTestCaseElement>(UTestCaseElement.class, this, UTestModelPackage.UTEST_PATH__SEQUENCE);
		}
		return sequence;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Double> getMeasuredValues() {
		if (measuredValues == null) {
			measuredValues = new EDataTypeEList<Double>(Double.class, this, UTestModelPackage.UTEST_PATH__MEASURED_VALUES);
		}
		return measuredValues;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getSize() {
		return size;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSize(int newSize) {
		int oldSize = size;
		size = newSize;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UTestModelPackage.UTEST_PATH__SIZE, oldSize, size));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case UTestModelPackage.UTEST_PATH__SEQUENCE:
				return ((InternalEList<?>)getSequence()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case UTestModelPackage.UTEST_PATH__UINFO:
				return getUinfo();
			case UTestModelPackage.UTEST_PATH__LENGTH:
				return getLength();
			case UTestModelPackage.UTEST_PATH__SEQUENCE:
				return getSequence();
			case UTestModelPackage.UTEST_PATH__MEASURED_VALUES:
				return getMeasuredValues();
			case UTestModelPackage.UTEST_PATH__SIZE:
				return getSize();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case UTestModelPackage.UTEST_PATH__UINFO:
				getUinfo().clear();
				getUinfo().addAll((Collection<? extends Double>)newValue);
				return;
			case UTestModelPackage.UTEST_PATH__LENGTH:
				setLength((Integer)newValue);
				return;
			case UTestModelPackage.UTEST_PATH__SEQUENCE:
				getSequence().clear();
				getSequence().addAll((Collection<? extends UTestCaseElement>)newValue);
				return;
			case UTestModelPackage.UTEST_PATH__MEASURED_VALUES:
				getMeasuredValues().clear();
				getMeasuredValues().addAll((Collection<? extends Double>)newValue);
				return;
			case UTestModelPackage.UTEST_PATH__SIZE:
				setSize((Integer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case UTestModelPackage.UTEST_PATH__UINFO:
				getUinfo().clear();
				return;
			case UTestModelPackage.UTEST_PATH__LENGTH:
				setLength(LENGTH_EDEFAULT);
				return;
			case UTestModelPackage.UTEST_PATH__SEQUENCE:
				getSequence().clear();
				return;
			case UTestModelPackage.UTEST_PATH__MEASURED_VALUES:
				getMeasuredValues().clear();
				return;
			case UTestModelPackage.UTEST_PATH__SIZE:
				setSize(SIZE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case UTestModelPackage.UTEST_PATH__UINFO:
				return uinfo != null && !uinfo.isEmpty();
			case UTestModelPackage.UTEST_PATH__LENGTH:
				return length != LENGTH_EDEFAULT;
			case UTestModelPackage.UTEST_PATH__SEQUENCE:
				return sequence != null && !sequence.isEmpty();
			case UTestModelPackage.UTEST_PATH__MEASURED_VALUES:
				return measuredValues != null && !measuredValues.isEmpty();
			case UTestModelPackage.UTEST_PATH__SIZE:
				return size != SIZE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == UTestCase.class) {
			switch (derivedFeatureID) {
				case UTestModelPackage.UTEST_PATH__UINFO: return UTestModelPackage.UTEST_CASE__UINFO;
				case UTestModelPackage.UTEST_PATH__LENGTH: return UTestModelPackage.UTEST_CASE__LENGTH;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == UTestCase.class) {
			switch (baseFeatureID) {
				case UTestModelPackage.UTEST_CASE__UINFO: return UTestModelPackage.UTEST_PATH__UINFO;
				case UTestModelPackage.UTEST_CASE__LENGTH: return UTestModelPackage.UTEST_PATH__LENGTH;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (uinfo: ");
		result.append(uinfo);
		result.append(", length: ");
		result.append(length);
		result.append(", measuredValues: ");
		result.append(measuredValues);
		result.append(", size: ");
		result.append(size);
		result.append(')');
		return result.toString();
	}

} //UTestPathImpl
