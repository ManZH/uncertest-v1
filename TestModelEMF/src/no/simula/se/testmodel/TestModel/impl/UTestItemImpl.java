/*******************************************************************************
 * Copyright (c) 2016, 2017 Simula Research Laboratory. 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Simula license 
 * which accompanies this distribution, and is available at LICENSE.TXT
 *  
 *  Contributors:
 *        Man Zhang
 *******************************************************************************/
/**
 */
package no.simula.se.testmodel.TestModel.impl;

import java.util.Collection;

import no.simula.se.testmodel.TestModel.UTestItem;
import no.simula.se.testmodel.TestModel.UTestModelPackage;
import no.simula.se.testmodel.TestModel.UTestSet;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>UTest Item</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link no.simula.se.testmodel.TestModel.impl.UTestItemImpl#getUtestset <em>Utestset</em>}</li>
 *   <li>{@link no.simula.se.testmodel.TestModel.impl.UTestItemImpl#getAttachedObject <em>Attached Object</em>}</li>
 * </ul>
 *
 * @generated
 */
public class UTestItemImpl extends MinimalEObjectImpl.Container implements UTestItem {
	/**
	 * The cached value of the '{@link #getUtestset() <em>Utestset</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUtestset()
	 * @generated
	 * @ordered
	 */
	protected EList<UTestSet> utestset;

	/**
	 * The cached value of the '{@link #getAttachedObject() <em>Attached Object</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAttachedObject()
	 * @generated
	 * @ordered
	 */
	protected EObject attachedObject;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UTestItemImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return UTestModelPackage.Literals.UTEST_ITEM;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<UTestSet> getUtestset() {
		if (utestset == null) {
			utestset = new EObjectContainmentEList<UTestSet>(UTestSet.class, this, UTestModelPackage.UTEST_ITEM__UTESTSET);
		}
		return utestset;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EObject getAttachedObject() {
		if (attachedObject != null && attachedObject.eIsProxy()) {
			InternalEObject oldAttachedObject = (InternalEObject)attachedObject;
			attachedObject = eResolveProxy(oldAttachedObject);
			if (attachedObject != oldAttachedObject) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, UTestModelPackage.UTEST_ITEM__ATTACHED_OBJECT, oldAttachedObject, attachedObject));
			}
		}
		return attachedObject;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EObject basicGetAttachedObject() {
		return attachedObject;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAttachedObject(EObject newAttachedObject) {
		EObject oldAttachedObject = attachedObject;
		attachedObject = newAttachedObject;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UTestModelPackage.UTEST_ITEM__ATTACHED_OBJECT, oldAttachedObject, attachedObject));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case UTestModelPackage.UTEST_ITEM__UTESTSET:
				return ((InternalEList<?>)getUtestset()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case UTestModelPackage.UTEST_ITEM__UTESTSET:
				return getUtestset();
			case UTestModelPackage.UTEST_ITEM__ATTACHED_OBJECT:
				if (resolve) return getAttachedObject();
				return basicGetAttachedObject();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case UTestModelPackage.UTEST_ITEM__UTESTSET:
				getUtestset().clear();
				getUtestset().addAll((Collection<? extends UTestSet>)newValue);
				return;
			case UTestModelPackage.UTEST_ITEM__ATTACHED_OBJECT:
				setAttachedObject((EObject)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case UTestModelPackage.UTEST_ITEM__UTESTSET:
				getUtestset().clear();
				return;
			case UTestModelPackage.UTEST_ITEM__ATTACHED_OBJECT:
				setAttachedObject((EObject)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case UTestModelPackage.UTEST_ITEM__UTESTSET:
				return utestset != null && !utestset.isEmpty();
			case UTestModelPackage.UTEST_ITEM__ATTACHED_OBJECT:
				return attachedObject != null;
		}
		return super.eIsSet(featureID);
	}

} //UTestItemImpl
