/*******************************************************************************
 * Copyright (c) 2016, 2017 Simula Research Laboratory. 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Simula license 
 * which accompanies this distribution, and is available at LICENSE.TXT
 *  
 *  Contributors:
 *        Man Zhang
 *******************************************************************************/
/**
 */
package no.simula.se.testmodel.TestModel.impl;

import no.simula.se.testmodel.TestModel.UMSpaceA;
import no.simula.se.testmodel.TestModel.UTestAction;
import no.simula.se.testmodel.TestModel.UTestCase;
import no.simula.se.testmodel.TestModel.UTestCaseElement;
import no.simula.se.testmodel.TestModel.UTestConfiguration;
import no.simula.se.testmodel.TestModel.UTestItem;
import no.simula.se.testmodel.TestModel.UTestModel;
import no.simula.se.testmodel.TestModel.UTestModelFactory;
import no.simula.se.testmodel.TestModel.UTestModelPackage;
import no.simula.se.testmodel.TestModel.UTestParallel;
import no.simula.se.testmodel.TestModel.UTestPath;
import no.simula.se.testmodel.TestModel.UTestSet;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class UTestModelPackageImpl extends EPackageImpl implements UTestModelPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass uTestSetEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass uTestPathEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass uTestActionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass uTestCaseEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass uTestCaseElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass uTestParallelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass uTestItemEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass umSpaceAEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass uTestModelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass uTestConfigurationEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see no.simula.se.testmodel.TestModel.UTestModelPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private UTestModelPackageImpl() {
		super(eNS_URI, UTestModelFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link UTestModelPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static UTestModelPackage init() {
		if (isInited) return (UTestModelPackage)EPackage.Registry.INSTANCE.getEPackage(UTestModelPackage.eNS_URI);

		// Obtain or create and register package
		UTestModelPackageImpl theUTestModelPackage = (UTestModelPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof UTestModelPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new UTestModelPackageImpl());

		isInited = true;

		// Create package meta-data objects
		theUTestModelPackage.createPackageContents();

		// Initialize created meta-data
		theUTestModelPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theUTestModelPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(UTestModelPackage.eNS_URI, theUTestModelPackage);
		return theUTestModelPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getUTestSet() {
		return uTestSetEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUTestSet_Utestcase() {
		return (EReference)uTestSetEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUTestSet_AttachedObject() {
		return (EReference)uTestSetEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUTestSet_Umspacea() {
		return (EReference)uTestSetEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getUTestPath() {
		return uTestPathEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUTestPath_Sequence() {
		return (EReference)uTestPathEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getUTestPath_MeasuredValues() {
		return (EAttribute)uTestPathEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getUTestPath_Size() {
		return (EAttribute)uTestPathEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getUTestAction() {
		return uTestActionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUTestAction_AttachedObject() {
		return (EReference)uTestActionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getUTestAction_InstanceoName() {
		return (EAttribute)uTestActionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getUTestCase() {
		return uTestCaseEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getUTestCase_Uinfo() {
		return (EAttribute)uTestCaseEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getUTestCase_Length() {
		return (EAttribute)uTestCaseEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getUTestCaseElement() {
		return uTestCaseElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getUTestCaseElement_MeasuredValue() {
		return (EAttribute)uTestCaseElementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getUTestParallel() {
		return uTestParallelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUTestParallel_Paralles() {
		return (EReference)uTestParallelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getUTestParallel_Size() {
		return (EAttribute)uTestParallelEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getUTestItem() {
		return uTestItemEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUTestItem_Utestset() {
		return (EReference)uTestItemEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUTestItem_AttachedObject() {
		return (EReference)uTestItemEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getUMSpaceA() {
		return umSpaceAEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUMSpaceA_Source() {
		return (EReference)umSpaceAEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUMSpaceA_Transitions() {
		return (EReference)umSpaceAEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUMSpaceA_Ums() {
		return (EReference)umSpaceAEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getUTestModel() {
		return uTestModelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUTestModel_Utestitem() {
		return (EReference)uTestModelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUTestModel_Utestconfiguration() {
		return (EReference)uTestModelEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUTestModel_AttachedObject() {
		return (EReference)uTestModelEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getUTestConfiguration() {
		return uTestConfigurationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUTestConfiguration_AttachedObject() {
		return (EReference)uTestConfigurationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UTestModelFactory getUTestModelFactory() {
		return (UTestModelFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		uTestSetEClass = createEClass(UTEST_SET);
		createEReference(uTestSetEClass, UTEST_SET__UTESTCASE);
		createEReference(uTestSetEClass, UTEST_SET__ATTACHED_OBJECT);
		createEReference(uTestSetEClass, UTEST_SET__UMSPACEA);

		uTestPathEClass = createEClass(UTEST_PATH);
		createEReference(uTestPathEClass, UTEST_PATH__SEQUENCE);
		createEAttribute(uTestPathEClass, UTEST_PATH__MEASURED_VALUES);
		createEAttribute(uTestPathEClass, UTEST_PATH__SIZE);

		uTestActionEClass = createEClass(UTEST_ACTION);
		createEReference(uTestActionEClass, UTEST_ACTION__ATTACHED_OBJECT);
		createEAttribute(uTestActionEClass, UTEST_ACTION__INSTANCEO_NAME);

		uTestCaseEClass = createEClass(UTEST_CASE);
		createEAttribute(uTestCaseEClass, UTEST_CASE__UINFO);
		createEAttribute(uTestCaseEClass, UTEST_CASE__LENGTH);

		uTestCaseElementEClass = createEClass(UTEST_CASE_ELEMENT);
		createEAttribute(uTestCaseElementEClass, UTEST_CASE_ELEMENT__MEASURED_VALUE);

		uTestParallelEClass = createEClass(UTEST_PARALLEL);
		createEReference(uTestParallelEClass, UTEST_PARALLEL__PARALLES);
		createEAttribute(uTestParallelEClass, UTEST_PARALLEL__SIZE);

		uTestItemEClass = createEClass(UTEST_ITEM);
		createEReference(uTestItemEClass, UTEST_ITEM__UTESTSET);
		createEReference(uTestItemEClass, UTEST_ITEM__ATTACHED_OBJECT);

		umSpaceAEClass = createEClass(UM_SPACE_A);
		createEReference(umSpaceAEClass, UM_SPACE_A__SOURCE);
		createEReference(umSpaceAEClass, UM_SPACE_A__TRANSITIONS);
		createEReference(umSpaceAEClass, UM_SPACE_A__UMS);

		uTestModelEClass = createEClass(UTEST_MODEL);
		createEReference(uTestModelEClass, UTEST_MODEL__UTESTITEM);
		createEReference(uTestModelEClass, UTEST_MODEL__UTESTCONFIGURATION);
		createEReference(uTestModelEClass, UTEST_MODEL__ATTACHED_OBJECT);

		uTestConfigurationEClass = createEClass(UTEST_CONFIGURATION);
		createEReference(uTestConfigurationEClass, UTEST_CONFIGURATION__ATTACHED_OBJECT);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		uTestPathEClass.getESuperTypes().add(this.getUTestCaseElement());
		uTestPathEClass.getESuperTypes().add(this.getUTestCase());
		uTestActionEClass.getESuperTypes().add(this.getUTestCaseElement());
		uTestParallelEClass.getESuperTypes().add(this.getUTestCaseElement());
		uTestParallelEClass.getESuperTypes().add(this.getUTestCase());

		// Initialize classes, features, and operations; add parameters
		initEClass(uTestSetEClass, UTestSet.class, "UTestSet", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getUTestSet_Utestcase(), this.getUTestCase(), null, "utestcase", null, 0, -1, UTestSet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getUTestSet_AttachedObject(), ecorePackage.getEObject(), null, "attachedObject", null, 0, 1, UTestSet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getUTestSet_Umspacea(), this.getUMSpaceA(), null, "umspacea", null, 0, -1, UTestSet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(uTestPathEClass, UTestPath.class, "UTestPath", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getUTestPath_Sequence(), this.getUTestCaseElement(), null, "sequence", null, 0, -1, UTestPath.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getUTestPath_MeasuredValues(), ecorePackage.getEDouble(), "measuredValues", null, 0, -1, UTestPath.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getUTestPath_Size(), ecorePackage.getEInt(), "size", null, 0, 1, UTestPath.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(uTestActionEClass, UTestAction.class, "UTestAction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getUTestAction_AttachedObject(), ecorePackage.getEObject(), null, "attachedObject", null, 0, 1, UTestAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getUTestAction_InstanceoName(), ecorePackage.getEString(), "instanceoName", null, 0, 1, UTestAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(uTestCaseEClass, UTestCase.class, "UTestCase", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getUTestCase_Uinfo(), ecorePackage.getEDouble(), "uinfo", null, 0, -1, UTestCase.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getUTestCase_Length(), ecorePackage.getEInt(), "length", null, 0, 1, UTestCase.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(uTestCaseElementEClass, UTestCaseElement.class, "UTestCaseElement", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getUTestCaseElement_MeasuredValue(), ecorePackage.getEDouble(), "measuredValue", null, 0, 1, UTestCaseElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(uTestParallelEClass, UTestParallel.class, "UTestParallel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getUTestParallel_Paralles(), this.getUTestPath(), null, "paralles", null, 2, -1, UTestParallel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getUTestParallel_Size(), ecorePackage.getEInt(), "size", null, 0, 1, UTestParallel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(uTestItemEClass, UTestItem.class, "UTestItem", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getUTestItem_Utestset(), this.getUTestSet(), null, "utestset", null, 0, -1, UTestItem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getUTestItem_AttachedObject(), ecorePackage.getEObject(), null, "attachedObject", null, 0, 1, UTestItem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(umSpaceAEClass, UMSpaceA.class, "UMSpaceA", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getUMSpaceA_Source(), ecorePackage.getEObject(), null, "source", null, 0, 1, UMSpaceA.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getUMSpaceA_Transitions(), ecorePackage.getEObject(), null, "transitions", null, 0, -1, UMSpaceA.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getUMSpaceA_Ums(), ecorePackage.getEObject(), null, "ums", null, 0, -1, UMSpaceA.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(uTestModelEClass, UTestModel.class, "UTestModel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getUTestModel_Utestitem(), this.getUTestItem(), null, "utestitem", null, 0, -1, UTestModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getUTestModel_Utestconfiguration(), this.getUTestConfiguration(), null, "utestconfiguration", null, 0, -1, UTestModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getUTestModel_AttachedObject(), ecorePackage.getEObject(), null, "attachedObject", null, 0, 1, UTestModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(uTestConfigurationEClass, UTestConfiguration.class, "UTestConfiguration", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getUTestConfiguration_AttachedObject(), ecorePackage.getEObject(), null, "attachedObject", null, 0, 1, UTestConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //UTestModelPackageImpl
