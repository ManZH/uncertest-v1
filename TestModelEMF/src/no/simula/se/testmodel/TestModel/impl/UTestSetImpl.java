/*******************************************************************************
 * Copyright (c) 2016, 2017 Simula Research Laboratory. 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Simula license 
 * which accompanies this distribution, and is available at LICENSE.TXT
 *  
 *  Contributors:
 *        Man Zhang
 *******************************************************************************/
/**
 */
package no.simula.se.testmodel.TestModel.impl;

import java.util.Collection;

import no.simula.se.testmodel.TestModel.UMSpaceA;
import no.simula.se.testmodel.TestModel.UTestCase;
import no.simula.se.testmodel.TestModel.UTestModelPackage;
import no.simula.se.testmodel.TestModel.UTestSet;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>UTest Set</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link no.simula.se.testmodel.TestModel.impl.UTestSetImpl#getUtestcase <em>Utestcase</em>}</li>
 *   <li>{@link no.simula.se.testmodel.TestModel.impl.UTestSetImpl#getAttachedObject <em>Attached Object</em>}</li>
 *   <li>{@link no.simula.se.testmodel.TestModel.impl.UTestSetImpl#getUmspacea <em>Umspacea</em>}</li>
 * </ul>
 *
 * @generated
 */
public class UTestSetImpl extends MinimalEObjectImpl.Container implements UTestSet {
	/**
	 * The cached value of the '{@link #getUtestcase() <em>Utestcase</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUtestcase()
	 * @generated
	 * @ordered
	 */
	protected EList<UTestCase> utestcase;

	/**
	 * The cached value of the '{@link #getAttachedObject() <em>Attached Object</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAttachedObject()
	 * @generated
	 * @ordered
	 */
	protected EObject attachedObject;

	/**
	 * The cached value of the '{@link #getUmspacea() <em>Umspacea</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUmspacea()
	 * @generated
	 * @ordered
	 */
	protected EList<UMSpaceA> umspacea;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UTestSetImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return UTestModelPackage.Literals.UTEST_SET;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<UTestCase> getUtestcase() {
		if (utestcase == null) {
			utestcase = new EObjectContainmentEList<UTestCase>(UTestCase.class, this, UTestModelPackage.UTEST_SET__UTESTCASE);
		}
		return utestcase;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EObject getAttachedObject() {
		if (attachedObject != null && attachedObject.eIsProxy()) {
			InternalEObject oldAttachedObject = (InternalEObject)attachedObject;
			attachedObject = eResolveProxy(oldAttachedObject);
			if (attachedObject != oldAttachedObject) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, UTestModelPackage.UTEST_SET__ATTACHED_OBJECT, oldAttachedObject, attachedObject));
			}
		}
		return attachedObject;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EObject basicGetAttachedObject() {
		return attachedObject;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAttachedObject(EObject newAttachedObject) {
		EObject oldAttachedObject = attachedObject;
		attachedObject = newAttachedObject;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UTestModelPackage.UTEST_SET__ATTACHED_OBJECT, oldAttachedObject, attachedObject));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<UMSpaceA> getUmspacea() {
		if (umspacea == null) {
			umspacea = new EObjectContainmentEList<UMSpaceA>(UMSpaceA.class, this, UTestModelPackage.UTEST_SET__UMSPACEA);
		}
		return umspacea;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case UTestModelPackage.UTEST_SET__UTESTCASE:
				return ((InternalEList<?>)getUtestcase()).basicRemove(otherEnd, msgs);
			case UTestModelPackage.UTEST_SET__UMSPACEA:
				return ((InternalEList<?>)getUmspacea()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case UTestModelPackage.UTEST_SET__UTESTCASE:
				return getUtestcase();
			case UTestModelPackage.UTEST_SET__ATTACHED_OBJECT:
				if (resolve) return getAttachedObject();
				return basicGetAttachedObject();
			case UTestModelPackage.UTEST_SET__UMSPACEA:
				return getUmspacea();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case UTestModelPackage.UTEST_SET__UTESTCASE:
				getUtestcase().clear();
				getUtestcase().addAll((Collection<? extends UTestCase>)newValue);
				return;
			case UTestModelPackage.UTEST_SET__ATTACHED_OBJECT:
				setAttachedObject((EObject)newValue);
				return;
			case UTestModelPackage.UTEST_SET__UMSPACEA:
				getUmspacea().clear();
				getUmspacea().addAll((Collection<? extends UMSpaceA>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case UTestModelPackage.UTEST_SET__UTESTCASE:
				getUtestcase().clear();
				return;
			case UTestModelPackage.UTEST_SET__ATTACHED_OBJECT:
				setAttachedObject((EObject)null);
				return;
			case UTestModelPackage.UTEST_SET__UMSPACEA:
				getUmspacea().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case UTestModelPackage.UTEST_SET__UTESTCASE:
				return utestcase != null && !utestcase.isEmpty();
			case UTestModelPackage.UTEST_SET__ATTACHED_OBJECT:
				return attachedObject != null;
			case UTestModelPackage.UTEST_SET__UMSPACEA:
				return umspacea != null && !umspacea.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //UTestSetImpl
