/*******************************************************************************
 * Copyright (c) 2016, 2017 Simula Research Laboratory. 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Simula license 
 * which accompanies this distribution, and is available at LICENSE.TXT
 *  
 *  Contributors:
 *        Man Zhang
 *******************************************************************************/
/**
 */
package no.simula.se.testmodel.TestModel.impl;

import no.simula.se.testmodel.TestModel.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class UTestModelFactoryImpl extends EFactoryImpl implements UTestModelFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static UTestModelFactory init() {
		try {
			UTestModelFactory theUTestModelFactory = (UTestModelFactory)EPackage.Registry.INSTANCE.getEFactory(UTestModelPackage.eNS_URI);
			if (theUTestModelFactory != null) {
				return theUTestModelFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new UTestModelFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UTestModelFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case UTestModelPackage.UTEST_SET: return createUTestSet();
			case UTestModelPackage.UTEST_PATH: return createUTestPath();
			case UTestModelPackage.UTEST_ACTION: return createUTestAction();
			case UTestModelPackage.UTEST_CASE: return createUTestCase();
			case UTestModelPackage.UTEST_PARALLEL: return createUTestParallel();
			case UTestModelPackage.UTEST_ITEM: return createUTestItem();
			case UTestModelPackage.UM_SPACE_A: return createUMSpaceA();
			case UTestModelPackage.UTEST_MODEL: return createUTestModel();
			case UTestModelPackage.UTEST_CONFIGURATION: return createUTestConfiguration();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UTestSet createUTestSet() {
		UTestSetImpl uTestSet = new UTestSetImpl();
		return uTestSet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UTestPath createUTestPath() {
		UTestPathImpl uTestPath = new UTestPathImpl();
		return uTestPath;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UTestAction createUTestAction() {
		UTestActionImpl uTestAction = new UTestActionImpl();
		return uTestAction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UTestCase createUTestCase() {
		UTestCaseImpl uTestCase = new UTestCaseImpl();
		return uTestCase;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UTestParallel createUTestParallel() {
		UTestParallelImpl uTestParallel = new UTestParallelImpl();
		return uTestParallel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UTestItem createUTestItem() {
		UTestItemImpl uTestItem = new UTestItemImpl();
		return uTestItem;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UMSpaceA createUMSpaceA() {
		UMSpaceAImpl umSpaceA = new UMSpaceAImpl();
		return umSpaceA;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UTestModel createUTestModel() {
		UTestModelImpl uTestModel = new UTestModelImpl();
		return uTestModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UTestConfiguration createUTestConfiguration() {
		UTestConfigurationImpl uTestConfiguration = new UTestConfigurationImpl();
		return uTestConfiguration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UTestModelPackage getUTestModelPackage() {
		return (UTestModelPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static UTestModelPackage getPackage() {
		return UTestModelPackage.eINSTANCE;
	}

} //UTestModelFactoryImpl
