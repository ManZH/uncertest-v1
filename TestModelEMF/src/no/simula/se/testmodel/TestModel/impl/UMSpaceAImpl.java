/*******************************************************************************
 * Copyright (c) 2016, 2017 Simula Research Laboratory. 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Simula license 
 * which accompanies this distribution, and is available at LICENSE.TXT
 *  
 *  Contributors:
 *        Man Zhang
 *******************************************************************************/
/**
 */
package no.simula.se.testmodel.TestModel.impl;

import java.util.Collection;

import no.simula.se.testmodel.TestModel.UMSpaceA;
import no.simula.se.testmodel.TestModel.UTestModelPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>UM Space A</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link no.simula.se.testmodel.TestModel.impl.UMSpaceAImpl#getSource <em>Source</em>}</li>
 *   <li>{@link no.simula.se.testmodel.TestModel.impl.UMSpaceAImpl#getTransitions <em>Transitions</em>}</li>
 *   <li>{@link no.simula.se.testmodel.TestModel.impl.UMSpaceAImpl#getUms <em>Ums</em>}</li>
 * </ul>
 *
 * @generated
 */
public class UMSpaceAImpl extends MinimalEObjectImpl.Container implements UMSpaceA {
	/**
	 * The cached value of the '{@link #getSource() <em>Source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSource()
	 * @generated
	 * @ordered
	 */
	protected EObject source;

	/**
	 * The cached value of the '{@link #getTransitions() <em>Transitions</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTransitions()
	 * @generated
	 * @ordered
	 */
	protected EList<EObject> transitions;

	/**
	 * The cached value of the '{@link #getUms() <em>Ums</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUms()
	 * @generated
	 * @ordered
	 */
	protected EList<EObject> ums;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UMSpaceAImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return UTestModelPackage.Literals.UM_SPACE_A;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EObject getSource() {
		if (source != null && source.eIsProxy()) {
			InternalEObject oldSource = (InternalEObject)source;
			source = eResolveProxy(oldSource);
			if (source != oldSource) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, UTestModelPackage.UM_SPACE_A__SOURCE, oldSource, source));
			}
		}
		return source;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EObject basicGetSource() {
		return source;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSource(EObject newSource) {
		EObject oldSource = source;
		source = newSource;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UTestModelPackage.UM_SPACE_A__SOURCE, oldSource, source));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EObject> getTransitions() {
		if (transitions == null) {
			transitions = new EObjectResolvingEList<EObject>(EObject.class, this, UTestModelPackage.UM_SPACE_A__TRANSITIONS);
		}
		return transitions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EObject> getUms() {
		if (ums == null) {
			ums = new EObjectResolvingEList<EObject>(EObject.class, this, UTestModelPackage.UM_SPACE_A__UMS);
		}
		return ums;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case UTestModelPackage.UM_SPACE_A__SOURCE:
				if (resolve) return getSource();
				return basicGetSource();
			case UTestModelPackage.UM_SPACE_A__TRANSITIONS:
				return getTransitions();
			case UTestModelPackage.UM_SPACE_A__UMS:
				return getUms();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case UTestModelPackage.UM_SPACE_A__SOURCE:
				setSource((EObject)newValue);
				return;
			case UTestModelPackage.UM_SPACE_A__TRANSITIONS:
				getTransitions().clear();
				getTransitions().addAll((Collection<? extends EObject>)newValue);
				return;
			case UTestModelPackage.UM_SPACE_A__UMS:
				getUms().clear();
				getUms().addAll((Collection<? extends EObject>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case UTestModelPackage.UM_SPACE_A__SOURCE:
				setSource((EObject)null);
				return;
			case UTestModelPackage.UM_SPACE_A__TRANSITIONS:
				getTransitions().clear();
				return;
			case UTestModelPackage.UM_SPACE_A__UMS:
				getUms().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case UTestModelPackage.UM_SPACE_A__SOURCE:
				return source != null;
			case UTestModelPackage.UM_SPACE_A__TRANSITIONS:
				return transitions != null && !transitions.isEmpty();
			case UTestModelPackage.UM_SPACE_A__UMS:
				return ums != null && !ums.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //UMSpaceAImpl
