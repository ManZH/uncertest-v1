/*******************************************************************************
 * Copyright (c) 2016, 2017 Simula Research Laboratory. 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Simula license 
 * which accompanies this distribution, and is available at LICENSE.TXT
 *  
 *  Contributors:
 *        Man Zhang
 *******************************************************************************/
/**
 */
package no.simula.se.testmodel.TestModel;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>UTest Path</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link no.simula.se.testmodel.TestModel.UTestPath#getSequence <em>Sequence</em>}</li>
 *   <li>{@link no.simula.se.testmodel.TestModel.UTestPath#getMeasuredValues <em>Measured Values</em>}</li>
 *   <li>{@link no.simula.se.testmodel.TestModel.UTestPath#getSize <em>Size</em>}</li>
 * </ul>
 *
 * @see no.simula.se.testmodel.TestModel.UTestModelPackage#getUTestPath()
 * @model
 * @generated
 */
public interface UTestPath extends UTestCaseElement, UTestCase {
	/**
	 * Returns the value of the '<em><b>Sequence</b></em>' containment reference list.
	 * The list contents are of type {@link no.simula.se.testmodel.TestModel.UTestCaseElement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sequence</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sequence</em>' containment reference list.
	 * @see no.simula.se.testmodel.TestModel.UTestModelPackage#getUTestPath_Sequence()
	 * @model containment="true"
	 * @generated
	 */
	EList<UTestCaseElement> getSequence();

	/**
	 * Returns the value of the '<em><b>Measured Values</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.Double}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Measured Values</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Measured Values</em>' attribute list.
	 * @see no.simula.se.testmodel.TestModel.UTestModelPackage#getUTestPath_MeasuredValues()
	 * @model unique="false"
	 * @generated
	 */
	EList<Double> getMeasuredValues();

	/**
	 * Returns the value of the '<em><b>Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Size</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Size</em>' attribute.
	 * @see #setSize(int)
	 * @see no.simula.se.testmodel.TestModel.UTestModelPackage#getUTestPath_Size()
	 * @model
	 * @generated
	 */
	int getSize();

	/**
	 * Sets the value of the '{@link no.simula.se.testmodel.TestModel.UTestPath#getSize <em>Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Size</em>' attribute.
	 * @see #getSize()
	 * @generated
	 */
	void setSize(int value);

} // UTestPath
