/*******************************************************************************
 * Copyright (c) 2016, 2017 Simula Research Laboratory. 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Simula license 
 * which accompanies this distribution, and is available at LICENSE.TXT
 *  
 *  Contributors:
 *        Man Zhang
 *******************************************************************************/
/**
 */
package no.simula.se.testmodel.TestModel.util;

import no.simula.se.testmodel.TestModel.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see no.simula.se.testmodel.TestModel.UTestModelPackage
 * @generated
 */
public class UTestModelAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static UTestModelPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UTestModelAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = UTestModelPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UTestModelSwitch<Adapter> modelSwitch =
		new UTestModelSwitch<Adapter>() {
			@Override
			public Adapter caseUTestSet(UTestSet object) {
				return createUTestSetAdapter();
			}
			@Override
			public Adapter caseUTestPath(UTestPath object) {
				return createUTestPathAdapter();
			}
			@Override
			public Adapter caseUTestAction(UTestAction object) {
				return createUTestActionAdapter();
			}
			@Override
			public Adapter caseUTestCase(UTestCase object) {
				return createUTestCaseAdapter();
			}
			@Override
			public Adapter caseUTestCaseElement(UTestCaseElement object) {
				return createUTestCaseElementAdapter();
			}
			@Override
			public Adapter caseUTestParallel(UTestParallel object) {
				return createUTestParallelAdapter();
			}
			@Override
			public Adapter caseUTestItem(UTestItem object) {
				return createUTestItemAdapter();
			}
			@Override
			public Adapter caseUMSpaceA(UMSpaceA object) {
				return createUMSpaceAAdapter();
			}
			@Override
			public Adapter caseUTestModel(UTestModel object) {
				return createUTestModelAdapter();
			}
			@Override
			public Adapter caseUTestConfiguration(UTestConfiguration object) {
				return createUTestConfigurationAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link no.simula.se.testmodel.TestModel.UTestSet <em>UTest Set</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see no.simula.se.testmodel.TestModel.UTestSet
	 * @generated
	 */
	public Adapter createUTestSetAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link no.simula.se.testmodel.TestModel.UTestPath <em>UTest Path</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see no.simula.se.testmodel.TestModel.UTestPath
	 * @generated
	 */
	public Adapter createUTestPathAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link no.simula.se.testmodel.TestModel.UTestAction <em>UTest Action</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see no.simula.se.testmodel.TestModel.UTestAction
	 * @generated
	 */
	public Adapter createUTestActionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link no.simula.se.testmodel.TestModel.UTestCase <em>UTest Case</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see no.simula.se.testmodel.TestModel.UTestCase
	 * @generated
	 */
	public Adapter createUTestCaseAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link no.simula.se.testmodel.TestModel.UTestCaseElement <em>UTest Case Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see no.simula.se.testmodel.TestModel.UTestCaseElement
	 * @generated
	 */
	public Adapter createUTestCaseElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link no.simula.se.testmodel.TestModel.UTestParallel <em>UTest Parallel</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see no.simula.se.testmodel.TestModel.UTestParallel
	 * @generated
	 */
	public Adapter createUTestParallelAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link no.simula.se.testmodel.TestModel.UTestItem <em>UTest Item</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see no.simula.se.testmodel.TestModel.UTestItem
	 * @generated
	 */
	public Adapter createUTestItemAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link no.simula.se.testmodel.TestModel.UMSpaceA <em>UM Space A</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see no.simula.se.testmodel.TestModel.UMSpaceA
	 * @generated
	 */
	public Adapter createUMSpaceAAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link no.simula.se.testmodel.TestModel.UTestModel <em>UTest Model</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see no.simula.se.testmodel.TestModel.UTestModel
	 * @generated
	 */
	public Adapter createUTestModelAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link no.simula.se.testmodel.TestModel.UTestConfiguration <em>UTest Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see no.simula.se.testmodel.TestModel.UTestConfiguration
	 * @generated
	 */
	public Adapter createUTestConfigurationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //UTestModelAdapterFactory
