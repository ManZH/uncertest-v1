/*******************************************************************************
 * Copyright (c) 2016, 2017 Simula Research Laboratory. 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Simula license 
 * which accompanies this distribution, and is available at LICENSE.TXT
 *  
 *  Contributors:
 *        Man Zhang
 *******************************************************************************/
/**
 */
package no.simula.se.testmodel.TestModel.util;

import no.simula.se.testmodel.TestModel.*;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see no.simula.se.testmodel.TestModel.UTestModelPackage
 * @generated
 */
public class UTestModelSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static UTestModelPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UTestModelSwitch() {
		if (modelPackage == null) {
			modelPackage = UTestModelPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case UTestModelPackage.UTEST_SET: {
				UTestSet uTestSet = (UTestSet)theEObject;
				T result = caseUTestSet(uTestSet);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UTestModelPackage.UTEST_PATH: {
				UTestPath uTestPath = (UTestPath)theEObject;
				T result = caseUTestPath(uTestPath);
				if (result == null) result = caseUTestCaseElement(uTestPath);
				if (result == null) result = caseUTestCase(uTestPath);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UTestModelPackage.UTEST_ACTION: {
				UTestAction uTestAction = (UTestAction)theEObject;
				T result = caseUTestAction(uTestAction);
				if (result == null) result = caseUTestCaseElement(uTestAction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UTestModelPackage.UTEST_CASE: {
				UTestCase uTestCase = (UTestCase)theEObject;
				T result = caseUTestCase(uTestCase);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UTestModelPackage.UTEST_CASE_ELEMENT: {
				UTestCaseElement uTestCaseElement = (UTestCaseElement)theEObject;
				T result = caseUTestCaseElement(uTestCaseElement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UTestModelPackage.UTEST_PARALLEL: {
				UTestParallel uTestParallel = (UTestParallel)theEObject;
				T result = caseUTestParallel(uTestParallel);
				if (result == null) result = caseUTestCaseElement(uTestParallel);
				if (result == null) result = caseUTestCase(uTestParallel);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UTestModelPackage.UTEST_ITEM: {
				UTestItem uTestItem = (UTestItem)theEObject;
				T result = caseUTestItem(uTestItem);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UTestModelPackage.UM_SPACE_A: {
				UMSpaceA umSpaceA = (UMSpaceA)theEObject;
				T result = caseUMSpaceA(umSpaceA);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UTestModelPackage.UTEST_MODEL: {
				UTestModel uTestModel = (UTestModel)theEObject;
				T result = caseUTestModel(uTestModel);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case UTestModelPackage.UTEST_CONFIGURATION: {
				UTestConfiguration uTestConfiguration = (UTestConfiguration)theEObject;
				T result = caseUTestConfiguration(uTestConfiguration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>UTest Set</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>UTest Set</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUTestSet(UTestSet object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>UTest Path</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>UTest Path</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUTestPath(UTestPath object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>UTest Action</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>UTest Action</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUTestAction(UTestAction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>UTest Case</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>UTest Case</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUTestCase(UTestCase object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>UTest Case Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>UTest Case Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUTestCaseElement(UTestCaseElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>UTest Parallel</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>UTest Parallel</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUTestParallel(UTestParallel object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>UTest Item</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>UTest Item</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUTestItem(UTestItem object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>UM Space A</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>UM Space A</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUMSpaceA(UMSpaceA object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>UTest Model</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>UTest Model</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUTestModel(UTestModel object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>UTest Configuration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>UTest Configuration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUTestConfiguration(UTestConfiguration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //UTestModelSwitch
