/*******************************************************************************
 * Copyright (c) 2016, 2017 Simula Research Laboratory. 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Simula license 
 * which accompanies this distribution, and is available at LICENSE.TXT
 *  
 *  Contributors:
 *        Man Zhang
 *******************************************************************************/
/**
 */
package no.simula.se.testmodel.TestModel;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see no.simula.se.testmodel.TestModel.UTestModelPackage
 * @generated
 */
public interface UTestModelFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	UTestModelFactory eINSTANCE = no.simula.se.testmodel.TestModel.impl.UTestModelFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>UTest Set</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>UTest Set</em>'.
	 * @generated
	 */
	UTestSet createUTestSet();

	/**
	 * Returns a new object of class '<em>UTest Path</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>UTest Path</em>'.
	 * @generated
	 */
	UTestPath createUTestPath();

	/**
	 * Returns a new object of class '<em>UTest Action</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>UTest Action</em>'.
	 * @generated
	 */
	UTestAction createUTestAction();

	/**
	 * Returns a new object of class '<em>UTest Case</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>UTest Case</em>'.
	 * @generated
	 */
	UTestCase createUTestCase();

	/**
	 * Returns a new object of class '<em>UTest Parallel</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>UTest Parallel</em>'.
	 * @generated
	 */
	UTestParallel createUTestParallel();

	/**
	 * Returns a new object of class '<em>UTest Item</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>UTest Item</em>'.
	 * @generated
	 */
	UTestItem createUTestItem();

	/**
	 * Returns a new object of class '<em>UM Space A</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>UM Space A</em>'.
	 * @generated
	 */
	UMSpaceA createUMSpaceA();

	/**
	 * Returns a new object of class '<em>UTest Model</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>UTest Model</em>'.
	 * @generated
	 */
	UTestModel createUTestModel();

	/**
	 * Returns a new object of class '<em>UTest Configuration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>UTest Configuration</em>'.
	 * @generated
	 */
	UTestConfiguration createUTestConfiguration();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	UTestModelPackage getUTestModelPackage();

} //UTestModelFactory
