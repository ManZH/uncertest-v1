/*******************************************************************************
 * Copyright (c) 2016, 2017 Simula Research Laboratory. 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Simula license 
 * which accompanies this distribution, and is available at LICENSE.TXT
 *  
 *  Contributors:
 *        Man Zhang
 *******************************************************************************/
/**
 */
package no.simula.se.testmodel.TestModel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>UTest Configuration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link no.simula.se.testmodel.TestModel.UTestConfiguration#getAttachedObject <em>Attached Object</em>}</li>
 * </ul>
 *
 * @see no.simula.se.testmodel.TestModel.UTestModelPackage#getUTestConfiguration()
 * @model
 * @generated
 */
public interface UTestConfiguration extends EObject {
	/**
	 * Returns the value of the '<em><b>Attached Object</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Attached Object</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Attached Object</em>' reference.
	 * @see #setAttachedObject(EObject)
	 * @see no.simula.se.testmodel.TestModel.UTestModelPackage#getUTestConfiguration_AttachedObject()
	 * @model
	 * @generated
	 */
	EObject getAttachedObject();

	/**
	 * Sets the value of the '{@link no.simula.se.testmodel.TestModel.UTestConfiguration#getAttachedObject <em>Attached Object</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Attached Object</em>' reference.
	 * @see #getAttachedObject()
	 * @generated
	 */
	void setAttachedObject(EObject value);

} // UTestConfiguration
