/*******************************************************************************
 * Copyright (c) 2016, 2017 Simula Research Laboratory. 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Simula license 
 * which accompanies this distribution, and is available at LICENSE.TXT
 *  
 *  Contributors:
 *        Man Zhang
 *******************************************************************************/
/**
 */
package no.simula.se.testmodel.TestModel;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see no.simula.se.testmodel.TestModel.UTestModelFactory
 * @model kind="package"
 * @generated
 */
public interface UTestModelPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "TestModel";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "no.simula.se.testmodel";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "UTestModel";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	UTestModelPackage eINSTANCE = no.simula.se.testmodel.TestModel.impl.UTestModelPackageImpl.init();

	/**
	 * The meta object id for the '{@link no.simula.se.testmodel.TestModel.impl.UTestSetImpl <em>UTest Set</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see no.simula.se.testmodel.TestModel.impl.UTestSetImpl
	 * @see no.simula.se.testmodel.TestModel.impl.UTestModelPackageImpl#getUTestSet()
	 * @generated
	 */
	int UTEST_SET = 0;

	/**
	 * The feature id for the '<em><b>Utestcase</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UTEST_SET__UTESTCASE = 0;

	/**
	 * The feature id for the '<em><b>Attached Object</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UTEST_SET__ATTACHED_OBJECT = 1;

	/**
	 * The feature id for the '<em><b>Umspacea</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UTEST_SET__UMSPACEA = 2;

	/**
	 * The number of structural features of the '<em>UTest Set</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UTEST_SET_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>UTest Set</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UTEST_SET_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link no.simula.se.testmodel.TestModel.impl.UTestCaseElementImpl <em>UTest Case Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see no.simula.se.testmodel.TestModel.impl.UTestCaseElementImpl
	 * @see no.simula.se.testmodel.TestModel.impl.UTestModelPackageImpl#getUTestCaseElement()
	 * @generated
	 */
	int UTEST_CASE_ELEMENT = 4;

	/**
	 * The feature id for the '<em><b>Measured Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UTEST_CASE_ELEMENT__MEASURED_VALUE = 0;

	/**
	 * The number of structural features of the '<em>UTest Case Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UTEST_CASE_ELEMENT_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>UTest Case Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UTEST_CASE_ELEMENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link no.simula.se.testmodel.TestModel.impl.UTestPathImpl <em>UTest Path</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see no.simula.se.testmodel.TestModel.impl.UTestPathImpl
	 * @see no.simula.se.testmodel.TestModel.impl.UTestModelPackageImpl#getUTestPath()
	 * @generated
	 */
	int UTEST_PATH = 1;

	/**
	 * The feature id for the '<em><b>Measured Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UTEST_PATH__MEASURED_VALUE = UTEST_CASE_ELEMENT__MEASURED_VALUE;

	/**
	 * The feature id for the '<em><b>Uinfo</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UTEST_PATH__UINFO = UTEST_CASE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Length</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UTEST_PATH__LENGTH = UTEST_CASE_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Sequence</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UTEST_PATH__SEQUENCE = UTEST_CASE_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Measured Values</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UTEST_PATH__MEASURED_VALUES = UTEST_CASE_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UTEST_PATH__SIZE = UTEST_CASE_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>UTest Path</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UTEST_PATH_FEATURE_COUNT = UTEST_CASE_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The number of operations of the '<em>UTest Path</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UTEST_PATH_OPERATION_COUNT = UTEST_CASE_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link no.simula.se.testmodel.TestModel.impl.UTestActionImpl <em>UTest Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see no.simula.se.testmodel.TestModel.impl.UTestActionImpl
	 * @see no.simula.se.testmodel.TestModel.impl.UTestModelPackageImpl#getUTestAction()
	 * @generated
	 */
	int UTEST_ACTION = 2;

	/**
	 * The feature id for the '<em><b>Measured Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UTEST_ACTION__MEASURED_VALUE = UTEST_CASE_ELEMENT__MEASURED_VALUE;

	/**
	 * The feature id for the '<em><b>Attached Object</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UTEST_ACTION__ATTACHED_OBJECT = UTEST_CASE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Instanceo Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UTEST_ACTION__INSTANCEO_NAME = UTEST_CASE_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>UTest Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UTEST_ACTION_FEATURE_COUNT = UTEST_CASE_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>UTest Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UTEST_ACTION_OPERATION_COUNT = UTEST_CASE_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link no.simula.se.testmodel.TestModel.impl.UTestCaseImpl <em>UTest Case</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see no.simula.se.testmodel.TestModel.impl.UTestCaseImpl
	 * @see no.simula.se.testmodel.TestModel.impl.UTestModelPackageImpl#getUTestCase()
	 * @generated
	 */
	int UTEST_CASE = 3;

	/**
	 * The feature id for the '<em><b>Uinfo</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UTEST_CASE__UINFO = 0;

	/**
	 * The feature id for the '<em><b>Length</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UTEST_CASE__LENGTH = 1;

	/**
	 * The number of structural features of the '<em>UTest Case</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UTEST_CASE_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>UTest Case</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UTEST_CASE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link no.simula.se.testmodel.TestModel.impl.UTestParallelImpl <em>UTest Parallel</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see no.simula.se.testmodel.TestModel.impl.UTestParallelImpl
	 * @see no.simula.se.testmodel.TestModel.impl.UTestModelPackageImpl#getUTestParallel()
	 * @generated
	 */
	int UTEST_PARALLEL = 5;

	/**
	 * The feature id for the '<em><b>Measured Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UTEST_PARALLEL__MEASURED_VALUE = UTEST_CASE_ELEMENT__MEASURED_VALUE;

	/**
	 * The feature id for the '<em><b>Uinfo</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UTEST_PARALLEL__UINFO = UTEST_CASE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Length</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UTEST_PARALLEL__LENGTH = UTEST_CASE_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Paralles</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UTEST_PARALLEL__PARALLES = UTEST_CASE_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UTEST_PARALLEL__SIZE = UTEST_CASE_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>UTest Parallel</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UTEST_PARALLEL_FEATURE_COUNT = UTEST_CASE_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>UTest Parallel</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UTEST_PARALLEL_OPERATION_COUNT = UTEST_CASE_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link no.simula.se.testmodel.TestModel.impl.UTestItemImpl <em>UTest Item</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see no.simula.se.testmodel.TestModel.impl.UTestItemImpl
	 * @see no.simula.se.testmodel.TestModel.impl.UTestModelPackageImpl#getUTestItem()
	 * @generated
	 */
	int UTEST_ITEM = 6;

	/**
	 * The feature id for the '<em><b>Utestset</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UTEST_ITEM__UTESTSET = 0;

	/**
	 * The feature id for the '<em><b>Attached Object</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UTEST_ITEM__ATTACHED_OBJECT = 1;

	/**
	 * The number of structural features of the '<em>UTest Item</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UTEST_ITEM_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>UTest Item</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UTEST_ITEM_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link no.simula.se.testmodel.TestModel.impl.UMSpaceAImpl <em>UM Space A</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see no.simula.se.testmodel.TestModel.impl.UMSpaceAImpl
	 * @see no.simula.se.testmodel.TestModel.impl.UTestModelPackageImpl#getUMSpaceA()
	 * @generated
	 */
	int UM_SPACE_A = 7;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UM_SPACE_A__SOURCE = 0;

	/**
	 * The feature id for the '<em><b>Transitions</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UM_SPACE_A__TRANSITIONS = 1;

	/**
	 * The feature id for the '<em><b>Ums</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UM_SPACE_A__UMS = 2;

	/**
	 * The number of structural features of the '<em>UM Space A</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UM_SPACE_A_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>UM Space A</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UM_SPACE_A_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link no.simula.se.testmodel.TestModel.impl.UTestModelImpl <em>UTest Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see no.simula.se.testmodel.TestModel.impl.UTestModelImpl
	 * @see no.simula.se.testmodel.TestModel.impl.UTestModelPackageImpl#getUTestModel()
	 * @generated
	 */
	int UTEST_MODEL = 8;

	/**
	 * The feature id for the '<em><b>Utestitem</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UTEST_MODEL__UTESTITEM = 0;

	/**
	 * The feature id for the '<em><b>Utestconfiguration</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UTEST_MODEL__UTESTCONFIGURATION = 1;

	/**
	 * The feature id for the '<em><b>Attached Object</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UTEST_MODEL__ATTACHED_OBJECT = 2;

	/**
	 * The number of structural features of the '<em>UTest Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UTEST_MODEL_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>UTest Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UTEST_MODEL_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link no.simula.se.testmodel.TestModel.impl.UTestConfigurationImpl <em>UTest Configuration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see no.simula.se.testmodel.TestModel.impl.UTestConfigurationImpl
	 * @see no.simula.se.testmodel.TestModel.impl.UTestModelPackageImpl#getUTestConfiguration()
	 * @generated
	 */
	int UTEST_CONFIGURATION = 9;

	/**
	 * The feature id for the '<em><b>Attached Object</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UTEST_CONFIGURATION__ATTACHED_OBJECT = 0;

	/**
	 * The number of structural features of the '<em>UTest Configuration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UTEST_CONFIGURATION_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>UTest Configuration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UTEST_CONFIGURATION_OPERATION_COUNT = 0;


	/**
	 * Returns the meta object for class '{@link no.simula.se.testmodel.TestModel.UTestSet <em>UTest Set</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>UTest Set</em>'.
	 * @see no.simula.se.testmodel.TestModel.UTestSet
	 * @generated
	 */
	EClass getUTestSet();

	/**
	 * Returns the meta object for the containment reference list '{@link no.simula.se.testmodel.TestModel.UTestSet#getUtestcase <em>Utestcase</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Utestcase</em>'.
	 * @see no.simula.se.testmodel.TestModel.UTestSet#getUtestcase()
	 * @see #getUTestSet()
	 * @generated
	 */
	EReference getUTestSet_Utestcase();

	/**
	 * Returns the meta object for the reference '{@link no.simula.se.testmodel.TestModel.UTestSet#getAttachedObject <em>Attached Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Attached Object</em>'.
	 * @see no.simula.se.testmodel.TestModel.UTestSet#getAttachedObject()
	 * @see #getUTestSet()
	 * @generated
	 */
	EReference getUTestSet_AttachedObject();

	/**
	 * Returns the meta object for the containment reference list '{@link no.simula.se.testmodel.TestModel.UTestSet#getUmspacea <em>Umspacea</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Umspacea</em>'.
	 * @see no.simula.se.testmodel.TestModel.UTestSet#getUmspacea()
	 * @see #getUTestSet()
	 * @generated
	 */
	EReference getUTestSet_Umspacea();

	/**
	 * Returns the meta object for class '{@link no.simula.se.testmodel.TestModel.UTestPath <em>UTest Path</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>UTest Path</em>'.
	 * @see no.simula.se.testmodel.TestModel.UTestPath
	 * @generated
	 */
	EClass getUTestPath();

	/**
	 * Returns the meta object for the containment reference list '{@link no.simula.se.testmodel.TestModel.UTestPath#getSequence <em>Sequence</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Sequence</em>'.
	 * @see no.simula.se.testmodel.TestModel.UTestPath#getSequence()
	 * @see #getUTestPath()
	 * @generated
	 */
	EReference getUTestPath_Sequence();

	/**
	 * Returns the meta object for the attribute list '{@link no.simula.se.testmodel.TestModel.UTestPath#getMeasuredValues <em>Measured Values</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Measured Values</em>'.
	 * @see no.simula.se.testmodel.TestModel.UTestPath#getMeasuredValues()
	 * @see #getUTestPath()
	 * @generated
	 */
	EAttribute getUTestPath_MeasuredValues();

	/**
	 * Returns the meta object for the attribute '{@link no.simula.se.testmodel.TestModel.UTestPath#getSize <em>Size</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Size</em>'.
	 * @see no.simula.se.testmodel.TestModel.UTestPath#getSize()
	 * @see #getUTestPath()
	 * @generated
	 */
	EAttribute getUTestPath_Size();

	/**
	 * Returns the meta object for class '{@link no.simula.se.testmodel.TestModel.UTestAction <em>UTest Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>UTest Action</em>'.
	 * @see no.simula.se.testmodel.TestModel.UTestAction
	 * @generated
	 */
	EClass getUTestAction();

	/**
	 * Returns the meta object for the reference '{@link no.simula.se.testmodel.TestModel.UTestAction#getAttachedObject <em>Attached Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Attached Object</em>'.
	 * @see no.simula.se.testmodel.TestModel.UTestAction#getAttachedObject()
	 * @see #getUTestAction()
	 * @generated
	 */
	EReference getUTestAction_AttachedObject();

	/**
	 * Returns the meta object for the attribute '{@link no.simula.se.testmodel.TestModel.UTestAction#getInstanceoName <em>Instanceo Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Instanceo Name</em>'.
	 * @see no.simula.se.testmodel.TestModel.UTestAction#getInstanceoName()
	 * @see #getUTestAction()
	 * @generated
	 */
	EAttribute getUTestAction_InstanceoName();

	/**
	 * Returns the meta object for class '{@link no.simula.se.testmodel.TestModel.UTestCase <em>UTest Case</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>UTest Case</em>'.
	 * @see no.simula.se.testmodel.TestModel.UTestCase
	 * @generated
	 */
	EClass getUTestCase();

	/**
	 * Returns the meta object for the attribute list '{@link no.simula.se.testmodel.TestModel.UTestCase#getUinfo <em>Uinfo</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Uinfo</em>'.
	 * @see no.simula.se.testmodel.TestModel.UTestCase#getUinfo()
	 * @see #getUTestCase()
	 * @generated
	 */
	EAttribute getUTestCase_Uinfo();

	/**
	 * Returns the meta object for the attribute '{@link no.simula.se.testmodel.TestModel.UTestCase#getLength <em>Length</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Length</em>'.
	 * @see no.simula.se.testmodel.TestModel.UTestCase#getLength()
	 * @see #getUTestCase()
	 * @generated
	 */
	EAttribute getUTestCase_Length();

	/**
	 * Returns the meta object for class '{@link no.simula.se.testmodel.TestModel.UTestCaseElement <em>UTest Case Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>UTest Case Element</em>'.
	 * @see no.simula.se.testmodel.TestModel.UTestCaseElement
	 * @generated
	 */
	EClass getUTestCaseElement();

	/**
	 * Returns the meta object for the attribute '{@link no.simula.se.testmodel.TestModel.UTestCaseElement#getMeasuredValue <em>Measured Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Measured Value</em>'.
	 * @see no.simula.se.testmodel.TestModel.UTestCaseElement#getMeasuredValue()
	 * @see #getUTestCaseElement()
	 * @generated
	 */
	EAttribute getUTestCaseElement_MeasuredValue();

	/**
	 * Returns the meta object for class '{@link no.simula.se.testmodel.TestModel.UTestParallel <em>UTest Parallel</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>UTest Parallel</em>'.
	 * @see no.simula.se.testmodel.TestModel.UTestParallel
	 * @generated
	 */
	EClass getUTestParallel();

	/**
	 * Returns the meta object for the containment reference list '{@link no.simula.se.testmodel.TestModel.UTestParallel#getParalles <em>Paralles</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Paralles</em>'.
	 * @see no.simula.se.testmodel.TestModel.UTestParallel#getParalles()
	 * @see #getUTestParallel()
	 * @generated
	 */
	EReference getUTestParallel_Paralles();

	/**
	 * Returns the meta object for the attribute '{@link no.simula.se.testmodel.TestModel.UTestParallel#getSize <em>Size</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Size</em>'.
	 * @see no.simula.se.testmodel.TestModel.UTestParallel#getSize()
	 * @see #getUTestParallel()
	 * @generated
	 */
	EAttribute getUTestParallel_Size();

	/**
	 * Returns the meta object for class '{@link no.simula.se.testmodel.TestModel.UTestItem <em>UTest Item</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>UTest Item</em>'.
	 * @see no.simula.se.testmodel.TestModel.UTestItem
	 * @generated
	 */
	EClass getUTestItem();

	/**
	 * Returns the meta object for the containment reference list '{@link no.simula.se.testmodel.TestModel.UTestItem#getUtestset <em>Utestset</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Utestset</em>'.
	 * @see no.simula.se.testmodel.TestModel.UTestItem#getUtestset()
	 * @see #getUTestItem()
	 * @generated
	 */
	EReference getUTestItem_Utestset();

	/**
	 * Returns the meta object for the reference '{@link no.simula.se.testmodel.TestModel.UTestItem#getAttachedObject <em>Attached Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Attached Object</em>'.
	 * @see no.simula.se.testmodel.TestModel.UTestItem#getAttachedObject()
	 * @see #getUTestItem()
	 * @generated
	 */
	EReference getUTestItem_AttachedObject();

	/**
	 * Returns the meta object for class '{@link no.simula.se.testmodel.TestModel.UMSpaceA <em>UM Space A</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>UM Space A</em>'.
	 * @see no.simula.se.testmodel.TestModel.UMSpaceA
	 * @generated
	 */
	EClass getUMSpaceA();

	/**
	 * Returns the meta object for the reference '{@link no.simula.se.testmodel.TestModel.UMSpaceA#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Source</em>'.
	 * @see no.simula.se.testmodel.TestModel.UMSpaceA#getSource()
	 * @see #getUMSpaceA()
	 * @generated
	 */
	EReference getUMSpaceA_Source();

	/**
	 * Returns the meta object for the reference list '{@link no.simula.se.testmodel.TestModel.UMSpaceA#getTransitions <em>Transitions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Transitions</em>'.
	 * @see no.simula.se.testmodel.TestModel.UMSpaceA#getTransitions()
	 * @see #getUMSpaceA()
	 * @generated
	 */
	EReference getUMSpaceA_Transitions();

	/**
	 * Returns the meta object for the reference list '{@link no.simula.se.testmodel.TestModel.UMSpaceA#getUms <em>Ums</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Ums</em>'.
	 * @see no.simula.se.testmodel.TestModel.UMSpaceA#getUms()
	 * @see #getUMSpaceA()
	 * @generated
	 */
	EReference getUMSpaceA_Ums();

	/**
	 * Returns the meta object for class '{@link no.simula.se.testmodel.TestModel.UTestModel <em>UTest Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>UTest Model</em>'.
	 * @see no.simula.se.testmodel.TestModel.UTestModel
	 * @generated
	 */
	EClass getUTestModel();

	/**
	 * Returns the meta object for the containment reference list '{@link no.simula.se.testmodel.TestModel.UTestModel#getUtestitem <em>Utestitem</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Utestitem</em>'.
	 * @see no.simula.se.testmodel.TestModel.UTestModel#getUtestitem()
	 * @see #getUTestModel()
	 * @generated
	 */
	EReference getUTestModel_Utestitem();

	/**
	 * Returns the meta object for the containment reference list '{@link no.simula.se.testmodel.TestModel.UTestModel#getUtestconfiguration <em>Utestconfiguration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Utestconfiguration</em>'.
	 * @see no.simula.se.testmodel.TestModel.UTestModel#getUtestconfiguration()
	 * @see #getUTestModel()
	 * @generated
	 */
	EReference getUTestModel_Utestconfiguration();

	/**
	 * Returns the meta object for the reference '{@link no.simula.se.testmodel.TestModel.UTestModel#getAttachedObject <em>Attached Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Attached Object</em>'.
	 * @see no.simula.se.testmodel.TestModel.UTestModel#getAttachedObject()
	 * @see #getUTestModel()
	 * @generated
	 */
	EReference getUTestModel_AttachedObject();

	/**
	 * Returns the meta object for class '{@link no.simula.se.testmodel.TestModel.UTestConfiguration <em>UTest Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>UTest Configuration</em>'.
	 * @see no.simula.se.testmodel.TestModel.UTestConfiguration
	 * @generated
	 */
	EClass getUTestConfiguration();

	/**
	 * Returns the meta object for the reference '{@link no.simula.se.testmodel.TestModel.UTestConfiguration#getAttachedObject <em>Attached Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Attached Object</em>'.
	 * @see no.simula.se.testmodel.TestModel.UTestConfiguration#getAttachedObject()
	 * @see #getUTestConfiguration()
	 * @generated
	 */
	EReference getUTestConfiguration_AttachedObject();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	UTestModelFactory getUTestModelFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link no.simula.se.testmodel.TestModel.impl.UTestSetImpl <em>UTest Set</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see no.simula.se.testmodel.TestModel.impl.UTestSetImpl
		 * @see no.simula.se.testmodel.TestModel.impl.UTestModelPackageImpl#getUTestSet()
		 * @generated
		 */
		EClass UTEST_SET = eINSTANCE.getUTestSet();

		/**
		 * The meta object literal for the '<em><b>Utestcase</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UTEST_SET__UTESTCASE = eINSTANCE.getUTestSet_Utestcase();

		/**
		 * The meta object literal for the '<em><b>Attached Object</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UTEST_SET__ATTACHED_OBJECT = eINSTANCE.getUTestSet_AttachedObject();

		/**
		 * The meta object literal for the '<em><b>Umspacea</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UTEST_SET__UMSPACEA = eINSTANCE.getUTestSet_Umspacea();

		/**
		 * The meta object literal for the '{@link no.simula.se.testmodel.TestModel.impl.UTestPathImpl <em>UTest Path</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see no.simula.se.testmodel.TestModel.impl.UTestPathImpl
		 * @see no.simula.se.testmodel.TestModel.impl.UTestModelPackageImpl#getUTestPath()
		 * @generated
		 */
		EClass UTEST_PATH = eINSTANCE.getUTestPath();

		/**
		 * The meta object literal for the '<em><b>Sequence</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UTEST_PATH__SEQUENCE = eINSTANCE.getUTestPath_Sequence();

		/**
		 * The meta object literal for the '<em><b>Measured Values</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute UTEST_PATH__MEASURED_VALUES = eINSTANCE.getUTestPath_MeasuredValues();

		/**
		 * The meta object literal for the '<em><b>Size</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute UTEST_PATH__SIZE = eINSTANCE.getUTestPath_Size();

		/**
		 * The meta object literal for the '{@link no.simula.se.testmodel.TestModel.impl.UTestActionImpl <em>UTest Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see no.simula.se.testmodel.TestModel.impl.UTestActionImpl
		 * @see no.simula.se.testmodel.TestModel.impl.UTestModelPackageImpl#getUTestAction()
		 * @generated
		 */
		EClass UTEST_ACTION = eINSTANCE.getUTestAction();

		/**
		 * The meta object literal for the '<em><b>Attached Object</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UTEST_ACTION__ATTACHED_OBJECT = eINSTANCE.getUTestAction_AttachedObject();

		/**
		 * The meta object literal for the '<em><b>Instanceo Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute UTEST_ACTION__INSTANCEO_NAME = eINSTANCE.getUTestAction_InstanceoName();

		/**
		 * The meta object literal for the '{@link no.simula.se.testmodel.TestModel.impl.UTestCaseImpl <em>UTest Case</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see no.simula.se.testmodel.TestModel.impl.UTestCaseImpl
		 * @see no.simula.se.testmodel.TestModel.impl.UTestModelPackageImpl#getUTestCase()
		 * @generated
		 */
		EClass UTEST_CASE = eINSTANCE.getUTestCase();

		/**
		 * The meta object literal for the '<em><b>Uinfo</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute UTEST_CASE__UINFO = eINSTANCE.getUTestCase_Uinfo();

		/**
		 * The meta object literal for the '<em><b>Length</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute UTEST_CASE__LENGTH = eINSTANCE.getUTestCase_Length();

		/**
		 * The meta object literal for the '{@link no.simula.se.testmodel.TestModel.impl.UTestCaseElementImpl <em>UTest Case Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see no.simula.se.testmodel.TestModel.impl.UTestCaseElementImpl
		 * @see no.simula.se.testmodel.TestModel.impl.UTestModelPackageImpl#getUTestCaseElement()
		 * @generated
		 */
		EClass UTEST_CASE_ELEMENT = eINSTANCE.getUTestCaseElement();

		/**
		 * The meta object literal for the '<em><b>Measured Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute UTEST_CASE_ELEMENT__MEASURED_VALUE = eINSTANCE.getUTestCaseElement_MeasuredValue();

		/**
		 * The meta object literal for the '{@link no.simula.se.testmodel.TestModel.impl.UTestParallelImpl <em>UTest Parallel</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see no.simula.se.testmodel.TestModel.impl.UTestParallelImpl
		 * @see no.simula.se.testmodel.TestModel.impl.UTestModelPackageImpl#getUTestParallel()
		 * @generated
		 */
		EClass UTEST_PARALLEL = eINSTANCE.getUTestParallel();

		/**
		 * The meta object literal for the '<em><b>Paralles</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UTEST_PARALLEL__PARALLES = eINSTANCE.getUTestParallel_Paralles();

		/**
		 * The meta object literal for the '<em><b>Size</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute UTEST_PARALLEL__SIZE = eINSTANCE.getUTestParallel_Size();

		/**
		 * The meta object literal for the '{@link no.simula.se.testmodel.TestModel.impl.UTestItemImpl <em>UTest Item</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see no.simula.se.testmodel.TestModel.impl.UTestItemImpl
		 * @see no.simula.se.testmodel.TestModel.impl.UTestModelPackageImpl#getUTestItem()
		 * @generated
		 */
		EClass UTEST_ITEM = eINSTANCE.getUTestItem();

		/**
		 * The meta object literal for the '<em><b>Utestset</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UTEST_ITEM__UTESTSET = eINSTANCE.getUTestItem_Utestset();

		/**
		 * The meta object literal for the '<em><b>Attached Object</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UTEST_ITEM__ATTACHED_OBJECT = eINSTANCE.getUTestItem_AttachedObject();

		/**
		 * The meta object literal for the '{@link no.simula.se.testmodel.TestModel.impl.UMSpaceAImpl <em>UM Space A</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see no.simula.se.testmodel.TestModel.impl.UMSpaceAImpl
		 * @see no.simula.se.testmodel.TestModel.impl.UTestModelPackageImpl#getUMSpaceA()
		 * @generated
		 */
		EClass UM_SPACE_A = eINSTANCE.getUMSpaceA();

		/**
		 * The meta object literal for the '<em><b>Source</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UM_SPACE_A__SOURCE = eINSTANCE.getUMSpaceA_Source();

		/**
		 * The meta object literal for the '<em><b>Transitions</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UM_SPACE_A__TRANSITIONS = eINSTANCE.getUMSpaceA_Transitions();

		/**
		 * The meta object literal for the '<em><b>Ums</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UM_SPACE_A__UMS = eINSTANCE.getUMSpaceA_Ums();

		/**
		 * The meta object literal for the '{@link no.simula.se.testmodel.TestModel.impl.UTestModelImpl <em>UTest Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see no.simula.se.testmodel.TestModel.impl.UTestModelImpl
		 * @see no.simula.se.testmodel.TestModel.impl.UTestModelPackageImpl#getUTestModel()
		 * @generated
		 */
		EClass UTEST_MODEL = eINSTANCE.getUTestModel();

		/**
		 * The meta object literal for the '<em><b>Utestitem</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UTEST_MODEL__UTESTITEM = eINSTANCE.getUTestModel_Utestitem();

		/**
		 * The meta object literal for the '<em><b>Utestconfiguration</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UTEST_MODEL__UTESTCONFIGURATION = eINSTANCE.getUTestModel_Utestconfiguration();

		/**
		 * The meta object literal for the '<em><b>Attached Object</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UTEST_MODEL__ATTACHED_OBJECT = eINSTANCE.getUTestModel_AttachedObject();

		/**
		 * The meta object literal for the '{@link no.simula.se.testmodel.TestModel.impl.UTestConfigurationImpl <em>UTest Configuration</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see no.simula.se.testmodel.TestModel.impl.UTestConfigurationImpl
		 * @see no.simula.se.testmodel.TestModel.impl.UTestModelPackageImpl#getUTestConfiguration()
		 * @generated
		 */
		EClass UTEST_CONFIGURATION = eINSTANCE.getUTestConfiguration();

		/**
		 * The meta object literal for the '<em><b>Attached Object</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UTEST_CONFIGURATION__ATTACHED_OBJECT = eINSTANCE.getUTestConfiguration_AttachedObject();

	}

} //UTestModelPackage
