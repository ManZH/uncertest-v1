/*******************************************************************************
 * Copyright (c) 2016, 2017 Simula Research Laboratory. 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Simula license 
 * which accompanies this distribution, and is available at LICENSE.TXT
 *  
 *  Contributors:
 *        Man Zhang
 *******************************************************************************/
package no.simula.se.testing.minimization.rs;

import java.util.List;

import org.uma.jmetal.algorithm.Algorithm;
import org.uma.jmetal.problem.Problem;
import org.uma.jmetal.solution.Solution;


@SuppressWarnings("serial")
public class SimpleRS<S extends Solution<?>> implements Algorithm<List<S>> {
	
	
	private Problem<S> problem;
	private int maxEvaluations;
	SimpleSolutionListArchive<S> simpleSolution;

	final int numOfSolutions =  100;
	
	/** Constructor */
	public SimpleRS(Problem<S> problem, int maxEvaluations) {
		this.problem = problem;
		this.maxEvaluations = maxEvaluations;
		simpleSolution = new SimpleSolutionListArchive<S>();
	}

	/* Getter */
	public int getMaxEvaluations() {
		return maxEvaluations;
	}

	@Override
	public void run() {
	
		@SuppressWarnings("unused")
		int evaluations = 0;

		S newSolution;
		for (int i = 0; i < maxEvaluations; i++) {
			newSolution = problem.createSolution();
			problem.evaluate(newSolution);
			evaluations++;
			simpleSolution.add(newSolution);
		}
	}

	@Override
	public List<S> getResult() {
		return simpleSolution.getSolutionList();
	}

	@Override
	public String getName() {
		return "simpleRS";
	}

	@Override
	public String getDescription() {
		return "Multi-objective simple random search algorithm";
	}
}
