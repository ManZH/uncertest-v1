/*******************************************************************************
 * Copyright (c) 2016, 2017 Simula Research Laboratory. 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Simula license 
 * which accompanies this distribution, and is available at LICENSE.TXT
 *  
 *  Contributors:
 *        Man Zhang
 *******************************************************************************/
package no.simula.se.testing.minimization.rs;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import org.uma.jmetal.solution.Solution;
import org.uma.jmetal.util.archive.Archive;
import org.uma.jmetal.util.comparator.DominanceComparator;

@SuppressWarnings("serial")
public class SimpleSolutionListArchive<S extends Solution<?>> implements Archive<S> {

	private List<S> solutionList;
	private Comparator<S> dominanceComparator;
	private int maxSize = 100;

	public SimpleSolutionListArchive() {
		this(new DominanceComparator<S>());
	}

	public SimpleSolutionListArchive(DominanceComparator<S> comparator) {
		dominanceComparator = comparator;

		solutionList = new ArrayList<>();
	}
	
	public SimpleSolutionListArchive(DominanceComparator<S> comparator, int maxOfSolution) {
		dominanceComparator = comparator;

		solutionList = new ArrayList<>();
		
		maxSize = maxOfSolution;
	}

	@Override
	public boolean add(S solution) {
		if (size() < maxSize) {
			solutionList.add(solution);
		} else {
			int rand = (int) Math.random() * maxSize;

			find: for (int j = 0; j < size(); j++) {
				int index = (j + rand) % 100;
				S current = solutionList.get(index);
				int flag = dominanceComparator.compare(solution, current);

				if (flag == -1) { 
					solutionList.remove(index);
					solutionList.add(index, solution);
					break find;
				}
			}
		}

		return true;
	}

	@Override
	public S get(int index) {
		if (index < solutionList.size() - 1)
			return solutionList.get(index);
		return null;
	}

	@Override
	public List<S> getSolutionList() {
		return solutionList;
	}

	@Override
	public int size() {
		return solutionList.size();
	}

	public Comparator<S> getDominanceComparator() {
		return dominanceComparator;
	}

}
