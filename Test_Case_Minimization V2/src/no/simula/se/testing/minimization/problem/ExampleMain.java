package no.simula.se.testing.minimization.problem;

import java.io.FileNotFoundException;

import org.eclipse.emf.ecore.EObject;

import no.simula.se.testing.minimization.runner.UMinRunner;
import no.simula.se.testing.minimization.runner.UnNSGAIIRunner;
import no.simula.se.testing.utility.ModelUtil;
import no.simula.se.testmodel.TestModel.UTestSet;

public class ExampleMain {

	public static void main(String[] args) throws FileNotFoundException {
		
		UMinRunner.CASE = "Case Study Name";
		
		String uml_url = "uml_model";
		String emf_rul = "emf_model";
		ModelUtil.getInstance();
		EObject[] root = ModelUtil.loadUTestModels(uml_url, emf_rul);
	
		
		UMinRunner.USE_CASE = "Use Case Name";
		// get abstract test set generated by test case abstract generator
		UTestSet set = ModelUtil.getUTestSet(root[1], "Use Case Name in model");
		
		// specify a problem, i.e., number of uncertainties  
		int[] objs_array = {1,0,1,0,0,1,0};
				
		UnNSGAIIRunner nsgaii = new UnNSGAIIRunner();
		
		
		nsgaii.run(set, objs_array, "MIN_NumUn", "");
		
	}

}
