/*******************************************************************************
 * Copyright (c) 2016, 2017 Simula Research Laboratory. 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Simula license 
 * which accompanies this distribution, and is available at LICENSE.TXT
 *  
 *  Contributors:
 *        Man Zhang
 *******************************************************************************/
package no.simula.se.testing.minimization.problem;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.uml2.uml.StateMachine;
import org.eclipse.uml2.uml.Transition;
import org.uma.jmetal.problem.impl.AbstractDoubleProblem;
import org.uma.jmetal.solution.DoubleSolution;

import no.simula.se.testing.minimization.solution.UnDoubleSolution;
import no.simula.se.testing.utility.belief.BeliefUtility;
import no.simula.se.testing.utility.belief.BeliefUtilityException;
import no.simula.se.testmodel.TestModel.UMSpaceA;
import no.simula.se.testmodel.TestModel.UTestCase;
import no.simula.se.testmodel.TestModel.UTestPath;
import no.simula.se.testmodel.TestModel.UTestSet;

public class UncertaintyProblem extends AbstractDoubleProblem {

	private static final long serialVersionUID = 613632458548579685L;

	private UTestSet set;
	private int[] objs;

	public UncertaintyProblem() {

	}

	public void initial() {
		if (set != null && objs != null) {
			setNumberOfVariables(this.set.getUtestcase().size());
			int objleng = 0;
			for (int obj : this.objs) {
				if (obj == 1)
					objleng++;
			}
			setNumberOfObjectives(objleng);

			List<Double> lowerLimit = new ArrayList<>(getNumberOfVariables());
			List<Double> upperLimit = new ArrayList<>(getNumberOfVariables());

			for (int i = 0; i < getNumberOfVariables(); i++) {
				lowerLimit.add(0.0);
				upperLimit.add(1.0);
			}

			setLowerLimit(lowerLimit);
			setUpperLimit(upperLimit);
		}

	}

	/**
	 * This is used to define Uncertainty Prolem
	 * @param tset specify the info of sut
	 * @param tobjs	specify the employed objectives which can be configured with an array
	 *              0 -- a number of test cases
	 *              1 -- uncertainty measure
	 *              2 -- a number of uncertainties
	 *              3 -- ratio of uncertainty in terms of the length of test cases [not applied]
	 *              4 -- a number of unique uncertainties or coverage of uncertainties
	 *              5 -- transitions
	 *              6 -- uncertainty space
	 * @param pName specify a name of the problem
	 *
	 * e.g., in UncerTest paper, we defined four uncertainty-wise minimization problems as below:
	 *              int[][] objs_array = {
	 *                {1,0,1,0,0,1,0},// number of uncertainty
	 *                {1,0,0,0,0,1,1},// uncertainty space
	 *                {1,1,0,0,0,1,0},// uncertainty measure
	 *                {1,0,0,0,1,1,0} // number of unique uncertainty
	 * 		};
	 */
	public UncertaintyProblem(UTestSet tset, int[] tobjs, String pName) {
		this.set = tset;

		setNumberOfVariables(tset.getUtestcase().size());
		setNumberOfObjectives(tobjs.length);
		setName(pName);

		List<Double> lowerLimit = new ArrayList<>(getNumberOfVariables());
		List<Double> upperLimit = new ArrayList<>(getNumberOfVariables());

		for (int i = 0; i < getNumberOfVariables(); i++) {
			lowerLimit.add(0.0);
			upperLimit.add(1.0);
		}

		setLowerLimit(lowerLimit);
		setUpperLimit(upperLimit);
	}

	@Override
	public void evaluate(DoubleSolution solution) {
		// 0 - #test case; 1 - #uncertainty; 2 - ratio of uncertainty in terms
		// of total uncertainties; 3 - ration of uncertainty in terms of the
		// length of test cases
		// 4 - all uncertainty; 5 - all transitions

		double[] x = new double[solution.getNumberOfVariables()];

		BeliefUtility.getInstance();
		Set<EObject> all_u = null;
		Set<Transition> all_t = null;
		Set<Transition> included = null;
		Set<EObject> included_uncertaintes = null;
		if (objs[5] == 1) {
			included = new HashSet<Transition>();
		}
		if (objs[6] == 1)
			included_uncertaintes = new HashSet<EObject>();
		try {
			all_u = BeliefUtility.getUncertainties((StateMachine) set.getAttachedObject());
			all_t = BeliefUtility.getAllOwnedTransition((StateMachine) set.getAttachedObject());

			double total_u = all_u.size();
			// double total_t = all_t.size();

			double numOfTC = 0;
			double degree = 0.0;
			double numOfUncer = 0;
			double ratioOfUncer = 0.0;

			for (int i = 0; i < solution.getNumberOfVariables(); i++) {
				x[i] = solution.getVariableValue(i);
				if (x[i] > 0.5) {
					numOfTC++;
					UTestCase ut = set.getUtestcase().get(i);
					if (ut instanceof UTestPath) {
						degree = degree + ((UTestPath) ut).getMeasuredValue();
					}
					if (objs[4] == 1)
						all_u.removeAll(BeliefUtility.getUncertainties(ut));
					if (objs[5] == 1)
						included.addAll(BeliefUtility.getAllOwnedTransition(ut));
					if (objs[6] == 1)
						included_uncertaintes.addAll(BeliefUtility.getUncertainties(ut));
					numOfUncer = numOfUncer + ut.getUinfo().get(0) / (ut.getUinfo().get(0) + 1);
					ratioOfUncer = ratioOfUncer + ut.getUinfo().get(2);
				}
			}

			double total = solution.getNumberOfVariables();

			int i = 0;
			if (objs[0] == 1) {
				solution.setObjective(i, numOfTC / total);
				i++;
			}
			if (objs[1] == 1) {
				solution.setObjective(i, 1.0 - (degree / numOfTC));
				i++;
			}
			if (objs[2] == 1) {
				solution.setObjective(i, 1.0 - (numOfUncer / numOfTC));
				i++;
			}
			if (objs[3] == 1) {
				solution.setObjective(i, 1.0 - (ratioOfUncer / numOfTC));
				i++;
			}
			if (objs[4] == 1) {
				double final_u = all_u.size();
				solution.setObjective(i, final_u / total_u);
				i++;
			}
			if (objs[5] == 1) {
				// double final_t = all_t.size();
				// FIXME Man Zhang: old version
				// double precen_t =
				// BeliefUtility.getPrentageOfTransition(all_t, included);
				double precen_t = (included.size() * 1.0) / all_t.size();
				solution.setObjective(i, 1.0 - precen_t);
				i++;
			}
			if (objs[6] == 1) {
				double total_sm = set.getUmspacea().size();
				Set<UMSpaceA> totals = new HashSet<UMSpaceA>();
				for (Object obj : included_uncertaintes) {
					for (UMSpaceA sm : set.getUmspacea()) {
						if (sm.getUms().contains(obj))
							totals.add(sm);
					}
				}
				double included_sm = totals.size() * 1.0;
				solution.setObjective(i, 1.0 - included_sm / total_sm);
				i++;
			}
		} catch (BeliefUtilityException e) {
			e.printStackTrace();
		}
	}

	public void setSet(UTestSet set) {
		this.set = set;
	}

	public void setObjs(int[] objs) {
		this.objs = objs;
	}

	public void setProblemName(String pName) {
		this.setName(pName);
	}

	@Override
	public DoubleSolution createSolution() {
		return new UnDoubleSolution(this);
	}
}
