/*******************************************************************************
 * Copyright (c) 2016, 2017 Simula Research Laboratory. 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Simula license 
 * which accompanies this distribution, and is available at LICENSE.TXT
 *  
 *  Contributors:
 *        Man Zhang
 *******************************************************************************/
package no.simula.se.testing.minimization.solution;

import org.uma.jmetal.problem.DoubleProblem;
import org.uma.jmetal.solution.impl.DefaultDoubleSolution;

public class UnDoubleSolution extends DefaultDoubleSolution {

	private static final long serialVersionUID = -2488501809693855502L;

	public UnDoubleSolution(UnDoubleSolution solution) {
		super(solution);
	}

	public UnDoubleSolution(DoubleProblem problem) {
		super(problem);
	}
	
	@Override
	public String getVariableValueString(int index) {
		if(getVariableValue(index) <= 0.5) return "0";
		return "1";
	}

	@Override
	public UnDoubleSolution copy() {
		return new UnDoubleSolution(this);
	}
}
