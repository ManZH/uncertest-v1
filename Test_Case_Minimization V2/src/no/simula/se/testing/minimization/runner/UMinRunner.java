/*******************************************************************************
 * Copyright (c) 2016, 2017 Simula Research Laboratory. 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Simula license 
 * which accompanies this distribution, and is available at LICENSE.TXT
 *  
 *  Contributors:
 *        Man Zhang
 *******************************************************************************/
package no.simula.se.testing.minimization.runner;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.uma.jmetal.solution.DoubleSolution;
import org.uma.jmetal.util.JMetalLogger;
import org.uma.jmetal.util.fileoutput.SolutionListOutput;
import org.uma.jmetal.util.fileoutput.impl.DefaultFileOutputContext;
import org.uma.jmetal.util.pseudorandom.JMetalRandom;

public class UMinRunner {
	public final static String UNCERTEST_MIN_PROBLEM = "no.simula.se.testing.minimization.problem.UncertaintyProblem";
	public static String OUTPUT_PATH = "uncertest_min_analyzer";
	
	public static String CASE = "";
	public static String USE_CASE = "";
	public static String ALGO = "";
	
	public static int times;
	
	public static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
	
	public static void printDoubleSolution(List<DoubleSolution> population, String pName, long computingTime){
		
		String path = OUTPUT_PATH + "/";
		if(!CASE.equals("")){
			path = path + CASE +"/";
		}
		if(!USE_CASE.equals("")){
			path = path + USE_CASE +"/";
		}
		if(!ALGO.equals("")){
			path = path + ALGO +"/";
		}
		
		File directory = new File(path);
	    if (! directory.exists()){
	        directory.mkdirs();
	    }
		
		String var = path + pName+"_"+formatNum(times)+"_VAR.tsv";
		String fun = path + pName+"_"+formatNum(times)+"_FUN.tsv";
		String log = path + pName+"_log.log";
		
		Path plog = Paths.get(log);
		
		try {
			if(!Files.exists(plog)){
				Files.createFile(plog);
			}
			
			Files.write(plog, (LocalDateTime.now().format(formatter)+" "+ formatNum(times)+" "+ computingTime+"ms"+System.getProperty("line.separator")).getBytes(), StandardOpenOption.APPEND);
			
		} catch (IOException e) {
			e.printStackTrace();
		}

		
		DefaultFileOutputContext varContext = new DefaultFileOutputContext(var);
		varContext.setSeparator("");
		new SolutionListOutput(population)
        .setSeparator("\t")
        .setVarFileOutputContext(varContext)
        .setFunFileOutputContext(new DefaultFileOutputContext(fun))
        .print();

	    JMetalLogger.logger.info("Random seed: " + JMetalRandom.getInstance().getSeed());
	}
	
	public static void printDoubleSolution(List<DoubleSolution> population, String pName){
		
		String path = OUTPUT_PATH + "/";
		if(!CASE.equals("")){
			path = path + CASE +"/";
		}
		if(!USE_CASE.equals("")){
			path = path + USE_CASE +"/";
		}
		if(!ALGO.equals("")){
			path = path + ALGO +"/";
		}
		
		File directory = new File(path);
	    if (! directory.exists()){
	        directory.mkdirs();
	    }
		
		String var = path + pName+"_"+formatNum(times)+"_VAR.tsv";
		String fun = path + pName+"_"+formatNum(times)+"_FUN.tsv";

		
		DefaultFileOutputContext varContext = new DefaultFileOutputContext(var);
		varContext.setSeparator("");
		new SolutionListOutput(population)
        .setSeparator("\t")
        .setVarFileOutputContext(varContext)
        .setFunFileOutputContext(new DefaultFileOutputContext(fun))
        .print();

	    JMetalLogger.logger.info("Random seed: " + JMetalRandom.getInstance().getSeed());

	}
	
	
	public static String formatNum(int number){
		return String.format("%03d", number);
	}
}
