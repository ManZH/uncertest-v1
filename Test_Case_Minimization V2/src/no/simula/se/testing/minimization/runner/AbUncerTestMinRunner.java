/*******************************************************************************
 * Copyright (c) 2016, 2017 Simula Research Laboratory. 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Simula license 
 * which accompanies this distribution, and is available at LICENSE.TXT
 *  
 *  Contributors:
 *        Man Zhang
 *******************************************************************************/
package no.simula.se.testing.minimization.runner;

import java.io.FileNotFoundException;

import org.uma.jmetal.runner.AbstractAlgorithmRunner;

import no.simula.se.testmodel.TestModel.UTestSet;

public class AbUncerTestMinRunner extends AbstractAlgorithmRunner {

	
	public void run(UTestSet set, int[] objs, String pName, String referenceParetoFront) throws FileNotFoundException{
	}
	
	public void run(UTestSet set, int[] objs, String pName, String problemName, String referenceParetoFront) throws FileNotFoundException{
		
	}
}
