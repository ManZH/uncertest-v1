/*******************************************************************************
 * Copyright (c) 2016, 2017 Simula Research Laboratory. 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Simula license 
 * which accompanies this distribution, and is available at LICENSE.TXT
 *  
 *  Contributors:
 *        Man Zhang
 *******************************************************************************/
package no.simula.se.testing.minimization.runner;

import java.io.FileNotFoundException;
import java.util.List;

import org.uma.jmetal.algorithm.Algorithm;
import org.uma.jmetal.algorithm.multiobjective.nsgaii.NSGAIIBuilder;
import org.uma.jmetal.operator.CrossoverOperator;
import org.uma.jmetal.operator.MutationOperator;
import org.uma.jmetal.operator.SelectionOperator;
import org.uma.jmetal.operator.impl.crossover.SBXCrossover;
import org.uma.jmetal.operator.impl.mutation.PolynomialMutation;
import org.uma.jmetal.operator.impl.selection.BinaryTournamentSelection;
import org.uma.jmetal.problem.Problem;
import org.uma.jmetal.solution.DoubleSolution;
import org.uma.jmetal.util.AlgorithmRunner;
import org.uma.jmetal.util.JMetalLogger;
import org.uma.jmetal.util.ProblemUtils;
import org.uma.jmetal.util.comparator.RankingAndCrowdingDistanceComparator;

import no.simula.se.testing.minimization.problem.UncertaintyProblem;
import no.simula.se.testmodel.TestModel.UTestSet;


public class UnNSGAIIRunner extends AbUncerTestMinRunner {
	
	public final String algoName = "NSGA2";
	
	public void run(UTestSet set, int[] objs, String pName, String referenceParetoFront) throws FileNotFoundException{
		UMinRunner.ALGO = algoName;
		run(set, objs, pName, UMinRunner.UNCERTEST_MIN_PROBLEM, referenceParetoFront);
	}
	
	public void run(UTestSet set, int[] objs, String pName, String problemName, String referenceParetoFront) throws FileNotFoundException{
		UMinRunner.ALGO = algoName;
		
		Problem<DoubleSolution> problem; // the type of the problem, e.g., int, byte. For solving uncertainty minimization, i used double. for more info, please conduct jmetal 5 specification
	    Algorithm<List<DoubleSolution>> algorithm; // set the algorithm with the type
	    CrossoverOperator<DoubleSolution> crossover; // set the crossover operator with the type,
	    MutationOperator<DoubleSolution> mutation; // set the mutation operator with the type,
	    SelectionOperator<List<DoubleSolution>, DoubleSolution> selection; // set the selection operator, with the type
	    

	    //load the problem, if you create other problem, you may extend Problem class, please check UncertaintyProblem class.
	    problem = ProblemUtils.<DoubleSolution> loadProblem(problemName);

	    if(problem instanceof UncertaintyProblem){
	    	((UncertaintyProblem)problem).setSet(set);
	    	((UncertaintyProblem)problem).setObjs(objs);
	    	((UncertaintyProblem)problem).setProblemName(pName);
	    	((UncertaintyProblem)problem).initial();
	    }
	    
	    //crossover setting
	    double crossoverProbability = 0.9 ;
	    double crossoverDistributionIndex = 20.0 ;
	    crossover = new SBXCrossover(crossoverProbability, crossoverDistributionIndex) ;

	    //mutation setting
	    double mutationProbability = 1.0 / problem.getNumberOfVariables() ;
	    double mutationDistributionIndex = 20.0 ;
	    mutation = new PolynomialMutation(mutationProbability, mutationDistributionIndex) ;

	    //selection setting
	    selection = new BinaryTournamentSelection<DoubleSolution>(
	        new RankingAndCrowdingDistanceComparator<DoubleSolution>());

	    //algorithm setting
	    algorithm = new NSGAIIBuilder<DoubleSolution>(problem, crossover, mutation)
	        .setSelectionOperator(selection)
	        .setMaxEvaluations(25000) // times of evaluation
	        .setPopulationSize(100) // population size
	        .build() ;

	    AlgorithmRunner algorithmRunner = new AlgorithmRunner.Executor(algorithm)
	        .execute() ;

	    List<DoubleSolution> population = algorithm.getResult() ;
	    long computingTime = algorithmRunner.getComputingTime() ;

	    JMetalLogger.logger.info("Total execution time: " + computingTime + "ms");
		
		UMinRunner.printDoubleSolution(population, pName,computingTime);
		//printFinalSolutionSet(population);
		if (!referenceParetoFront.equals("")) {
			printQualityIndicators(population, referenceParetoFront);
		}
	}

  
}
