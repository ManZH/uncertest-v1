/*******************************************************************************
 * Copyright (c) 2016, 2017 Simula Research Laboratory. 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Simula license 
 * which accompanies this distribution, and is available at LICENSE.TXT
 *  
 *  Contributors:
 *        Man Zhang
 *******************************************************************************/
package no.simula.se.testing.minimization.runner;

import java.io.FileNotFoundException;
import java.util.List;

import org.uma.jmetal.algorithm.Algorithm;
import org.uma.jmetal.algorithm.multiobjective.abyss.ABYSSBuilder;
import org.uma.jmetal.problem.DoubleProblem;
import org.uma.jmetal.problem.Problem;
import org.uma.jmetal.solution.DoubleSolution;
import org.uma.jmetal.util.AlgorithmRunner;
import org.uma.jmetal.util.JMetalLogger;
import org.uma.jmetal.util.ProblemUtils;
import org.uma.jmetal.util.archive.Archive;
import org.uma.jmetal.util.archive.impl.CrowdingDistanceArchive;

import no.simula.se.testing.minimization.problem.UncertaintyProblem;
import no.simula.se.testmodel.TestModel.UTestSet;


public class UnABYSSRunner extends AbUncerTestMinRunner {

	public final String algoName = "ABYSS";

	public void run(UTestSet set, int[] objs, String pName, String referenceParetoFront) throws FileNotFoundException {
		UMinRunner.ALGO = algoName;
		run(set, objs, pName, UMinRunner.UNCERTEST_MIN_PROBLEM, referenceParetoFront);
	}

	public void run(UTestSet set, int[] objs, String pName, String problemName, String referenceParetoFront)
			throws FileNotFoundException {
		UMinRunner.ALGO = algoName;

		Problem<DoubleSolution> problem;
		Algorithm<List<DoubleSolution>> algorithm;
//		CrossoverOperator<DoubleSolution> crossover;
//		MutationOperator<DoubleSolution> mutation;
//		SelectionOperator<List<DoubleSolution>, DoubleSolution> selection;

		problem = ProblemUtils.<DoubleSolution> loadProblem(problemName);

		if (problem instanceof UncertaintyProblem) {
			((UncertaintyProblem) problem).setSet(set);
			((UncertaintyProblem) problem).setObjs(objs);
			((UncertaintyProblem) problem).setProblemName(pName);
			((UncertaintyProblem) problem).initial();
		}

		Archive<DoubleSolution> archive = new CrowdingDistanceArchive<DoubleSolution>(100) ;

	    algorithm = new ABYSSBuilder((DoubleProblem)problem, archive)
	        .setMaxEvaluations(25000)
	        .build();

	    AlgorithmRunner algorithmRunner = new AlgorithmRunner.Executor(algorithm)
	        .execute();

		List<DoubleSolution> population = algorithm.getResult();
		long computingTime = algorithmRunner.getComputingTime();

		JMetalLogger.logger.info("Total execution time: " + computingTime + "ms");

		UMinRunner.printDoubleSolution(population, pName, computingTime);
		// printFinalSolutionSet(population);
		if (!referenceParetoFront.equals("")) {
			printQualityIndicators(population, referenceParetoFront);
		}
	}
}
