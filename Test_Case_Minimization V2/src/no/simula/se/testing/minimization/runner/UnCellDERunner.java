/*******************************************************************************
 * Copyright (c) 2016, 2017 Simula Research Laboratory. 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Simula license 
 * which accompanies this distribution, and is available at LICENSE.TXT
 *  
 *  Contributors:
 *        Man Zhang
 *******************************************************************************/

package no.simula.se.testing.minimization.runner;

import java.io.FileNotFoundException;
import java.util.List;

import org.uma.jmetal.algorithm.Algorithm;
import org.uma.jmetal.algorithm.multiobjective.cellde.CellDE45;
import org.uma.jmetal.operator.SelectionOperator;
import org.uma.jmetal.operator.impl.crossover.DifferentialEvolutionCrossover;
import org.uma.jmetal.operator.impl.selection.BinaryTournamentSelection;
import org.uma.jmetal.problem.Problem;
import org.uma.jmetal.solution.DoubleSolution;
import org.uma.jmetal.util.AlgorithmRunner;
import org.uma.jmetal.util.JMetalLogger;
import org.uma.jmetal.util.ProblemUtils;
import org.uma.jmetal.util.archive.impl.CrowdingDistanceArchive;
import org.uma.jmetal.util.comparator.RankingAndCrowdingDistanceComparator;
import org.uma.jmetal.util.evaluator.impl.SequentialSolutionListEvaluator;
import org.uma.jmetal.util.neighborhood.impl.C9;

import no.simula.se.testing.minimization.problem.UncertaintyProblem;
import no.simula.se.testmodel.TestModel.UTestSet;

public class UnCellDERunner extends AbUncerTestMinRunner {

	public final String algoName = "CellDE";

	public void run(UTestSet set, int[] objs, String pName, String referenceParetoFront) throws FileNotFoundException {
		UMinRunner.ALGO = algoName;
		run(set, objs, pName, UMinRunner.UNCERTEST_MIN_PROBLEM, referenceParetoFront);
	}

	public void run(UTestSet set, int[] objs, String pName, String problemName, String referenceParetoFront)
			throws FileNotFoundException {
		UMinRunner.ALGO = algoName;

		Problem<DoubleSolution> problem;
		Algorithm<List<DoubleSolution>> algorithm;
		SelectionOperator<List<DoubleSolution>, DoubleSolution> selection;
		DifferentialEvolutionCrossover crossover;

		problem = ProblemUtils.<DoubleSolution> loadProblem(problemName);

		if (problem instanceof UncertaintyProblem) {
			((UncertaintyProblem) problem).setSet(set);
			((UncertaintyProblem) problem).setObjs(objs);
			((UncertaintyProblem) problem).setProblemName(pName);
			((UncertaintyProblem) problem).initial();
		}

		double cr = 0.5;
		double f = 0.5;

		crossover = new DifferentialEvolutionCrossover(cr, f, "rand/1/bin");

		selection = new BinaryTournamentSelection<DoubleSolution>(
				new RankingAndCrowdingDistanceComparator<DoubleSolution>());

		algorithm = new CellDE45(problem, 25000, 100, new CrowdingDistanceArchive<DoubleSolution>(100),
				new C9<DoubleSolution>((int) Math.sqrt(100), (int) Math.sqrt(100)), selection, crossover, 20,
				new SequentialSolutionListEvaluator<DoubleSolution>());

		AlgorithmRunner algorithmRunner = new AlgorithmRunner.Executor(algorithm).execute();

		List<DoubleSolution> population = algorithm.getResult();
		long computingTime = algorithmRunner.getComputingTime();

		JMetalLogger.logger.info("Total execution time: " + computingTime + "ms");

		UMinRunner.printDoubleSolution(population, pName,computingTime);
		// printFinalSolutionSet(population);
		if (!referenceParetoFront.equals("")) {
			printQualityIndicators(population, referenceParetoFront);
		}
	}

}
