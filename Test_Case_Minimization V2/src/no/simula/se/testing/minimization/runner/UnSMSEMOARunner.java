/*******************************************************************************
 * Copyright (c) 2016, 2017 Simula Research Laboratory. 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Simula license 
 * which accompanies this distribution, and is available at LICENSE.TXT
 *  
 *  Contributors:
 *        Man Zhang
 *******************************************************************************/

package no.simula.se.testing.minimization.runner;

import java.io.FileNotFoundException;
import java.util.List;

import org.uma.jmetal.algorithm.Algorithm;
import org.uma.jmetal.algorithm.multiobjective.smsemoa.SMSEMOABuilder;
import org.uma.jmetal.operator.CrossoverOperator;
import org.uma.jmetal.operator.MutationOperator;
import org.uma.jmetal.operator.SelectionOperator;
import org.uma.jmetal.operator.impl.crossover.SBXCrossover;
import org.uma.jmetal.operator.impl.mutation.PolynomialMutation;
import org.uma.jmetal.operator.impl.selection.RandomSelection;
import org.uma.jmetal.problem.DoubleProblem;
import org.uma.jmetal.qualityindicator.impl.Hypervolume;
import org.uma.jmetal.qualityindicator.impl.hypervolume.PISAHypervolume;
import org.uma.jmetal.solution.DoubleSolution;
import org.uma.jmetal.util.AlgorithmRunner;
import org.uma.jmetal.util.JMetalLogger;
import org.uma.jmetal.util.ProblemUtils;

import no.simula.se.testing.minimization.problem.UncertaintyProblem;
import no.simula.se.testmodel.TestModel.UTestSet;

public class UnSMSEMOARunner extends AbUncerTestMinRunner {

	public final String algoName = "SMSEMOA";

	public void run(UTestSet set, int[] objs, String pName, String referenceParetoFront) throws FileNotFoundException {
		UMinRunner.ALGO = algoName;
		run(set, objs, pName, UMinRunner.UNCERTEST_MIN_PROBLEM, referenceParetoFront);
	}

	public void run(UTestSet set, int[] objs, String pName, String problemName, String referenceParetoFront)
			throws FileNotFoundException {
		UMinRunner.ALGO = algoName;

		DoubleProblem problem;
		Algorithm<List<DoubleSolution>> algorithm;
		CrossoverOperator<DoubleSolution> crossover;
		MutationOperator<DoubleSolution> mutation;
		SelectionOperator<List<DoubleSolution>, DoubleSolution> selection;

		problem = (DoubleProblem) ProblemUtils.<DoubleSolution> loadProblem(problemName);

		if (problem instanceof UncertaintyProblem) {
			((UncertaintyProblem) problem).setSet(set);
			((UncertaintyProblem) problem).setObjs(objs);
			((UncertaintyProblem) problem).setProblemName(pName);
			((UncertaintyProblem) problem).initial();
		}

		double crossoverProbability = 0.9;
		double crossoverDistributionIndex = 20.0;
		crossover = new SBXCrossover(crossoverProbability, crossoverDistributionIndex);

		double mutationProbability = 1.0 / problem.getNumberOfVariables();
		double mutationDistributionIndex = 20.0;
		mutation = new PolynomialMutation(mutationProbability, mutationDistributionIndex);

		selection = new RandomSelection<DoubleSolution>();

		Hypervolume<DoubleSolution> hypervolume;
		hypervolume = new PISAHypervolume<>();
		hypervolume.setOffset(100.0);

		algorithm = new SMSEMOABuilder<DoubleSolution>(problem, crossover, mutation).setSelectionOperator(selection)
				.setMaxEvaluations(25000).setPopulationSize(100).setHypervolumeImplementation(hypervolume).build();

		AlgorithmRunner algorithmRunner = new AlgorithmRunner.Executor(algorithm).execute();

		List<DoubleSolution> population = algorithm.getResult();
		long computingTime = algorithmRunner.getComputingTime();

		JMetalLogger.logger.info("Total execution time: " + computingTime + "ms");

		UMinRunner.printDoubleSolution(population, pName,computingTime);
		// printFinalSolutionSet(population);
		if (!referenceParetoFront.equals("")) {
			printQualityIndicators(population, referenceParetoFront);
		}
	}

}
