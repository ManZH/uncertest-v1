/*******************************************************************************
 * Copyright (c) 2016, 2017 Simula Research Laboratory. 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Simula license 
 * which accompanies this distribution, and is available at LICENSE.TXT
 *  
 *  Contributors:
 *        Man Zhang
 *******************************************************************************/
package no.simula.se.testing.generator.eval.ocl;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TestOCLPattern {
	
	public static TestOCLPattern instance;
	
	public static TestOCLPattern getInstance(){
		if(instance == null) instance = new TestOCLPattern();
		return instance;
	}

	public static void main(String[] args) {
		String text = "self.isPositionUpdated and (not equalPosition(lat, lon))";
		System.out.println(parseVar(text));
	}
	
	public static String parseVar(String text){
		String var_pattern = "(\\w+.\\s?)?\\w+";
		String query_pattern = "\\(("+var_pattern+",)*"+var_pattern+"\\)";
		String fliter_pattern = "(?!self.)\\w+";
		Pattern r1 = Pattern.compile(query_pattern);
		
		Matcher m = r1.matcher(text);
		while(m.find()){
			//System.out.println(m.group());
			String result = m.group();
			String replace = m.group().replaceAll(" ", "");
			for(String str: replace.substring(1, replace.length()-1).split(",")){
				//System.out.println("-"+str+" "+ str.matches(fliter_pattern));
				if(str.matches(fliter_pattern))
					replace = replace.replace(str, "\"+"+str+"+\"");
			}
			text = text.replace(result, replace);
			
		}
		return text;
	}

}
