/*******************************************************************************
 * Copyright (c) 2016, 2017 Simula Research Laboratory. 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Simula license 
 * which accompanies this distribution, and is available at LICENSE.TXT
 *  
 *  Contributors:
 *        Man Zhang
 *******************************************************************************/
package no.simula.se.testing.generator.eval.ocl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Constraint;
import org.eclipse.uml2.uml.InstanceSpecification;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.State;
import org.eclipse.uml2.uml.Transition;

import no.simula.se.testing.utility.MOCLUtil;
import no.simula.se.testing.utility.ModelUtil;

public abstract class StatementToJunit {
	
	public static final String ST_TO_JUNIT = "ST_TO_JUNIT";
	public static final String SM_NAME = "";
	public static final String SIMPLE_ST_NAME = "";
	public static final String TRIGGER_NAME = "";
	public static final String CONSTRAINT_NAME = "";
	public static final String EFFECT_NAME = "";
	public static final String FINAL_ST_NAME = "";
	
	public static boolean islogger = false;
	
	public static String printCondition(String comment, String var_Instance, String constraint){
		String result = "";
		result= result + "System.out.println(\""+comment+"\"+MOCLUtil.getInstance().evaluateOCL("+var_Instance+".eClass(), \""+TestOCLPattern.parseVar(constraint)+"\", "+var_Instance+"));";
		return result;
	}
	
	public static String reportUncertaintyCondition(String comment, String var_Instance, String constraint){
		String result = "";
		result= result + "unReporter.reportUncerInfo(\""+comment+"\"+MOCLUtil.getInstance().evaluateOCL("+var_Instance+".eClass(), \""+TestOCLPattern.parseVar(constraint)+"\", "+var_Instance+"));";
		return result;
	}
	
	public static String getCondition(String comment, String var_Instance, String constraint){
		String result = "";
		result= result + "MOCLUtil.getInstance().evaluateOCL("+var_Instance+".eClass(), \""+TestOCLPattern.parseVar(constraint)+"\", "+var_Instance+")";
		return result;
	}
	
	// ownerClassifier is State, object is Constraint
	public static final StatementToJunit STATE_INVARIANT = new StatementToJunit("state_stateInvariant", SIMPLE_ST_NAME, CONSTRAINT_NAME){

		@Override
		public boolean validate(EObject owner, EObject object) {
			boolean result = (owner instanceof State && ((State)owner).isSimple() && object instanceof Constraint);
			if(((Constraint)object).getContext() != null && ModelUtil.checkContextOfConstraint((Constraint)object)){
				result = result && MOCLUtil.getInstance().validateOCL((Classifier)((Constraint)object).getContext(), ModelUtil.getBodyConstraint((Constraint)object));
			}//FIXME set the context by default
			return result;
		}

		@Override
		public boolean evaluate(Model umlModel, EObject object, EObject instance) {
			EObject context = ((Constraint)object).getContext();
			if(context instanceof Classifier){
				return MOCLUtil.getInstance().evaluateOCL((Classifier)context, ModelUtil.getBodyConstraint((Constraint)object), instance);
			}
			return false;//FIXME -- check the context for the operation and others
		}

		@Override
		public List<String> toJunit(Model umlModel, String var_Instance, EObject object) {//var_Insrance is the instance of clazz or operation, object is constraint
			List<String> result = new ArrayList<String>();
			result.add("//parse "+ object);
			String body = ModelUtil.getBodyConstraint((Constraint)object);
			TestOCLPattern.getInstance();
			if(islogger)
				result.add("assertTrue(getCurrentInfo(), MOCLUtil.getInstance().evaluateOCL("+var_Instance+".eClass(), \""+TestOCLPattern.parseVar(body)+"\", "+var_Instance+"));");
			else
				result.add("assertTrue(MOCLUtil.getInstance().evaluateOCL("+var_Instance+".eClass(), \""+TestOCLPattern.parseVar(body)+"\", "+var_Instance+"));");
			return result;
		}
		@Override
		public List<String> toJunit2(Model umlModel, String var_Instance, String constraint) {//var_Insrance is the instance of clazz or operation, object is constraint
			List<String> result = new ArrayList<String>();
			if(islogger)
				result.add("assertTrue(getCurrentInfo(), MOCLUtil.getInstance().evaluateOCL("+var_Instance+".eClass(), \""+TestOCLPattern.parseVar(constraint)+"\", "+var_Instance+"));");
			else
				result.add("assertTrue(MOCLUtil.getInstance().evaluateOCL("+var_Instance+".eClass(), \""+TestOCLPattern.parseVar(constraint)+"\", "+var_Instance+"));");
			return result;
		}
		
		@Override
		public List<String> toJunit2(Model umlModel, String var_Instance, String constraint, String info) {//var_Insrance is the instance of clazz or operation, object is constraint
			List<String> result = new ArrayList<String>();
			result.add("assertTrue(\""+info+"\", MOCLUtil.getInstance().evaluateOCL("+var_Instance+".eClass(), \""+TestOCLPattern.parseVar(constraint)+"\", "+var_Instance+"));");
			
			return result;
		}
		
		@Override
		public List<String> toJunitCheckThat(Model umlModel, String var_Instance, String constraint, String loggerInfo, String correcter){
			List<String> result = new ArrayList<String>();
			result.add(correcter+".checkThat(\""+loggerInfo+"\", MOCLUtil.getInstance().evaluateOCL("+var_Instance+".eClass(), \""+TestOCLPattern.parseVar(constraint)+"\", "+var_Instance+"),"
					+ "IsEqual.equalTo(true));");
			return result;
		}
	};
	
	//FIXME notice the global recongization does not work! 
	public static final StatementToJunit CHANGE_EVENT = new StatementToJunit("change_event", SIMPLE_ST_NAME, CONSTRAINT_NAME){

		@Override
		public boolean validate(EObject owner, EObject object) {
			boolean result = (owner instanceof Transition && object instanceof Constraint);
			if(((Constraint)object).getContext() != null && ModelUtil.checkContextOfConstraint((Constraint)object)){
				result = result && MOCLUtil.getInstance().validateOCL((Classifier)((Constraint)object).getContext(), ModelUtil.getBodyConstraint((Constraint)object));
			}
			return result;
		}

		@Override
		public boolean evaluate(Model umlModel, EObject object, EObject instance) {
			EObject context = ((Constraint)object).getContext();
			if(context instanceof Classifier){
				return MOCLUtil.getInstance().evaluateOCL((Classifier)context, ModelUtil.getBodyConstraint((Constraint)object), instance);
			}
			return false;//FIXME -- check the context for the operation and others
		}

		@Override
		public List<String> toJunit(Model umlModel, String var_Instance, EObject object) {//var_Insrance is the instance of clazz or operation, object is constraint
			List<String> result = new ArrayList<String>();
			result.add("//parse "+ object);
			String body = ModelUtil.getBodyConstraint((Constraint)object);
			TestOCLPattern.getInstance();
			result.add("assertTrue(MOCLUtil.getInstance().evaluateOCL("+var_Instance+".eClass(), \""+TestOCLPattern.parseVar(body)+"\", "+var_Instance+"));");
			return result;
		}
		@Override
		public List<String> toJunit2(Model umlModel, String var_Instance, String constraint) {//var_Insrance is the instance of clazz or operation, object is constraint
			List<String> result = new ArrayList<String>();
			TestOCLPattern.getInstance();
			result.add("assertTrue(MOCLUtil.getInstance().evaluateOCL("+var_Instance+".eClass(), \""+TestOCLPattern.parseVar(constraint)+"\", "+var_Instance+"));");
			return result;
		}
		
		@Override
		public List<String> toJunit2(Model umlModel, String var_Instance, String constraint, String info) {//var_Insrance is the instance of clazz or operation, object is constraint
			List<String> result = new ArrayList<String>();
			result.add("assertTrue("+info+", MOCLUtil.getInstance().evaluateOCL("+var_Instance+".eClass(), \""+TestOCLPattern.parseVar(constraint)+"\", "+var_Instance+"));");
			
			return result;
		}
	};
	
	public static final StatementToJunit TRANSITION_GUARD = new StatementToJunit("transition_guard", SIMPLE_ST_NAME, CONSTRAINT_NAME){

		@Override
		public boolean validate(EObject owner, EObject object) {
			boolean result = (owner instanceof Transition && object instanceof Constraint);
			if(((Constraint)object).getContext() != null && ModelUtil.checkContextOfConstraint((Constraint)object)){
				result = result && MOCLUtil.getInstance().validateOCL((Classifier)((Constraint)object).getContext(), ModelUtil.getBodyConstraint((Constraint)object));
			}
			return result;
		}

		@Override
		public boolean evaluate(Model umlModel, EObject object, EObject instance) {
			EObject context = ((Constraint)object).getContext();
			if(context instanceof Classifier){
				return MOCLUtil.getInstance().evaluateOCL((Classifier)context, ModelUtil.getBodyConstraint((Constraint)object), instance);
			}
			return false;//FIXME -- check the context for the operation and others
		}

		@Override
		public List<String> toJunit(Model umlModel, String var_Instance, EObject object) {//var_Insrance is the instance of clazz or operation, object is constraint
			List<String> result = new ArrayList<String>();
			result.add("//parse "+ object);
			String body = ModelUtil.getBodyConstraint((Constraint)object);
			TestOCLPattern.getInstance();
			result.add("assertTrue(MOCLUtil.getInstance().evaluateOCL("+var_Instance+".eClass(), \""+TestOCLPattern.parseVar(body)+"\", "+var_Instance+"));");
			return result;
		}
		@Override
		public List<String> toJunit2(Model umlModel, String var_Instance, String constraint) {//var_Insrance is the instance of clazz or operation, object is constraint
			List<String> result = new ArrayList<String>();
			TestOCLPattern.getInstance();
			if(islogger)
				result.add("assertTrue(\""+constraint+"\",MOCLUtil.getInstance().evaluateOCL("+var_Instance+".eClass(), \""+TestOCLPattern.parseVar(constraint)+"\", "+var_Instance+"));");
			else
				result.add("assertTrue(MOCLUtil.getInstance().evaluateOCL("+var_Instance+".eClass(), \""+TestOCLPattern.parseVar(constraint)+"\", "+var_Instance+"));");
			return result;
		}
		@Override
		public List<String> toJunit2(Model umlModel, String var_Instance, String constraint, String info) {//var_Insrance is the instance of clazz or operation, object is constraint
			List<String> result = new ArrayList<String>();
			result.add("assertTrue(\""+info+"\", MOCLUtil.getInstance().evaluateOCL("+var_Instance+".eClass(), \""+TestOCLPattern.parseVar(constraint)+"\", "+var_Instance+"));");
			
			return result;
		}
		
		@Override
		public List<String> toJunitCheckThat(Model umlModel, String var_Instance, String constraint, String loggerInfo, String correcter){
			List<String> result = new ArrayList<String>();
			result.add(correcter+".checkThat(\""+loggerInfo+"\", MOCLUtil.getInstance().evaluateOCL("+var_Instance+".eClass(), \""+TestOCLPattern.parseVar(constraint)+"\", "+var_Instance+"),"
					+ "IsEqual.equalTo(true));");
			return result;
		}
		
		
	};
	
	// ownerClassifier is Package?, object is 
	public static final StatementToJunit OBJECT_INSTANCE = new StatementToJunit("", "",""){

		@Override
		public boolean validate(EObject owner, EObject object) {
			return (object instanceof InstanceSpecification);
		}

		@Override
		public boolean evaluate(Model umlModel, EObject object, EObject instance) {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public List<String> toJunit(Model umlModel, String var_Instance, EObject object) {
			//FIXME
			List<String> script = new ArrayList<String>();
			InstanceSpecification ins = ((InstanceSpecification)object);
			for(Classifier classifier: ins.getClassifiers()){
				script.add(classifier.getName()+" "+ins.getName()+"");
			}
			return null;
		}

		@Override
		public List<String> toJunit2(Model umlModel, String var_Instance, String cons) {
			// TODO Auto-generated method stub
			return null;
		}};
	
	private final String name;
	private final Set<String> classifiers;
	private final String ownerClassifier;
	
	public StatementToJunit(String name, String ownerClassifer, String...classifiers){
		this.name = name;
		this.ownerClassifier = ownerClassifer;
		Set<String> tmp = new HashSet<String>();
		for(String t : classifiers){
			tmp.add(t);
		}
		this.classifiers = Collections.unmodifiableSet(tmp);
		
	}
	
	public StatementToJunit(String name, EObject ownerClass, EObject... clazz){
		this.name = name;
		this.ownerClassifier = ownerClass.eClass().getName();
		Set<String> tmp = new HashSet<String>();
		for(EObject t : clazz){
			tmp.add(t.eClass().getName());
		}
		this.classifiers = Collections.unmodifiableSet(tmp);
	}
	// this method is to implement the match of this pattern
	public abstract boolean validate(EObject owner, EObject object);
	// this method is to implement to evaluate at runtime
	public abstract boolean evaluate(Model umlModel, EObject object, EObject instance);
	// this method is to implement to generate test script specified by junit
	public abstract List<String> toJunit(Model umlModel, String var_Instance, EObject object);
	public abstract List<String> toJunit2(Model umlModel, String var_Instance, String cons);
	public List<String> toJunit2(Model umlModel, String var_Instance, String cons, String loggerInfo){
		System.err.println("not implement the logger info");
		return null;
	}
	public List<String> toJunitCheckThat(Model umlModel, String var_Instance, String cons, String loggerInfo, String correcter){
		System.err.println("not implement the logger info");
		return null;
	}

	public String getName() {
		return name;
	}

	public Set<String> getClassifiers() {
		return classifiers;
	}

	public String getOwnerClassifier() {
		return ownerClassifier;
	}
	public static void enableLogger(){
		islogger = true;
	}

}
