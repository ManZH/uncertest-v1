/*******************************************************************************
 * Copyright (c) 2016, 2017 Simula Research Laboratory. 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Simula license 
 * which accompanies this distribution, and is available at LICENSE.TXT
 *  
 *  Contributors:
 *        Man Zhang
 *******************************************************************************/
package no.simula.se.testing.execution.api;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.uml2.uml.InstanceSpecification;
import org.eclipse.uml2.uml.InstanceValue;
import org.eclipse.uml2.uml.LiteralSpecification;
import org.eclipse.uml2.uml.OpaqueExpression;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.uml2.uml.Slot;
import org.eclipse.uml2.uml.ValueSpecification;

import no.simula.se.testing.execution.api.util.GenStatementUtil;
import no.simula.se.testing.utility.ModelUtil;

public abstract class APIConfiguration {
	
	final String name;
	final String type;
	final String etype;
	final List<String> parms;
	String factoryName;
	int apiType;
	APIConfiguration owner;
	
	public APIConfiguration(String name, String etype,String type, String... parms){
		this.apiType = 0;
		this.name = name;
		this.type = type;
		this.etype = type;
		
		if(parms != null){
			this.parms = new ArrayList<String>();
			for(String str: parms){
				if(!this.parms.contains(str)){
					this.parms.add(str);
				}
			}
		}else{
			this.parms = null;
		}
	}
	public APIConfiguration(int api, String name, String etype,String type, String... parms){
		this.apiType = api;
		
		this.name = name;
		this.type = type;
		this.etype = type;
		
		if(parms != null){
			this.parms = new ArrayList<String>();
			for(String str: parms){
				if(!this.parms.contains(str)){
					this.parms.add(str);
				}
			}
		}else{
			this.parms = null;
		}
	}
	
	public APIConfiguration(String facName, int api, String name, String etype,String type, String... parms){
		this.factoryName = facName;
		this.apiType = api;
		
		this.name = name;
		this.type = type;
		this.etype = type;
		
		if(parms != null){
			this.parms = new ArrayList<String>();
			for(String str: parms){
				if(!this.parms.contains(str)){
					this.parms.add(str);
				}
			}
		}else{
			this.parms = null;
		}
	}
	
	public APIConfiguration(APIConfiguration owner, int api , String name, String etype,String type, String... parms){
		this.apiType = api;
		this.owner = owner;
		this.name = name;
		this.type = type;
		this.etype = type;
		
		if(parms != null){
			this.parms = new ArrayList<String>();
			for(String str: parms){
				if(!this.parms.contains(str)){
					this.parms.add(str);
				}
			}
		}else{
			this.parms = null;
		}
	}
	
	public APIConfiguration(APIConfiguration owner, String facName, int api, String name, String etype,String type, String... parms){
		this.factoryName = facName;
		this.apiType = api;
		this.owner = owner;
		this.name = name;
		this.type = type;
		this.etype = type;
		
		if(parms != null){
			this.parms = new ArrayList<String>();
			for(String str: parms){
				if(!this.parms.contains(str)){
					this.parms.add(str);
				}
			}
		}else{
			this.parms = null;
		}
	}
	
	// validate if satisfy this api
	
	// api = 0,3 class 
	// api = 1 operation
	// api = 2 signal
	// api = 4 operation, same name with operation in the model
	public boolean validate(EObject object){
		if(this.apiType == 0 || this.apiType == 3){
			return ((object instanceof org.eclipse.uml2.uml.InstanceSpecification)&& (((org.eclipse.uml2.uml.InstanceSpecification)object).getClassifier(this.getType())!= null));
		}else if(this.apiType == 1 || this.apiType == 4){
			boolean result = false;
			if(object instanceof Operation){
				Operation op = (Operation) object;
				result = true && (op.getName().equals(this.getType()));
			}
			return result;
		}else if(this.apiType == 2){
			return ((object instanceof org.eclipse.uml2.uml.Signal)&& (((org.eclipse.uml2.uml.Signal)object).getName().equals(this.getType())));
		}
		return false;
	}
	
	public String toJava(org.eclipse.uml2.uml.Package model,EObject owner, EObject object, String instance_var){
		//FIXME if class, notice the emf construction
		if(this.apiType == 0){
			String result = instance_var+"= new "+this.getType()+"(";
			if(this.getParms() != null){
				for(String par : this.getParms()){
					result = result + par + ",";
				}
				result = result.substring(0, result.length() - 2);
			}
			result = result + ");";
			return result;
		}else if(this.apiType == 1){
//			String result = instance_var+".(";
//			if(this.getParms() != null){
//				for(String par : this.getParms()){
//					result = result + par + ",";
//				}
//				result = result.substring(0, result.length() - 2);
//			}
//			result = result + ");";
//			return result;
			String name = this.getType().substring(0, 1).toUpperCase()+ this.getType().substring(1);
			String result = String.format("%1s.op%2s(\"%3s\"", this.factoryName, name, this.getType());
			if(this.getParms() != null){
				for(String par : this.getParms()){
					result = result +","+ par;
				}
			}
			result = result + ");";
			return result;
		}else if(this.apiType == 2){
			String name = this.getType().substring(0, 1).toUpperCase()+ this.getType().substring(1);
			String result = String.format("%1s.execute%2s(\"%3s\"", this.factoryName, name, this.getType());
			
			if (object instanceof InstanceSpecification) {
				InstanceSpecification ins = (InstanceSpecification) object;
				for (Slot s : ins.getSlots()) {
//					org.eclipse.uml2.uml.Property p = (org.eclipse.uml2.uml.Property) s.getDefiningFeature();
//					
//					String pName = p.getName();
					for(ValueSpecification vs : s.getValues()){
						if(vs instanceof InstanceValue){
							InstanceValue insV = (InstanceValue)vs;
							result = result + "," + insV.getInstance().getName();
							
						}else if(vs instanceof OpaqueExpression){
							result = result + ","+ ModelUtil.getStrOperationExpression((OpaqueExpression)vs);
						}else if(vs instanceof LiteralSpecification){
							result = result + ","+ ModelUtil.getStrLiteralSpecification((LiteralSpecification)vs);
						}else{
							System.err.println("the type of this vs is not implemented "+ vs);
						}
					}
				}
			}
//			if(this.getParms() != null){
//				for(String par : this.getParms()){
//					result = result +","+ par;
//				}
//			}
			result = result + ");";
			return result;
		}else if(this.apiType == 3){
			String result = "";
			if (object instanceof InstanceSpecification) {
				InstanceSpecification ins = (InstanceSpecification) object;
				String ins_name = ins.getName();
				result = result + GenStatementUtil.generateEMFCreate(ins_name,this.factoryName, this.getType());
				for (Slot s : ins.getSlots()) {
					org.eclipse.uml2.uml.Property p = (org.eclipse.uml2.uml.Property) s.getDefiningFeature();
					
					String pName = p.getName();
					for(ValueSpecification vs : s.getValues()){
						if(vs instanceof InstanceValue){
							InstanceValue insV = (InstanceValue)vs;
							if(p.getUpper() != 1){
								result = result + GenStatementUtil.generateListSetter(ins_name, pName, insV.getInstance().getName());
							}else{
								result = result + GenStatementUtil.generateSetter(ins_name, pName, insV.getInstance().getName());
							}
						}else if(vs instanceof OpaqueExpression){
							result = result + GenStatementUtil.generateSetter(ins_name, pName,  ModelUtil.getStrOperationExpression((OpaqueExpression)vs));
						}else if(vs instanceof LiteralSpecification){
							result = result + GenStatementUtil.generateSetter(ins_name, pName,  ModelUtil.getStrLiteralSpecification((LiteralSpecification)vs));
						}else{
							System.err.println("the type of this vs is not implemented "+ vs);
						}
					}
				}
				return result;
			}
			
		}else if(this.apiType == 4){
//			String result = instance_var+".(";
//			if(this.getParms() != null){
//				for(String par : this.getParms()){
//					result = result + par + ",";
//				}
//				result = result.substring(0, result.length() - 2);
//			}
//			result = result + ");";
//			return result;
		//	String name = this.getType().substring(0, 1).toUpperCase()+ this.getType().substring(1);
			String result = String.format("%1s.%2s(", this.factoryName, this.getType());
			if(this.getParms() != null){
				for(String par : this.getParms()){
					result = result +","+ par;
				}
			}
			result = result + ");";
			return result;
		}
		//FIXME signal has no default one
		return null;
	}
	
	public String getName() {
		return name;
	}
	public String getType() {
		return type;
	}
	public String getEtype() {
		return etype;
	}
	public List<String> getParms() {
		return parms;
	}
	public APIConfiguration getOwner() {
		return owner;
	}
	public void setOwner(APIConfiguration owner) {
		this.owner = owner;
	}
	public String getFactoryName() {
		return factoryName;
	}
//	public void setFactoryName(String factoryName) {
//		this.factoryName = factoryName;
//	}
	
	
}
