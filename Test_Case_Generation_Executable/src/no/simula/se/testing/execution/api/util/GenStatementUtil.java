/*******************************************************************************
 * Copyright (c) 2016, 2017 Simula Research Laboratory. 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Simula license 
 * which accompanies this distribution, and is available at LICENSE.TXT
 *  
 *  Contributors:
 *        Man Zhang
 *******************************************************************************/
package no.simula.se.testing.execution.api.util;



public class GenStatementUtil {

	public static String generateEMFCreate(String facName, String clazName){
		return String.format("%1s.eINSTANCE.create%2s();\n", facName, clazName);
	}
	
	public static String generateEMFCreate(String insName, String facName, String clazName){
		return String.format("%1s = %2s.eINSTANCE.create%3s();\n", insName, facName, clazName);
	}
	
	public static String generateSetter(String insName, String attName, String parInsName){
		return String.format("%1s.set%2s(%3s);\n", insName,attName.substring(0, 1).toUpperCase() + attName.substring(1), parInsName);
	}
	
	public static String generateListSetter(String insName, String attName, String parInsName){
		return String.format("%1s.get%2s().add(%3s);\n", insName,attName.substring(0, 1).toUpperCase() + attName.substring(1), parInsName);
	}
	
}
