/*******************************************************************************
 * Copyright (c) 2016, 2017 Simula Research Laboratory. 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Simula license 
 * which accompanies this distribution, and is available at LICENSE.TXT
 *  
 *  Contributors:
 *        Man Zhang
 *******************************************************************************/
package no.simula.se.testing.execution.api;

import java.util.Map;

import org.eclipse.emf.ecore.EObject;

import no.simula.se.testing.transformation.testmodel2junit.CommentUtil;

public abstract class RegisterAPI {
	
	private String[] prefined_setup1;
	private String[] prefined_setup2;
	
	private String[] prefined_declar;
	private String[] deleted_declar;
	private String[] prefined_after;
	private String[] prefined_updated_before_constraint;
	
	public String[] predfined_method;
	
	//FIXME later for the previous version of register API
	public String[] getPrefined_setup() {
		return prefined_setup1;
	}
	public void setPrefined_setup(String[] prefined_setup) {
		this.prefined_setup1 = prefined_setup;
	}
	public String[] getPrefined_declar() {
		return prefined_declar;
	}
	public void setPrefined_declar(String[] prefined_declar) {
		this.prefined_declar = prefined_declar;
	}
	public String[] getPrefined_after() {
		return prefined_after;
	}
	public void setPrefined_after(String[] prefined_after) {
		this.prefined_after = prefined_after;
	}
	public String[] getPrefined_updated_before_constraint() {
		return prefined_updated_before_constraint;
	}
	public void setPrefined_updated_before_constraint(String[] prefined_updated_before_constraint) {
		this.prefined_updated_before_constraint = prefined_updated_before_constraint;
	}
	public String[] getPredfined_method() {
		return predfined_method;
	}
	public void setPredfined_method(String[] predfined_method) {
		this.predfined_method = predfined_method;
	}
	public String[] getDeleted_declar() {
		return deleted_declar;
	}
	public void setDeleted_declar(String[] deleted_declar) {
		this.deleted_declar = deleted_declar;
	}
	public String updateBefore(String var){
		return CommentUtil.getSigleJavaComment("do not use update "+var);
	}
	
//	public static Map<String, APIConfiguration> maps;//FIXME different parms of operation, but the name of operation is same;
//	static {
//		maps = new HashMap<String, APIConfiguration>();
//	}
	
	public final void registerAPI(APIConfiguration apiConfig){
		getMap().put(apiConfig.getType(), apiConfig);
	}
	
	public final APIConfiguration getAPIConfiguration(String key){
		return getMap().get(key);
	}
	
	public final APIConfiguration getAPIConfiguration(EObject object){
		for(APIConfiguration api : getMap().values()){
			if(api.validate(object))
				return api;
		}
		return null;
	}
	
	public String[] getPrefined_setup1() {
		return prefined_setup1;
	}
	public void setPrefined_setup1(String[] prefined_setup1) {
		this.prefined_setup1 = prefined_setup1;
	}
	public String[] getPrefined_setup2() {
		return prefined_setup2;
	}
	public void setPrefined_setup2(String[] prefined_setup2) {
		this.prefined_setup2 = prefined_setup2;
	}
	public abstract Map<String, APIConfiguration> getMap();
	
	public String[] getImports(){ return null;};
}
