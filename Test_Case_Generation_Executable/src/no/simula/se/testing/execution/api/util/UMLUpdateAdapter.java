/*******************************************************************************
 * Copyright (c) 2016, 2017 Simula Research Laboratory. 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Simula license 
 * which accompanies this distribution, and is available at LICENSE.TXT
 *  
 *  Contributors:
 *        Man Zhang
 *******************************************************************************/
package no.simula.se.testing.execution.api.util;

import java.util.ListIterator;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.FinalState;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.Pseudostate;
import org.eclipse.uml2.uml.PseudostateKind;
import org.eclipse.uml2.uml.State;
import org.eclipse.uml2.uml.Transition;

import no.simula.se.testing.generator.atc.ToUTestModel;
import no.simula.se.testmodel.TestModel.UTestAction;
import no.simula.se.testmodel.TestModel.UTestCase;
import no.simula.se.testmodel.TestModel.UTestCaseElement;
import no.simula.se.testmodel.TestModel.UTestConfiguration;
import no.simula.se.testmodel.TestModel.UTestItem;
import no.simula.se.testmodel.TestModel.UTestModel;
import no.simula.se.testmodel.TestModel.UTestParallel;
import no.simula.se.testmodel.TestModel.UTestPath;
import no.simula.se.testmodel.TestModel.UTestSet;

public class UMLUpdateAdapter {

	public void findElement(org.eclipse.uml2.uml.Package previous, org.eclipse.uml2.uml.Package adaptee, UTestModel tmodel){
		for(UTestItem item : tmodel.getUtestitem()){
			for(UTestSet uts : item.getUtestset()){
				int i = 0;
				for(UTestCase utc : uts.getUtestcase()){
					int j = 0;
					for(UTestCaseElement aa : ((UTestPath)utc).getSequence()){
						if(aa instanceof UTestAction){
							EObject eobj = ((UTestAction)aa).getAttachedObject();
							if(eobj instanceof State){
								if(((State)eobj).getName()!= null && ((State)eobj).getName().equals("Item Rejected")){
									System.out.println(i+" "+j);
								}
							}
						}
						j++;
					}
					i++;
				}
			}
		}
	}
	
	
	public UTestModel regenerateTSConfiguration(org.eclipse.uml2.uml.Package adaptee, UTestModel tmodel, String setup_name){
		System.err.println("orignal:"+tmodel.getUtestconfiguration().size());
		EObject[] removed = new EObject[tmodel.getUtestconfiguration().size()];
		int i = 0;
		for(ListIterator<UTestConfiguration> iter = tmodel.getUtestconfiguration().listIterator(); iter.hasNext();){
			UTestConfiguration conf = iter.next();
			removed[i] = conf;
			i++;
		}
		for(int j = 0; i < removed.length ;j++){
			EcoreUtil.delete(removed[j]);
		}
		tmodel.getUtestconfiguration().clear();
		System.err.println("delete:"+tmodel.getUtestconfiguration().size());
		ToUTestModel toT = new ToUTestModel();
		toT.generateSpecifiedTestConfigs(tmodel, adaptee, setup_name);
		System.err.println("re-generate:"+tmodel.getUtestconfiguration().size());
		return tmodel;
	}
	
	public UTestModel adapter(org.eclipse.uml2.uml.Package previous, org.eclipse.uml2.uml.Package adaptee, UTestModel tmodel) throws Exception{
		EObject am = tmodel.getAttachedObject();
		if(am != null){
			Element newE = getClazzByName(adaptee, ((NamedElement)am).getName());
			if(newE != null) tmodel.setAttachedObject(newE);
		}
		
		for(UTestItem item : tmodel.getUtestitem()){
			EObject aitem = item.getAttachedObject();
			if(aitem != null){
				Element newC = getClazzByName(adaptee, ((NamedElement)aitem).getName());
				if(newC != null) item.setAttachedObject(newC);
				
				for(UTestSet uts : item.getUtestset()){
					EObject aset = uts.getAttachedObject();
					if(aset != null){
						Element newSM = getElementByName(newC, ((NamedElement)aset).getName());
						if(newSM != null) uts.setAttachedObject(newSM);
						
						//
						for(UTestCase utc : uts.getUtestcase()){
							adapter(previous, adaptee, utc);
						}
						
					}
					
				}
			}
		}
		
//		for(UTestConfiguration conf : tmodel.getUtestconfiguration()){
//			EObject aconf = conf.getAttachedObject();
//			if(aconf != null){
//				//find package first
//				Element newpack = getClazzByName(adaptee, ((NamedElement)((NamedElement)aconf).getOwner()).getName());
//				Element newconf = getElementByName(newpack, ((NamedElement)aconf).getName());
//				if(newconf != null) conf.setAttachedObject(newconf);
//			}
//		}
		return tmodel;
	}
	
	public void adapter(org.eclipse.uml2.uml.Package previous, org.eclipse.uml2.uml.Package adaptee, UTestCase utc) throws Exception{
		if(utc instanceof UTestPath){
			 adapter(previous, adaptee, (UTestPath)utc);
		}else if(utc instanceof UTestParallel){
			 adapter(previous, adaptee, (UTestParallel)utc);
		}
	}
	
	public void adapter(org.eclipse.uml2.uml.Package previous, org.eclipse.uml2.uml.Package adaptee, UTestPath path) throws Exception{
		for(UTestCaseElement aa : path.getSequence()){
			if(aa instanceof UTestAction) adapter(previous, adaptee, (UTestAction)aa);
			else if(aa instanceof UTestPath) adapter(previous, adaptee, (UTestPath)aa);
			else if(aa instanceof UTestParallel) adapter(previous, adaptee, (UTestParallel)aa);
 		}
	}
	
	public void adapter(org.eclipse.uml2.uml.Package previous, org.eclipse.uml2.uml.Package adaptee, UTestParallel para) throws Exception{
		for(UTestPath ap : para.getParalles()){
			adapter(previous, adaptee, ap);
		}
	}
	
	public void adapter(org.eclipse.uml2.uml.Package previous, org.eclipse.uml2.uml.Package adaptee, UTestAction aa) throws Exception{
		EObject aobj = aa.getAttachedObject();
		if(aobj instanceof Transition){
			Transition atr = (Transition) aobj;
			//TestSetupif(atr.getName() == null) System.err.println(atr.getSource().getName()+" "+atr.getTarget().getName());
			if(atr.getName() == null) throw new Exception("the tr name is not set!");
			String smName = ((NamedElement)atr.getOwner().getOwner()).getName();
			Element aSM = getClazzByName(adaptee, smName);
			
			Element newTR = getElementByName(aSM, atr.getName());
			aa.setAttachedObject(newTR);
		}else if(aobj instanceof FinalState){
			FinalState afs = (FinalState) aobj;
			String smName = ((NamedElement)afs.getOwner().getOwner()).getName();
			Element aSM = getClazzByName(adaptee, smName);
			if(afs.getName() ==null){
				Element newfs = getElementByFinal(aSM);
				aa.setAttachedObject(newfs);
			}else{
				Element newfs = getElementByName(aSM, afs.getName());
				aa.setAttachedObject(newfs);
			}
			
		}else if(aobj instanceof Pseudostate){
			Pseudostate aps = (Pseudostate) aobj;
			String smName = ((NamedElement)aps.getOwner().getOwner()).getName();
			Element aSM = getClazzByName(adaptee, smName);
			
			if(aps.getName() != null){
				Element newfs = getElementByName(aSM, aps.getName());
				aa.setAttachedObject(newfs);
			}else{
				if(aps.getKind() == PseudostateKind.INITIAL_LITERAL){
					Element newfs = getElementByInital(aSM);
					aa.setAttachedObject(newfs);
				}else if(aps.getKind() == PseudostateKind.CHOICE_LITERAL){
					Element newfs = getElementByChoice(aSM);
					aa.setAttachedObject(newfs);
				}else if(aps.getKind() == PseudostateKind.TERMINATE_LITERAL){
					Element newfs = getElementByTerminate(aSM);
					aa.setAttachedObject(newfs);
				}
				else{
					throw new Exception("no name for this object "+aobj);
				}
			}
		}else if(aobj instanceof State){
			State ast = (State) aobj;
			if(ast.getName() == null) throw new Exception("the tr name is not set!");
			String smName = ((NamedElement)ast.getOwner().getOwner()).getName();
			Element aSM = getClazzByName(adaptee, smName);
			
			Element newST = getElementByName(aSM, ast.getName());
			aa.setAttachedObject(newST);
		}
	}
	
	
	public Element getClazzByName(org.eclipse.uml2.uml.Package adaptee, String name) throws Exception{
		for(Element obj : adaptee.allOwnedElements()){
			if(obj instanceof NamedElement && ((NamedElement)obj).getName() != null && ((NamedElement)obj).getName().equals(name)){
				return obj;
			}
		}
		System.err.println(adaptee+"\n"+name);
		throw new Exception("none of name is matched!");
	}
	
	public Element getElementByName(Element adaptee, String name) throws Exception{
		for(Element obj : adaptee.allOwnedElements()){
			if(obj instanceof NamedElement && ((NamedElement)obj).getName() != null && ((NamedElement)obj).getName().equals(name)){
				return obj;
			}
		}
		System.err.println(adaptee+"\n"+name);
		throw new Exception("none of name is matched!");
	}
	
	public Element getElementByType(Element adaptee, String typeName) throws Exception{
		for(Element obj : adaptee.getOwnedElements()){
			if(obj.getClass().getName().equals(typeName)){
				return obj;
			}
		}
		System.err.println(adaptee+"\n"+typeName);
		throw new Exception("none of type is matched!");
	}
	
	public Element getElementByInital(Element adaptee) throws Exception{
		for(Element obj : adaptee.allOwnedElements()){
			if(obj instanceof Pseudostate && ((Pseudostate)obj).getKind() == PseudostateKind.INITIAL_LITERAL && obj.getOwner().getOwner().equals(adaptee)){
				return obj;
			}
		}
		throw new Exception("none of initial is matched!");
	}
	
	public Element getElementByChoice(Element adaptee) throws Exception{
		for(Element obj : adaptee.allOwnedElements()){
			if(obj instanceof Pseudostate && ((Pseudostate)obj).getKind() == PseudostateKind.CHOICE_LITERAL && obj.getOwner().getOwner().equals(adaptee)){
				return obj;
			}
		}
		throw new Exception("none of choice is matched!");
	}
	
	public Element getElementByTerminate(Element adaptee) throws Exception{
		for(Element obj : adaptee.allOwnedElements()){
			if(obj instanceof Pseudostate && ((Pseudostate)obj).getKind() == PseudostateKind.TERMINATE_LITERAL && obj.getOwner().getOwner().equals(adaptee)){
				return obj;
			}
		}
		throw new Exception("none of choice is matched!");
	}
	
	public Element getElementByFinal(Element adaptee) throws Exception{
		for(Element obj : adaptee.allOwnedElements()){
			if(obj instanceof FinalState && obj.getOwner().getOwner().equals(adaptee)){
				return obj;
			}
		}
		throw new Exception("none of final is matched!");
	}
	
}
