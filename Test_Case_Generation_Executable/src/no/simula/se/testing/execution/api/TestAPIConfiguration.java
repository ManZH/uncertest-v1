/*******************************************************************************
 * Copyright (c) 2016, 2017 Simula Research Laboratory. 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Simula license 
 * which accompanies this distribution, and is available at LICENSE.TXT
 *  
 *  Contributors:
 *        Man Zhang
 *******************************************************************************/
package no.simula.se.testing.execution.api;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.eclipse.emf.ecore.EObject;

public abstract class TestAPIConfiguration {
	
	final String name;
	final String type;
	final String etype;
	final Set<String> parms;
	
	public TestAPIConfiguration(String name, String etype,String type, String... parms){
		this.name = name;
		this.type = type;
		this.etype = type;
		
		if(parms != null){
			Set<String> tmp = new HashSet<String>();
			for(String str: parms){
				tmp.add(str);
			}
			this.parms = Collections.unmodifiableSet(tmp);
		}else{
			this.parms = null;
		}
	}
	public abstract boolean validate(EObject object);
	public abstract String getAPI(EObject object);
	public abstract String toString();
	public abstract String toJunit(org.eclipse.uml2.uml.Package model,EObject owner, EObject object, String instance_var);
	
	public String getName() {
		return name;
	}
	public String getType() {
		return type;
	}
	public String getEtype() {
		return etype;
	}
	public Set<String> getParms() {
		return parms;
	}
	
	
}
