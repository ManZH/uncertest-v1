/*******************************************************************************
 * Copyright (c) 2016, 2017 Simula Research Laboratory. 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Simula license 
 * which accompanies this distribution, and is available at LICENSE.TXT
 *  
 *  Contributors:
 *        Man Zhang
 *******************************************************************************/
package no.simula.se.testing.execution.api;

public class TestDataGenerationUtil {
	public static TestDataGenerationUtil instance;
	
	public static TestDataGenerationUtil getInstance(){
		if(instance == null){
			instance = new TestDataGenerationUtil();
		}
		return instance;
	}
	
	public String generateTestData(){
		String text = "RandomData.getInstance();\n double lat = RandomData.random(0, 3.5);\n double lon = 0.0;\n";
		return text;
	}
	
	public String generatePositionTestData(){
		String text = "RandomData.getInstance(); double lat = RandomData.random(0, 3.5); double lon = 0.0;";
		return text;
	}
	
	public String generateSetPositionTestData(){
		String text ="String tagId = ";
		if(Math.random() > 0.5){
			text = text + "\"b4994c878131\";\n";
		}else{
			text = text + "\"b4994c8bc090\";\n";
		}
		text = text+"RandomData.getInstance();\ndouble lat = RandomData.random(0, 3.5);\ndouble lon = 1.0;\n";
		return text;
	}
	
//	public EObject generatedRelatedObject(EObject obj, String Type){
//		
//	}
	
	public String generateSafeHomeTestData(String var){
		String text = "";
		text = text+"RandomData.getInstance();\nint id = RandomData.random(0, "+var+".getSensors().size());";
		return text;
	}
	
	public String generateSafeHome2TestData(String var){
		String text = "";
		text = text+"RandomData.getInstance();\nint i= RandomData.random(0, "+var+".getMAX_SENSOR() * 2);\n"
				+ "Sensor sensor = null;\nif(i<"+var+".getMAX_SENSOR()) sensor = api.findWinDoorSensor(i);\n"
						+ "else sensor = api.findMotionDetector(i-5);";
		return text;
	}
	
}
