/*******************************************************************************
 * Copyright (c) 2016, 2017 Simula Research Laboratory. 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Simula license 
 * which accompanies this distribution, and is available at LICENSE.TXT
 *  
 *  Contributors:
 *        Man Zhang
 *******************************************************************************/
package no.simula.se.testing.transformation.testmodel2junit;

import java.util.List;

public class CommentUtil {
	
	private static CommentUtil instance;
	public static CommentUtil getInstance(){
		if(instance == null) instance = new CommentUtil();
		return instance;
	}
	public static String getSigleJavaComment(String comment){
		return "//"+comment;
	}
	
	public String getMultiJavaComment(List<String> comments){
		return null;
	}
	
	public static String getReporterInfo(String reporter, String info){
		return String.format("%1s.reportUncerInfo(%2s);", reporter, info);
	}
}
