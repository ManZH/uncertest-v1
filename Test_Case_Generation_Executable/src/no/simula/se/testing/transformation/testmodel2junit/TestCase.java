/*******************************************************************************
 * Copyright (c) 2016, 2017 Simula Research Laboratory. 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Simula license 
 * which accompanies this distribution, and is available at LICENSE.TXT
 *  
 *  Contributors:
 *        Man Zhang
 *******************************************************************************/
package no.simula.se.testing.transformation.testmodel2junit;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.eclipse.uml2.uml.FinalState;
import org.eclipse.uml2.uml.InstanceSpecification;
import org.eclipse.uml2.uml.State;
import org.eclipse.uml2.uml.Transition;

import no.simula.se.testing.utility.belief.BeliefUtility;
import no.simula.se.testmodel.TestModel.UTestAction;
import no.simula.se.testmodel.TestModel.UTestCase;
import no.simula.se.testmodel.TestModel.UTestCaseElement;
import no.simula.se.testmodel.TestModel.UTestParallel;
import no.simula.se.testmodel.TestModel.UTestPath;

public class TestCase implements TestFunctionTemplate{
	
	private String name;
	private UTestCase _case;
	private Map<String, InstanceSpecification> insMap;// instance_name:classifier_name[full name]
	private ModelToString engine;
	//private UReporterToString reEngine;
	
	public TestCase(String name, UTestCase _case, Map<String, InstanceSpecification> insMap, ModelToString engine){
		this.name = name;
		this._case = _case;
		this.insMap = insMap;
		this.engine = engine;
		this.engine.setInsMap(insMap);
	}
	
//	public TestCase(String name, UTestCase _case, Map<String, InstanceSpecification> insMap, ModelToString engine, UReporterToString reEngine){
//		this.name = name;
//		this._case = _case;
//		this.insMap = insMap;
//		this.engine = engine;
//		this.reEngine = reEngine;
//		this.engine.setReEngine(reEngine);
//		this.engine.setInsMap(insMap);
//	}

	@Override
	public String getAnnoation() {
		if(engine.getTimeout() != null)
			return "@org.junit.Test(timeout="+engine.getTimeout()+")";
		return "@org.junit.Test";
	}

	@Override
	public List<String> getContent() {
		return 	getContent(this._case);
	}

	@Override
	public String getBegin() {
		return this.generateBegin(name);
	}
	
	private List<String> getContent(UTestCase _case){
		if(_case instanceof UTestPath) return getContentPath((UTestPath)_case);
		return getContentParallel((UTestParallel)_case);
	}
	private List<String> getContentPath(UTestPath path){
		List<String> list = new ArrayList<String>();
		
		ModelToStringUtil.getInstance();
		engine.getReporter().init();
		for(UTestCaseElement e : path.getSequence()){
			if(e instanceof UTestAction){
				try {
//					if(((UTestAction) e).getAttachedObject() instanceof State){
//						State state = ((State)((UTestAction) e).getAttachedObject());
//						if(state.getName() != null && state.getName().equals("Item Rejected")){
//							System.out.println("found");
//						}
//					}
					
					if(((UTestAction) e).getAttachedObject() instanceof State && 
							((State)((UTestAction) e).getAttachedObject()).isSimple()){
						String var = null;
						State state = ((State)((UTestAction) e).getAttachedObject());
						
						if(!(state instanceof FinalState)){
							
							if(((UTestAction) e).getInstanceoName() != null && !((UTestAction) e).getInstanceoName().equals("")){
								
								//FIXME
								if(((UTestAction) e).getInstanceoName().contains(" ") || state.isSubmachineState()){
									var = getVar((UTestAction)e);
									((UTestAction) e).setInstanceoName("");
								}else{
									var = ((UTestAction) e).getInstanceoName();
								}
							}else{
								var = getVar((UTestAction)e);
							}
							list.addAll(engine.simpleStateToString(((State)((UTestAction) e).getAttachedObject()), var));
							
						}else{
							//FIXME Man Zhang Nov.13
							//System.err.println("skip final state and initial state");
						}
					}else if(((UTestAction) e).getAttachedObject() instanceof Transition){
						String var = null;
						Transition transition = ((Transition)((UTestAction) e).getAttachedObject());
						if(transition.getTriggers().size() > 0 || transition.getGuard() != null || transition.getEffect() != null){
							if(((UTestAction) e).getInstanceoName() != null && !((UTestAction) e).getInstanceoName().equals("")){
								var = ((UTestAction) e).getInstanceoName();
							}else{
								var = getVar((UTestAction)e);
							}
							list.addAll(engine.transitionToString(transition, var));
							//FIXME
							if(engine.getReporter()!=null && BeliefUtility.isApplyCause(transition)){
//								if(!(transition.getTarget() instanceof FinalState)){
//									
//								}
								engine.getReporter().setCurDefUnObj(transition);
								
							}
							
						}else{
							//FIXME Man Zhang Nov.13
							//System.err.println("empty transition");
						}	
						
					} 
				} catch (ModelToStringException e1) {
					e1.printStackTrace();
				}
			}else if(e instanceof UTestPath){
				list.addAll(getContentPath((UTestPath)e));
			}else if(e instanceof UTestParallel){
				list.addAll(getContentParallel((UTestParallel)e));
			}
		}
		return list;
	}
	
	public List<String> getContentParallel(UTestParallel parallel){
//		List<String> list = new ArrayList<String>();
//		ModelToStringUtil.getInstance();
//		for(UTestPath path : parallel.getParalles()){
//			list.addAll(ModelToStringUtil.putInThread(getContentPath(path)));
//		}
//		return list;
		
		List<String> list = new ArrayList<String>();
		ModelToStringUtil.getInstance();
		list.addAll(ModelToStringUtil.generateExecutorBegin(new ArrayList<String>()));
		for(int i = 0; i < parallel.getParalles().size(); i++){
			UTestPath path = parallel.getParalles().get(i);
			list.addAll(ModelToStringUtil.putInExecutor(getContentPath(path), (i==parallel.getParalles().size()-1)));
		}
		list.addAll(ModelToStringUtil.generateExecutorEnd(new ArrayList<String>()));
		return list;
	}
	
	public String getVar(UTestAction e){
		//System.out.println(e.getAttachedObject());
		String clazz = ModelToStringUtil.getClassifierOwner(e.getAttachedObject()).getName();
		for(String key: insMap.keySet()){
			if(insMap.get(key).getClassifier(clazz)!=null)
				return key;
		}
		return null;
	}
	
}
