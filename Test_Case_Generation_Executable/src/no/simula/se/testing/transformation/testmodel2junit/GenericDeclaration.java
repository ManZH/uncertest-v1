/*******************************************************************************
 * Copyright (c) 2016, 2017 Simula Research Laboratory. 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Simula license 
 * which accompanies this distribution, and is available at LICENSE.TXT
 *  
 *  Contributors:
 *        Man Zhang
 *******************************************************************************/
package no.simula.se.testing.transformation.testmodel2junit;

public class GenericDeclaration{
	private int visible;
	private String type;
	private String var_str;
	private String create_str;
	private boolean isEMF;
	
	public GenericDeclaration(){}
	public GenericDeclaration(String type, String var_str){
		this.type = type;
		this.var_str = var_str;
		this.create_str = type;
		this.visible = 0;
		this.setEMF(false);
	}
	public GenericDeclaration(String type, String var_str, int visible){
		this.type = type;
		this.var_str = var_str;
		this.create_str = type;
		this.visible = visible;
		this.setEMF(false);
	}
	public GenericDeclaration(String type, String var_str, boolean isEMF){
		this.type = type;
		this.var_str = var_str;
		this.create_str = type;
		this.visible = 0;
		this.setEMF(isEMF);
	}
	public GenericDeclaration(String type, String var_str, int visible, boolean isEMF){
		this.type = type;
		this.var_str = var_str;
		this.create_str = type;
		this.visible = visible;
		this.setEMF(isEMF);
	}
	public GenericDeclaration(String type, String var_str, String create_str){
		this.type = type;
		this.var_str = var_str;
		this.create_str = create_str;
		this.visible = 0;
		this.setEMF(false);
	}
	public GenericDeclaration(String type, String var_str, String create_str, boolean isEMF){
		this.type = type;
		this.var_str = var_str;
		this.create_str = create_str;
		this.visible = 0;
		this.setEMF(isEMF);
	}
	public GenericDeclaration(String type, String var_str, String create_str, int visible){
		this.type = type;
		this.var_str = var_str;
		this.create_str = create_str;
		this.visible = visible;
		this.setEMF(false);
	}
	public GenericDeclaration(String type, String var_str, String create_str, int visible, boolean isEMF){
		this.type = type;
		this.var_str = var_str;
		this.create_str = create_str;
		this.visible = visible;
		this.setEMF(isEMF);
	}
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getVar_str() {
		return var_str;
	}
	public void setVar_str(String var_str) {
		this.var_str = var_str;
	}
	public String getCreate_str() {
		return create_str;
	}
	public void setCreate_str(String create_str) {
		this.create_str = create_str;
	}
	
	public int getVisible() {
		return visible;
	}
	public void setVisible(int visible) {
		this.visible = visible;
	}
	
	public boolean isEMF() {
		return isEMF;
	}
	public void setEMF(boolean isEMF) {
		this.isEMF = isEMF;
	}
	
	public String toDeclaration(){
		String text = "";
		switch(visible){
		case 0: break;
		case 1: text = text +"private ";break;
		case 2: text = text +"public ";break;
		case 3: text = text +"protected ";break;
		}
		text = text  + this.getType() + " " + this.getVar_str();
		return text;
	}
	
	public String toInstance(){
		String text = "";
		text = text  + this.getVar_str() + " = ";
		if(isEMF){
			text = text + " " + this.create_str+"Factorye.INSTANCE.create" +this.getClass()+"();";
		}else{
			text = text + "new "+ this.create_str +"();";
		}
		return text;
	}
	
	public String toDeclarationAndInstance(){
		String text = "";
		switch(visible){
		case 0: break;
		case 1: text = text +"private ";break;
		case 2: text = text +"public ";break;
		case 3: text = text +"protected ";break;
		}
		text = text  + this.getType() + " " + this.getVar_str() + " = ";
		if(isEMF){
			text = text + " " + this.create_str+"Factorye.INSTANCE.create" +this.getClass()+"();";
		}else{
			text = text + "new "+ this.create_str +"();";
		}
		return text;
	}
}
