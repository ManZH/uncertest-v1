/*******************************************************************************
 * Copyright (c) 2016, 2017 Simula Research Laboratory. 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Simula license 
 * which accompanies this distribution, and is available at LICENSE.TXT
 *  
 *  Contributors:
 *        Man Zhang
 *******************************************************************************/
package no.simula.se.testing.transformation.testmodel2junit;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.List;

public interface TestFunctionTemplate {
	
	public default String generateBegin(String functionName){
		return String.format("public void %s()throws ExecutionException, Exception{", functionName);
	}
	public String getAnnoation();
	public List<String> getContent();
	public String getBegin();
	public default String getEnd(){
		return "}";
	};
	
	public default void writeToFile(BufferedWriter writer){
		try {
			writer.write(getAnnoation());
			writer.write(System.getProperty("line.separator"));
			
			writer.write(getBegin()+System.getProperty("line.separator"));
			//System.out.println(getBegin());
			for(String c : getContent()){
				//System.out.print(c);
				writer.write(c+System.getProperty("line.separator"));
			}
			writer.write(getEnd()+System.getProperty("line.separator"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
