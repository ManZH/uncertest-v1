/*******************************************************************************
 * Copyright (c) 2016, 2017 Simula Research Laboratory. 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Simula license 
 * which accompanies this distribution, and is available at LICENSE.TXT
 *  
 *  Contributors:
 *        Man Zhang
 *******************************************************************************/
package no.simula.se.testing.transformation.testmodel2junit;

import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.InstanceSpecification;
import org.eclipse.uml2.uml.StateMachine;

import no.simula.se.testing.strategies.jgraph.GraphsException;
import no.simula.se.testing.strategies.jgraph.SMGraph;
import no.simula.se.testing.strategies.jgraph.TCsCombinations;
import no.simula.se.testing.strategies.jgraph.TCsGenerateAlgo;
import no.simula.se.testing.utility.belief.BeliefUtilityException;
import no.simula.se.testmodel.TestModel.UTestConfiguration;
import no.simula.se.testmodel.TestModel.UTestItem;
import no.simula.se.testmodel.TestModel.UTestModel;
import no.simula.se.testmodel.TestModel.UTestModelFactory;
import no.simula.se.testmodel.TestModel.UTestSet;

public class ToUTestModel {
	
	private int algo; 
	private int combinations;
	boolean isloop;
	private int theory; // 2- uncertainty theory
	private int minus = 0;
	
	public ToUTestModel(){
		this.algo = TCsGenerateAlgo.ALL_DIRECTED_PATHS;
		this.combinations = TCsCombinations.ALL_COMBINATIONS;
		this.isloop = false;
		this.theory = 2;
	}
	public ToUTestModel(boolean isloop, int minus){
		this.algo = TCsGenerateAlgo.ALL_DIRECTED_PATHS;
		this.combinations = TCsCombinations.ALL_COMBINATIONS;
		this.isloop = isloop;
		this.theory = 2;
		this.minus = minus;
	}
	
	public ToUTestModel(int algo, int combinations, boolean isloop){
		this.algo = algo;
		this.combinations = combinations;
		this.isloop = isloop;
		this.theory = 2;
	}
	
	public UTestModel converToUTestModel(org.eclipse.uml2.uml.Package model) throws GraphsException, BeliefUtilityException{
		UTestModel _tmodel = UTestModelFactory.eINSTANCE.createUTestModel();
		generateSpecifiedTestConfigs(_tmodel, model);
		generateTestItem(_tmodel, model);
		return _tmodel;
	}
	
	public UTestModel converToUTestModel(org.eclipse.uml2.uml.Package model, String... setups) throws GraphsException, BeliefUtilityException{
		UTestModel _tmodel = UTestModelFactory.eINSTANCE.createUTestModel();
		generateSpecifiedTestConfigs(_tmodel, model, setups);
		generateTestItem(_tmodel, model);
		return _tmodel;
	}
	
	private void generateSpecifiedTestConfigs(UTestModel _tmodel, org.eclipse.uml2.uml.Package model){
		generateSpecifiedTestConfigs(_tmodel, model, "TestSetup");
	}
	
	private void generateSpecifiedTestConfigs(UTestModel _tmodel, org.eclipse.uml2.uml.Package model, String... setups){
		for(Element e: model.getOwnedElements()){
			if(e instanceof org.eclipse.uml2.uml.Package){
				org.eclipse.uml2.uml.Package _pack = (org.eclipse.uml2.uml.Package)e;
				if(isPart(setups, _pack.getName())){
					for(Element in : _pack.getOwnedElements()){
						if(in instanceof InstanceSpecification){
							UTestConfiguration _tconfig = UTestModelFactory.eINSTANCE.createUTestConfiguration();
							_tconfig.setAttachedObject(in);
							_tmodel.getUtestconfiguration().add(_tconfig);
						}
					}
				}
				
			}
		}
	}
	
	private boolean isPart(String[] all, String one){
		for(String e : all){
			if(e.equals(one)) return true;
		}
		return false;
	}
	
	private void generateTestItem(UTestModel _tmodel, org.eclipse.uml2.uml.Package model) throws GraphsException, BeliefUtilityException{
		for(Element e : model.getOwnedElements()){
			if(e instanceof org.eclipse.uml2.uml.Classifier){
				UTestItem item = null;
				for(Element inside : e.getOwnedElements()){
					if(inside instanceof StateMachine){
						StateMachine sm = (StateMachine)inside;
						if(item == null) item = UTestModelFactory.eINSTANCE.createUTestItem();
						SMGraph smGraph = new SMGraph(sm, algo, combinations, isloop,minus);
						UTestSet set = smGraph.convertToUTestElement(theory);
						item.getUtestset().add(set);
					}
				}
				if(item != null){
					_tmodel.getUtestitem().add(item);
					item.setAttachedObject(e);
				}
			}
			if(e instanceof StateMachine){
				UTestItem item = UTestModelFactory.eINSTANCE.createUTestItem();
				item.setAttachedObject(model);//if attached object is model, it is integrated one.
				StateMachine sm = (StateMachine)e;
				SMGraph smGraph = new SMGraph(sm, algo, combinations, isloop,minus);
				UTestSet set = smGraph.convertToUTestElement(theory);
				//set.setAttachedObject(sm);
				item.getUtestset().add(set);
				_tmodel.getUtestitem().add(item);
			}
		}
	}
}
