/*******************************************************************************
 * Copyright (c) 2016, 2017 Simula Research Laboratory. 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Simula license 
 * which accompanies this distribution, and is available at LICENSE.TXT
 *  
 *  Contributors:
 *        Man Zhang
 *******************************************************************************/
package no.simula.se.testing.transformation.testmodel2junit;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.List;

public interface TestClassTemplate {
	
	public default String generateBegin(String className){
		return String.format("public class %s {", className);
	}
	public String getAnnoation();
	public String[] getImports();
	public List<String> getDeclaration();
	public String getBegin();
	public default String getEnd(){
		return "}";
	};
	
	public List<TestFunctionTemplate> getfunctions();
	
	public TestFunctionTemplate getSetup();
	public String getPackagePath();
	
	//FIXME Man Zhang
	public List<String> customMethods();
	
	public default void writeToFile(BufferedWriter writer){
		try {
			writer.write(this.getPackagePath());
			writer.write(System.getProperty("line.separator"));
			for(String i : this.getImports()){
				writer.write(i+System.getProperty("line.separator"));
			}
			writer.write(System.getProperty("line.separator"));
			writer.write(getBegin()+System.getProperty("line.separator"));
			
			if(this.getDeclaration() != null){
				writer.write(System.getProperty("line.separator"));
				for(String d : this.getDeclaration()){
					writer.write(d+System.getProperty("line.separator"));
				}
			}
			
			for(TestFunctionTemplate f : this.getfunctions()){
				writer.write(System.getProperty("line.separator"));
				f.writeToFile(writer);
			}
			
			if(this.customMethods() !=null){
				
				for(String content : this.customMethods()){
					writer.write(System.getProperty("line.separator"));
					writer.write(content);
				}			
			}
			writer.write(getEnd());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public default void writeToFileConfigs(BufferedWriter writer){
		try {
			writer.write(this.getPackagePath());
			writer.write(System.getProperty("line.separator"));
			for(String i : this.getImports()){
				writer.write(i+System.getProperty("line.separator"));
			}
			writer.write(System.getProperty("line.separator"));
			writer.write(getBegin()+System.getProperty("line.separator"));
			
			if(this.getDeclaration() != null){
				writer.write(System.getProperty("line.separator"));
				for(String d : this.getDeclaration()){
					writer.write(d+System.getProperty("line.separator"));
				}
			}
			
			if(this.getSetup()!=null){
				writer.write(System.getProperty("line.separator"));
				this.getSetup().writeToFile(writer);
			}
			writer.write(getEnd());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
