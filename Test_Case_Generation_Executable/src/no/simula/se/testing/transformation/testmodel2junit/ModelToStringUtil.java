/*******************************************************************************
 * Copyright (c) 2016, 2017 Simula Research Laboratory. 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Simula license 
 * which accompanies this distribution, and is available at LICENSE.TXT
 *  
 *  Contributors:
 *        Man Zhang
 *******************************************************************************/
package no.simula.se.testing.transformation.testmodel2junit;

import java.util.List;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.uml2.uml.Constraint;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.InstanceSpecification;
import org.eclipse.uml2.uml.InstanceValue;
import org.eclipse.uml2.uml.Slot;
import org.eclipse.uml2.uml.State;
import org.eclipse.uml2.uml.StateMachine;
import org.eclipse.uml2.uml.Transition;
import org.eclipse.uml2.uml.ValueSpecification;


import no.simula.se.testmodel.TestModel.UTestConfiguration;
import no.simula.se.testmodel.TestModel.UTestModel;
import no.simula.se.testmodel.TestModel.UTestSet;

public class ModelToStringUtil {
	
	private static ModelToStringUtil instance;
	public static ModelToStringUtil getInstance(){
		if(instance == null) instance = new ModelToStringUtil();
		return instance;
	}
	public static Constraint rebuildContext(EObject eObject, Constraint constraint){
		if(eObject instanceof State){
			if(!(constraint.getContext() instanceof org.eclipse.uml2.uml.Class)){
				org.eclipse.uml2.uml.Class clazz_ = getClassifierOwner(constraint.getContext());
				constraint.setContext(clazz_);
			}
		}else if(eObject instanceof Transition){
			if(!(constraint.getContext() instanceof org.eclipse.uml2.uml.Class)){
				org.eclipse.uml2.uml.Class clazz_ = getClassifierOwner(constraint.getContext());
				constraint.setContext(clazz_);
			}
		}
		return constraint;
	}
	
	public static org.eclipse.uml2.uml.Class getClassifierOwner(EObject object){
		if((((Element)object).getOwner() instanceof org.eclipse.uml2.uml.Class) && !(((Element)object).getOwner() instanceof org.eclipse.uml2.uml.StateMachine)){
			return (org.eclipse.uml2.uml.Class)((Element)object).getOwner();
		}
		return getClassifierOwner(((Element)object).getOwner());
	}
	
	public static List<String> putInThread(List<String> list){
		list.add(0, "new Thread(new Runnable() {	public void run(){");
		list.add("}}).start();");
		return list;
	}
	
	public static List<String> putInExecutor(List<String> list, boolean last){
		list.add(0, " () -> {");
		
		if(last){
			list.add("return true;});");
		}else{
			list.add("return true;},");
		}
		return list;
	}
	
	public static List<String> generateExecutorBegin(List<String> list){
		list.add("{");
		list.add("ExecutorService executor = Executors.newWorkStealingPool();");
		list.add("List<Callable<Boolean>> callables = Arrays.asList(");
		return list;
	}
	
	public static List<String> generateExecutorEnd(List<String> list){
		list.add("executor.invokeAll(callables);");
		list.add("}");
		return list;
	}
	
	
	// if the type of attached object of test set is package or model
	public static EList<UTestConfiguration> getUTestConfigurationOfTestSet(UTestSet set, UTestModel model) throws ModelToStringException{
		if(set.getAttachedObject() instanceof StateMachine){
			StateMachine sm = (StateMachine) set.getAttachedObject();
			if(sm.getOwner() instanceof org.eclipse.uml2.uml.Class){// class or component(extends from class) or artifact (extends from classifier)
				return getSpecifiedUTestConfigByTestItemWithRelated(sm.getOwner(), model);
			}else if(sm.getOwner() instanceof org.eclipse.uml2.uml.Package){
				return getAllConfigurationOfTestItem(model);
			}throw new ModelToStringException("the owner of sm is not class or package!");
		}
		throw new ModelToStringException("the attached object of test set is not state machine!");
	}
	
	public static EList<UTestConfiguration> getAllConfigurationOfTestItem(UTestModel model)throws ModelToStringException{
		EList<UTestConfiguration> list = new BasicEList<UTestConfiguration>();
		for(UTestConfiguration conf : model.getUtestconfiguration()){
			if(conf.getAttachedObject() instanceof InstanceSpecification){
				InstanceSpecification ins = (InstanceSpecification)conf.getAttachedObject();
				if(!ins.getName().contains("AssociationInstance")) list.add(conf);
			}
		}
		return list;
	}
		
	// if the type of attached object of test set is classifier
	//object is test item whose type is classifier 
	public static EList<UTestConfiguration> getSpecifiedUTestConfigByTestItemWithRelated(EObject object, UTestModel model) throws ModelToStringException{
		EList<UTestConfiguration> list = new BasicEList<UTestConfiguration>();
		for(UTestConfiguration conf : model.getUtestconfiguration()){
			if(conf.getAttachedObject() instanceof InstanceSpecification){
				if(((InstanceSpecification)conf.getAttachedObject()).getClassifiers().contains(object)){
					list.add(conf);
					list.addAll(getRelateds(conf, model));
					return list;
				}
					
			}
		}
		
		throw new ModelToStringException("cannot find instance!");
	}

	public static EList<UTestConfiguration> getSpecifiedUTestConfigByTestItemWithRelated(EObject object, UTestModel model, String name) throws ModelToStringException{
		EList<UTestConfiguration> list = new BasicEList<UTestConfiguration>();
		for(UTestConfiguration conf : model.getUtestconfiguration()){
			if(conf.getAttachedObject() instanceof InstanceSpecification){
				if(((InstanceSpecification)conf.getAttachedObject()).getClassifiers().contains(object) && ((InstanceSpecification)conf.getAttachedObject()).getName().equals(name)){
					list.add(conf);
					list.addAll(getRelateds(conf, model));
					return list;
				}
					
			}
		}
		
		throw new ModelToStringException("cannot find instance!");
	}
	
	public static List<UTestConfiguration> getRelateds(UTestConfiguration uconf, UTestModel model){
		List<UTestConfiguration> relations = new BasicEList<UTestConfiguration>();
		for(Slot slot :((InstanceSpecification)uconf.getAttachedObject()).getSlots()){
			for(ValueSpecification v : slot.getValues()){
				if(v instanceof InstanceValue){
					for(UTestConfiguration conf : model.getUtestconfiguration()){
						if(((InstanceValue)v).getInstance().equals((InstanceSpecification)conf.getAttachedObject()))
							relations.add(conf);
					}
				}
			}
		}
		return relations;
		
	}
//	public EList<EObject> getRelatedEObjects(EObject eObj, String type){
//		for(EAttribute a :eObj.eClass().getEAllAttributes()){
//			a.getEType().
//		}
//		return;
//	}
	
	public static String stringInText(String str){
		return "\""+str+"\"";
	}
}
