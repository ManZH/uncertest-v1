/*******************************************************************************
 * Copyright (c) 2016, 2017 Simula Research Laboratory. 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Simula license 
 * which accompanies this distribution, and is available at LICENSE.TXT
 *  
 *  Contributors:
 *        Man Zhang
 *******************************************************************************/
package no.simula.se.testing.transformation.testmodel2junit;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.uml2.uml.InstanceSpecification;

import no.simula.se.testmodel.TestModel.UTestConfiguration;
import no.simula.se.testmodel.TestModel.UTestModel;
import no.simula.se.testmodel.TestModel.UTestSet;

public class TestSuite implements TestClassTemplate{
	private String[] imports;
	
	private ModelToString engine;
	private Map<String, InstanceSpecification> insMap;
	
	private UTestSet set;
	private String name;
	
	private int[] selected;
	private String packagePath;
	
	
	public TestSuite(String name, String packagePath, UTestSet set, UTestModel model, Path path, int[] selected, ModelToString eng, String[] imports){
		this.name = name;
		this.set = set;
		this.selected = selected;
		this.engine = eng;
		this.imports = imports;
		this.packagePath = packagePath;
		this.insMap = new HashMap<String, InstanceSpecification>();
		ModelToStringUtil.getInstance();
		try {
			for(UTestConfiguration conf : ModelToStringUtil.getUTestConfigurationOfTestSet(set, model)){
				InstanceSpecification ins = (InstanceSpecification) conf.getAttachedObject();
				insMap.put(ins.getName(), ins);
			}
		} catch (ModelToStringException e1) {
			e1.printStackTrace();
		}
		try (BufferedWriter writer = Files.newBufferedWriter(path)) {
			this.writeToFile(writer);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public TestSuite(String name, String packagePath, UTestSet set, UTestModel model, Path path, int[] selected, ModelToString eng, UReporterToString reporter, String[] imports, boolean isAllConfigs){
		this.name = name;
		this.set = set;
		this.selected = selected;
		this.engine = eng;
		this.imports = imports;
		this.packagePath = packagePath;
		this.insMap = new HashMap<String, InstanceSpecification>();
		ModelToStringUtil.getInstance();
		this.engine.setReEngine(reporter);
		try {
//			for(UTestConfiguration conf : ModelToStringUtil.getUTestConfigurationOfTestSet(set, model)){
//				InstanceSpecification ins = (InstanceSpecification) conf.getAttachedObject();
//				insMap.put(ins.getName(), ins);
//			}
			if(isAllConfigs){
				for(UTestConfiguration conf : ModelToStringUtil.getAllConfigurationOfTestItem(model)){
					InstanceSpecification ins = (InstanceSpecification) conf.getAttachedObject();
					insMap.put(ins.getName(), ins);
				}
			}else{
				for(UTestConfiguration conf : ModelToStringUtil.getUTestConfigurationOfTestSet(set, model)){
					InstanceSpecification ins = (InstanceSpecification) conf.getAttachedObject();
					insMap.put(ins.getName(), ins);
				}
			}
		} catch (ModelToStringException e1) {
			e1.printStackTrace();
		}
		try (BufferedWriter writer = Files.newBufferedWriter(path)) {
			this.writeToFile(writer);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	//only configuration is generated
	public TestSuite(String name, String packagePath, UTestModel model, Path path, ModelToString eng, String[] imports, boolean isAllConfigs){
		this.name = name;
		this.engine = eng;
		this.imports = imports;
		this.packagePath = packagePath;
		this.insMap = new HashMap<String, InstanceSpecification>();
		ModelToStringUtil.getInstance();
		try {

			if(isAllConfigs){
				for(UTestConfiguration conf : ModelToStringUtil.getAllConfigurationOfTestItem(model)){
					InstanceSpecification ins = (InstanceSpecification) conf.getAttachedObject();
					insMap.put(ins.getName(), ins);
				}
			}else{
				for(UTestConfiguration conf : ModelToStringUtil.getUTestConfigurationOfTestSet(set, model)){
					InstanceSpecification ins = (InstanceSpecification) conf.getAttachedObject();
					insMap.put(ins.getName(), ins);
				}
			}
		} catch (ModelToStringException e1) {
			e1.printStackTrace();
		}
		try (BufferedWriter writer = Files.newBufferedWriter(path)) {
			this.writeToFileConfigs(writer);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public TestSuite(String name, String packagePath, UTestSet set, UTestModel model, Path path, int[] selected, ModelToString eng, String[] imports, boolean isAllConfigs){
		this.name = name;
		this.set = set;
		this.selected = selected;
		this.engine = eng;
		this.imports = imports;
		this.packagePath = packagePath;
		this.insMap = new HashMap<String, InstanceSpecification>();
		ModelToStringUtil.getInstance();
		try {
			if(isAllConfigs){
				for(UTestConfiguration conf : ModelToStringUtil.getAllConfigurationOfTestItem(model)){
					InstanceSpecification ins = (InstanceSpecification) conf.getAttachedObject();
					insMap.put(ins.getName(), ins);
				}
			}else{
				for(UTestConfiguration conf : ModelToStringUtil.getUTestConfigurationOfTestSet(set, model)){
					InstanceSpecification ins = (InstanceSpecification) conf.getAttachedObject();
					insMap.put(ins.getName(), ins);
				}
			}
			
		} catch (ModelToStringException e1) {
			e1.printStackTrace();
		}
		try (BufferedWriter writer = Files.newBufferedWriter(path)) {
			this.writeToFile(writer);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	// skip this one
	@Override
	public String getAnnoation() {
		return null;
	}
	@Override
	public List<String> getDeclaration() {
		List<String> list = new ArrayList<String>();
		
		for(String ins_name : insMap.keySet()){
			
			try {
				
				if(engine.getRegisterAPI() != null && engine.getRegisterAPI().getDeleted_declar() != null){
					boolean reD = false;
					for(String removed: engine.getRegisterAPI().getDeleted_declar()){
						if(removed.equals(ins_name)){
							reD = true;
							break;
						}
					}
					if(reD) continue;
				}
				//FIXME sort the sequence
				
				if(engine.createDeclartion(ins_name, insMap.get(ins_name)) != null){
					list.addAll(engine.createDeclartion(ins_name, insMap.get(ins_name)));
				}
			} catch (ModelToStringException e) {
				e.printStackTrace();
			}
		}
		

		try {
			if(engine.getRegisterAPI() != null){
				if(engine.getRegisterAPI().getPrefined_declar() !=null){
					for(String str : engine.getRegisterAPI().getPrefined_declar()){
						list.add(str);
					}
				}
			}
		} catch (ModelToStringException e) {
			e.printStackTrace();
		}
		return list;
	}
	@Override
	public String getBegin() {
		return this.generateBegin(name);
	}
	@Override
	public List<TestFunctionTemplate> getfunctions() {
		List<TestFunctionTemplate> list = new ArrayList<TestFunctionTemplate>();
		list.add(getSetup());
		list.addAll(getTests());
		list.add(getAfter());
		return list;
	}
	@Override
	public String[] getImports() {
		String[] _new = new String[this.imports.length];
		for(int i = 0; i < this.imports.length; i++){
			_new[i] = "import "+this.imports[i] + ";";
		}
		return _new;
	}
	
	public TestFunctionTemplate getSetup(){
		List<String> list = new ArrayList<String>();
		
		try {
			if(engine.getRegisterAPI() != null){
				if(engine.getRegisterAPI().getPrefined_setup1() !=null){
					for(String str : engine.getRegisterAPI().getPrefined_setup1()){
						list.add(str);
					}
				}
			}
		} catch (ModelToStringException e) {
			e.printStackTrace();
		}
		
		List<String> declist = new ArrayList<String>();
		for(String ins_name : insMap.keySet()){
			try {
				if(engine.createInstance(ins_name, insMap.get(ins_name)) != null)
					declist.addAll(engine.createInstance(ins_name, insMap.get(ins_name)));
			} catch (ModelToStringException e) {
				e.printStackTrace();
			}
		}
		
		List<String> first = new ArrayList<String>();
		List<String> second = new ArrayList<String>();
		for(String line : declist){
			//System.err.println(line);
			if(line.indexOf(".eINSTANCE.") != -1){
				first.add(line);
			}else{
				second.add(line);
			}
		}	
		//System.err.println(first.size()+" "+second.size());
		list.addAll(first);
		list.addAll(second);
		
		try {
			if(engine.getRegisterAPI() != null){
				if(engine.getRegisterAPI().getPrefined_setup2() !=null){
					for(String str : engine.getRegisterAPI().getPrefined_setup2()){
						list.add(str);
					}
				}
			}
		} catch (ModelToStringException e) {
			e.printStackTrace();
		}
		return new TestBefore(list);
	}
	
	public List<TestFunctionTemplate> getTests(){
		List<TestFunctionTemplate> list = new ArrayList<TestFunctionTemplate>();
		//FIXME
		if(selected == null){
			for(int i = 0; i < set.getUtestcase().size() ; i++){
				String t_name = "test"+ i;
				list.add(new TestCase(t_name, set.getUtestcase().get(i), insMap, engine));
			}
		}else{
			int count = 0;
			for(int i : selected){
				if(i==1){
					String t_name = "test"+ count;
					list.add(new TestCase(t_name, set.getUtestcase().get(count), insMap, engine));
				}
				count++;
			}
		}
		return list;
	}
	public TestFunctionTemplate getAfter(){
		List<String> list = new ArrayList<String>();
		
		try {
			if(engine.getRegisterAPI() != null){
				if(engine.getRegisterAPI().getPrefined_after() !=null){
					for(String str : engine.getRegisterAPI().getPrefined_after()){
						list.add(str);
					}
				}
			}
		} catch (ModelToStringException e) {
			e.printStackTrace();
		}
		
		return new TestAfter(list);
	}

	@Override
	public String getPackagePath() {
		if(packagePath == null)
			return "";
		return "package "+this.packagePath+";";
	}
	
	@Override
	public List<String> customMethods(){
		List<String> methods = new ArrayList<String>();
		try {
			if(engine.getRegisterAPI().getPredfined_method() != null){
				for(String method: engine.getRegisterAPI().getPredfined_method()){
					methods.add(method);
				}
			}
			
		} catch (ModelToStringException e) {
			e.printStackTrace();
		}
		return methods;
	}
}
