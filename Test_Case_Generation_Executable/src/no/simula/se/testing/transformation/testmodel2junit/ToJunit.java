/*******************************************************************************
 * Copyright (c) 2016, 2017 Simula Research Laboratory. 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Simula license 
 * which accompanies this distribution, and is available at LICENSE.TXT
 *  
 *  Contributors:
 *        Man Zhang
 *******************************************************************************/
package no.simula.se.testing.transformation.testmodel2junit;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.StateMachine;

import no.simula.se.testmodel.TestModel.UTestItem;
import no.simula.se.testmodel.TestModel.UTestModel;
import no.simula.se.testmodel.TestModel.UTestSet;

public class ToJunit {
	
	private static ToJunit instance;
	public static ToJunit getInstance(){
		if(instance == null) instance = new ToJunit();
		return instance;
	}
	
	
	public static void utmodelToJunitPlugin(org.eclipse.uml2.uml.Package umlmodel, UTestModel model, String str_path, 
			String packagePath, ModelToString engine, String[] imports, int[] selected, String output){
		String pack_ = packagePath;
		for(UTestItem item : model.getUtestitem()){
			if(item.getAttachedObject() instanceof org.eclipse.uml2.uml.Package){
				pack_ = pack_+".integrated";
			}else{
				//System.out.println(item.getAttachedObject());
				pack_ = pack_+"."+((NamedElement)item.getAttachedObject()).getName().replaceAll(" ", "").toLowerCase();
			}
			for(UTestSet set: item.getUtestset()){
				String suite_name = ((StateMachine)set.getAttachedObject()).getName().replaceAll(" ", "").replaceAll(":", "");
				
				try {
					String file_folder = str_path+"/"+pack_.replaceAll("\\.", "/");
					Path path = Paths.get(output+"/"+file_folder);
					if (!Files.exists(path)) {
			            try {
			                Files.createDirectories(path);
			            } catch (IOException e) {
			                e.printStackTrace();
			            }
			        }
					Path fPath = Paths.get(output+"/"+file_folder+ "/"+suite_name+".java");
					Files.createDirectories(path);
					
					new TestSuite(suite_name, pack_, set, model, fPath, selected, engine, imports);
				} catch (IOException e) {
					e.printStackTrace();
				}
				
			}
		}
	}
	
	public static void utmodelToJunit(UTestModel model, String str_path, String packagePath, ModelToString engine, String[] imports, int[] selected){
		String pack_ = packagePath;
		for(UTestItem item : model.getUtestitem()){
			if(item.getAttachedObject() instanceof org.eclipse.uml2.uml.Package){
				pack_ = pack_+".integrated";
			}else{
				//System.out.println(item.getAttachedObject());
				if(pack_==null)
					pack_=null;
				else
					pack_ = pack_+"."+((NamedElement)item.getAttachedObject()).getName().replaceAll(" ", "").toLowerCase();
			}
			for(UTestSet set: item.getUtestset()){
				String suite_name = ((StateMachine)set.getAttachedObject()).getName().replaceAll(" ", "").replaceAll(":", "");
				
				try {
					String file_folder = null;
					if(pack_ != null)
						file_folder = str_path+"/"+pack_.replaceAll("\\.", "/");
					else
						file_folder = str_path;
					Path path = Paths.get(file_folder);
					if (!Files.exists(path)) {
			            try {
			                Files.createDirectories(path);
			            } catch (IOException e) {
			                e.printStackTrace();
			            }
			        }
					Path fPath = Paths.get(file_folder+ "/"+suite_name+".java");
					Files.createDirectories(path);
					new TestSuite(suite_name, pack_, set, model, fPath, selected, engine, imports);
				} catch (IOException e) {
					e.printStackTrace();
				}
				
			}
		}
	}
	
	public static void utmodelToConfig(String sm_name, UTestModel model, String str_path, String packagePath, ModelToString engine, String[] imports){
		String pack_ = packagePath;
		
		String file_folder = null;
		if(pack_ != null)
			file_folder = str_path+"/"+pack_.replaceAll("\\.", "/");
		else
			file_folder = str_path;
		Path path = Paths.get(file_folder);
		if (!Files.exists(path)) {
            try {
                Files.createDirectories(path);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
		Path fPath = Paths.get(file_folder+ "/"+sm_name+".java");
		try {
			Files.createDirectories(path);
			new TestSuite(sm_name, pack_, model, fPath, engine, imports, true);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void utmodelToJunit(UTestModel model, String str_path, String packagePath, ModelToString engine, UReporterToString reporter, String[] imports, int[] selected){
		String pack_ = packagePath;
		for(UTestItem item : model.getUtestitem()){
			if(item.getAttachedObject() instanceof org.eclipse.uml2.uml.Package){
				pack_ = pack_+".integrated";
			}else{
				//System.out.println(item.getAttachedObject());
				if(pack_==null)
					pack_=null;
				else
					pack_ = pack_+"."+((NamedElement)item.getAttachedObject()).getName().replaceAll(" ", "").toLowerCase();
			}
			for(UTestSet set: item.getUtestset()){
				String suite_name = ((StateMachine)set.getAttachedObject()).getName().replaceAll(" ", "").replaceAll(":", "");
				
				try {
					String file_folder = null;
					if(pack_ != null)
						file_folder = str_path+"/"+pack_.replaceAll("\\.", "/");
					else
						file_folder = str_path;
					Path path = Paths.get(file_folder);
					if (!Files.exists(path)) {
			            try {
			                Files.createDirectories(path);
			            } catch (IOException e) {
			                e.printStackTrace();
			            }
			        }
					suite_name = suite_name.replaceAll("\\.", "_");
					Path fPath = Paths.get(file_folder+ "/"+suite_name+".java");
					Files.createDirectories(path);
					new TestSuite(suite_name, pack_, set, model, fPath, selected, engine,reporter, imports, true);
				} catch (IOException e) {
					e.printStackTrace();
				}
				
			}
		}
	}
	
	
	
	public static void utmodelToJunit(UTestModel model, String str_path, String packagePath, String suff_tcName,ModelToString engine, UReporterToString reporter, String[] imports, int max){
		String pack_ = packagePath;
		for(UTestItem item : model.getUtestitem()){
			if(item.getAttachedObject() instanceof org.eclipse.uml2.uml.Package){
				pack_ = pack_+".integrated";
			}else{
				//System.out.println(item.getAttachedObject());
				if(pack_==null)
					pack_=null;
				else
					pack_ = pack_+"."+((NamedElement)item.getAttachedObject()).getName().replaceAll(" ", "").toLowerCase();
			}
			
			for(UTestSet set: item.getUtestset()){
				String suite_name = ((StateMachine)set.getAttachedObject()).getName().replaceAll(" ", "").replaceAll(":", "");
				int part =0;
				int sizeOfPart = 20;
				if(set.getUtestcase().size() > max){
					int total = set.getUtestcase().size();
					int start = 0;
					while(start < total){
						int[] selected = new int[total];
						int i = start;
						for(; i < start + max  && i < total;i++){
							selected[i] = 1;
						}
						
						try {
							String file_folder = null;
							if(pack_ != null)
								file_folder = str_path+"-part"+String.valueOf(part/sizeOfPart)+"/"+pack_.replaceAll("\\.", "/");
							else
								file_folder = str_path;
							Path path = Paths.get(file_folder);
							if (!Files.exists(path)) {
					            try {
					                Files.createDirectories(path);
					            } catch (IOException e) {
					                e.printStackTrace();
					            }
					        }
							String setName = suite_name.replaceAll("\\.", "_") + "_"+suff_tcName+"_"+start;
							Path fPath = Paths.get(file_folder+ "/"+setName+".java");
							Files.createDirectories(path);
							new TestSuite(setName, pack_, set, model, fPath, selected, engine,reporter, imports, true);
							start  = i;
							part++;
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}else{
					try {
						String file_folder = null;
						if(pack_ != null)
							file_folder = str_path+"/"+pack_.replaceAll("\\.", "/");
						else
							file_folder = str_path;
						Path path = Paths.get(file_folder);
						if (!Files.exists(path)) {
				            try {
				                Files.createDirectories(path);
				            } catch (IOException e) {
				                e.printStackTrace();
				            }
				        }
						suite_name = suite_name.replaceAll("\\.", "_");
						Path fPath = Paths.get(file_folder+ "/"+suite_name+".java");
						Files.createDirectories(path);
						new TestSuite(suite_name, pack_, set, model, fPath, null, engine,reporter, imports, true);
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				
				
			}
		}
	}
	public static void utmodelToJunit(UTestModel model, String str_path, String packagePath, ModelToString engine, UReporterToString reporter, String[] imports, int[] selectedtotal, int max){
		
		String pack_ = packagePath;
		for(UTestItem item : model.getUtestitem()){
			if(item.getAttachedObject() instanceof org.eclipse.uml2.uml.Package){
				pack_ = pack_+".integrated";
			}else{
				//System.out.println(item.getAttachedObject());
				if(pack_==null)
					pack_=null;
				else
					pack_ = pack_+"."+((NamedElement)item.getAttachedObject()).getName().replaceAll(" ", "").toLowerCase();
			}
			
			for(UTestSet set: item.getUtestset()){
				if(selectedtotal == null)
					selectedtotal = new int[set.getUtestcase().size()]; 
				for(int j = 0; j < selectedtotal.length; j++){
					selectedtotal[j] = 1;
				}
				String suite_name = ((StateMachine)set.getAttachedObject()).getName().replaceAll(" ", "").replaceAll(":", "");
				int part =0;
				int sizeOfPart = 20;
				int total = set.getUtestcase().size();
				int start = 0;
				while(start < total){
					int[] selected = new int[total];
					int i = start;
					int count = 0;
					for(;  count< max && i < total;i++){
						if(selectedtotal[i] == 1){count++;}
						selected[i] = selectedtotal[i];
					}
					
					try {
						String file_folder = null;
						if(pack_ != null)
							file_folder = str_path+"-part"+String.valueOf(part/sizeOfPart)+"/"+pack_.replaceAll("\\.", "/");
						else
							file_folder = str_path;
						Path path = Paths.get(file_folder);
						if (!Files.exists(path)) {
				            try {
				                Files.createDirectories(path);
				            } catch (IOException e) {
				                e.printStackTrace();
				            }
				        }
						String setName = suite_name.replaceAll("\\.", "_") + "_"+start;
						Path fPath = Paths.get(file_folder+ "/"+setName+".java");
						Files.createDirectories(path);
						new TestSuite(setName, pack_, set, model, fPath, selected, engine,reporter, imports, true);
						start  = i;
						part++;
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}
	}
	
	public static void utmodelToJunit(UTestModel model, String str_path, String packagePath, String suff_tcName,ModelToString engine, UReporterToString reporter, String[] imports, int[] selectedtotal, int max){
		
		String pack_ = packagePath;
		for(UTestItem item : model.getUtestitem()){
			if(item.getAttachedObject() instanceof org.eclipse.uml2.uml.Package){
				pack_ = pack_+".integrated";
			}else{
				//System.out.println(item.getAttachedObject());
				if(pack_==null)
					pack_=null;
				else
					pack_ = pack_+"."+((NamedElement)item.getAttachedObject()).getName().replaceAll(" ", "").toLowerCase();
			}
			
			for(UTestSet set: item.getUtestset()){
				String suite_name = ((StateMachine)set.getAttachedObject()).getName().replaceAll(" ", "").replaceAll(":", "");
				int part =0;
				int sizeOfPart = 20;
				int total = set.getUtestcase().size();
				int start = 0;
				if(selectedtotal == null){
					selectedtotal = new int[total];
					for(int i = 0; i < total; i++) selectedtotal[i] =1;
				}
				while(start < total){
					int[] selected = new int[total];
					int i = start;
					int count = 0;
					for(;  count< max && i < total;i++){
						if(selectedtotal[i] == 1){count++;}
						selected[i] = selectedtotal[i];
					}
					
					try {
						String file_folder = null;
						if(pack_ != null)
							file_folder = str_path+"-part"+String.valueOf(part/sizeOfPart)+"/"+pack_.replaceAll("\\.", "/");
						else
							file_folder = str_path;
						Path path = Paths.get(file_folder);
						if (!Files.exists(path)) {
				            try {
				                Files.createDirectories(path);
				            } catch (IOException e) {
				                e.printStackTrace();
				            }
				        }
						String setName = suite_name.replaceAll("\\.", "_") + "_"+suff_tcName+"_"+start;
						Path fPath = Paths.get(file_folder+ "/"+setName+".java");
						Files.createDirectories(path);
						new TestSuite(setName, pack_, set, model, fPath, selected, engine,reporter, imports, true);
						start  = i;
						part++;
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}
	}
	
	public static void utmodelToJunit(UTestModel model, String str_path, String packagePath, ModelToString engine, UReporterToString reporter, String[] imports, int max){
		String pack_ = packagePath;
		for(UTestItem item : model.getUtestitem()){
			if(item.getAttachedObject() instanceof org.eclipse.uml2.uml.Package){
				pack_ = pack_+".integrated";
			}else{
				//System.out.println(item.getAttachedObject());
				if(pack_==null)
					pack_=null;
				else
					pack_ = pack_+"."+((NamedElement)item.getAttachedObject()).getName().replaceAll(" ", "").toLowerCase();
			}
			for(UTestSet set: item.getUtestset()){
				String suite_name = ((StateMachine)set.getAttachedObject()).getName().replaceAll(" ", "").replaceAll(":", "");
				
				if(set.getUtestcase().size() > max){
					int total = set.getUtestcase().size();
					int start = 0;
					while(start < total){
						int[] selected = new int[total];
						int i = start;
						for(; i < start + max  && i < total;i++){
							selected[i] = 1;
						}
						
						try {
							String file_folder = null;
							if(pack_ != null)
								file_folder = str_path+"/"+pack_.replaceAll("\\.", "/");
							else
								file_folder = str_path;
							Path path = Paths.get(file_folder);
							if (!Files.exists(path)) {
					            try {
					                Files.createDirectories(path);
					            } catch (IOException e) {
					                e.printStackTrace();
					            }
					        }
							String setName = suite_name.replaceAll("\\.", "_") +"_"+start;
							Path fPath = Paths.get(file_folder+ "/"+setName+".java");
							Files.createDirectories(path);
							new TestSuite(setName, pack_, set, model, fPath, selected, engine,reporter, imports, true);
							start  = i;
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}else{
					try {
						String file_folder = null;
						if(pack_ != null)
							file_folder = str_path+"/"+pack_.replaceAll("\\.", "/");
						else
							file_folder = str_path;
						Path path = Paths.get(file_folder);
						if (!Files.exists(path)) {
				            try {
				                Files.createDirectories(path);
				            } catch (IOException e) {
				                e.printStackTrace();
				            }
				        } 
						suite_name = suite_name.replaceAll("\\.", "_");
						Path fPath = Paths.get(file_folder+ "/"+suite_name+".java");
						Files.createDirectories(path);
						new TestSuite(suite_name, pack_, set, model, fPath, null, engine,reporter, imports, true);
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				
				
			}
		}
	}
	//fix
	public static void utmodelToJunitAllConfigs(UTestModel model, String str_path, String packagePath, ModelToString engine, String[] imports, int[] selected){
		String pack_ = packagePath;
		for(UTestItem item : model.getUtestitem()){
			if(item.getAttachedObject() instanceof org.eclipse.uml2.uml.Package){
				pack_ = pack_+".integrated";
			}else{
				//System.out.println(item.getAttachedObject());
				if(pack_==null)
					pack_=null;
				else
					pack_ = pack_+"."+((NamedElement)item.getAttachedObject()).getName().replaceAll(" ", "").toLowerCase();
			}
			for(UTestSet set: item.getUtestset()){
				String suite_name = ((StateMachine)set.getAttachedObject()).getName().replaceAll(" ", "").replaceAll(":", "");
				
				try {
					String file_folder = null;
					if(pack_ != null)
						file_folder = str_path+"/"+pack_.replaceAll("\\.", "/");
					else
						file_folder = str_path;
					Path path = Paths.get(file_folder);
					if (!Files.exists(path)) {
			            try {
			                Files.createDirectories(path);
			            } catch (IOException e) {
			                e.printStackTrace();
			            }
			        }
					Path fPath = Paths.get(file_folder+ "/"+suite_name+".java");
					Files.createDirectories(path);
					new TestSuite(suite_name, pack_, set, model, fPath, selected, engine, imports, true);
				} catch (IOException e) {
					e.printStackTrace();
				}
				
			}
		}
	}
	
	public static void utmodelToJunit(UTestModel model, String str_path, String packagePath, ModelToString engine, String[] imports, int[] selected, boolean isSplit){
		if(!isSplit) return;
		String pack_ = packagePath;
		for(UTestItem item : model.getUtestitem()){
			if(item.getAttachedObject() instanceof org.eclipse.uml2.uml.Package){
				pack_ = pack_+".integrated";
			}else{
				pack_ = pack_+"."+((NamedElement)item.getAttachedObject()).getName().replaceAll(" ", "").toLowerCase();
			}
			try {
				String file_folder = str_path+"/"+pack_.replaceAll("\\.", "/");
				Path path = Paths.get(file_folder);
				if (!Files.exists(path)) {
		            try {
		                Files.createDirectories(path);
		            } catch (IOException e) {
		                e.printStackTrace();
		            }
		        }
				for(UTestSet set: item.getUtestset()){
					String suite_name = ((StateMachine)set.getAttachedObject()).getName().replaceAll(" ", "").replaceAll(":", "");
					for(int index = 0; index < set.getUtestcase().size(); index++){
						if(selected == null){
							selected = new int[set.getUtestcase().size()];
						}
						if(index !=0){
							selected[index-1]=0;
						}
						selected[index]=1;
						String customName = suite_name+"_"+index;
						Path fPath = Paths.get(file_folder+ "/"+customName+".java");
						Files.createDirectories(path);
						new TestSuite(customName, pack_, set, model, fPath, selected, engine, imports);
					}
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}
	}
	
	public static void utmodelToJunit(UTestSet set, String str_path, String packagePath, ModelToString engine, String[] imports, int[] selected){
		String pack_ = packagePath;
		String suite_name = ((StateMachine)set.getAttachedObject()).getName().replaceAll(" ", "").replaceAll(":", "");
		
		try {
			String file_folder = str_path+"/"+pack_.replaceAll("\\.", "/");
			Path path = Paths.get(file_folder);
			if (!Files.exists(path)) {
	            try {
	                Files.createDirectories(path);
	            } catch (IOException e) {
	                e.printStackTrace();
	            }
	        }
			Path fPath = Paths.get(file_folder+ "/"+suite_name+".java");
			Files.createDirectories(path);
			new TestSuite(suite_name, pack_, set, null, fPath, selected, engine, imports);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
