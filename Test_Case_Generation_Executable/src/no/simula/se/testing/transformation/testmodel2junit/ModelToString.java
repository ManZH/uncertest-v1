/*******************************************************************************
 * Copyright (c) 2016, 2017 Simula Research Laboratory. 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Simula license 
 * which accompanies this distribution, and is available at LICENSE.TXT
 *  
 *  Contributors:
 *        Man Zhang
 *******************************************************************************/
package no.simula.se.testing.transformation.testmodel2junit;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.eclipse.uml2.uml.CallEvent;
import org.eclipse.uml2.uml.ChangeEvent;
import org.eclipse.uml2.uml.Constraint;
import org.eclipse.uml2.uml.Event;
import org.eclipse.uml2.uml.InstanceSpecification;
import org.eclipse.uml2.uml.LiteralString;
import org.eclipse.uml2.uml.SignalEvent;
import org.eclipse.uml2.uml.State;
import org.eclipse.uml2.uml.TimeEvent;
import org.eclipse.uml2.uml.Transition;

import no.simula.se.testing.execution.api.RegisterAPI;

public abstract class ModelToString {
	
	/*
	 * @author: Man Zhang
	 * 
	 * */
	protected int flag = 0;
	private final String timePatternStr = "^(\\d+)(min|s|h)$";
	private final Pattern  timePattern = Pattern.compile(timePatternStr);
	protected UReporterToString reEngine;
	protected Map<String, InstanceSpecification> insMap;
	
	public void setReEngine(UReporterToString reEngine){this.reEngine = reEngine;}
	public void setInsMap(Map<String, InstanceSpecification> insMap){this.insMap = insMap;}
	
	public List<String> transitionToString(Transition transition, String var) throws ModelToStringException{
		List<String> list=new ArrayList<String>();
		CommentUtil.getInstance();
		list.add(CommentUtil.getSigleJavaComment("transition:"+transition.getName()));
		if(transition.getTriggers().size() == 0){
			System.err.println("no trigger in the transition. (transition: "+transition.getName()+").");
		}else {
			if(transition.getTriggers().size() > 1){
				System.err.println("the size of triggers of trainstion is more than 1. (transition: "+transition.getName()+").");
			}
			Event event = transition.getTriggers().get(0).getEvent();
			if(event instanceof CallEvent){
				//if(transition.getGuard()!=null) list.addAll(this.guardToString(transition.getGuard()));
				list.addAll(this.callEventToString(transition, (CallEvent)event, var));
			}else if(event instanceof SignalEvent){
				//if(transition.getGuard()!=null) list.addAll(this.guardToString(transition.getGuard()));
				list.addAll(this.signalEventToString(transition, (SignalEvent)event, var));
			}else if(event instanceof TimeEvent){
				list.addAll(this.timeEventToString((TimeEvent)event,var));
			}else if(event instanceof ChangeEvent){
				list.addAll(this.changeEventToString(transition, (ChangeEvent)event, var));
			}else
				throw new ModelToStringException("the type of event of trainsition is undefined. (transition: "+transition.getName()+").");
		}
		return list;
	}
	
	
	public List<String> timeEventToString(TimeEvent event, String var){
		List<String> list = new ArrayList<String>();
		list.add(CommentUtil.getSigleJavaComment("from now, the time event is implemented as thread sleep!"));
		String str = ((LiteralString)((TimeEvent)event).getWhen().getExpr()).getValue();
		str =str.replaceAll("after", "");
		str =str.replaceAll(" ", "");
		try{
			
			list.add("try {Thread.sleep(new BigDecimal("+Long.parseLong(str)+").intValueExact());}\n  catch (InterruptedException e) {e.printStackTrace();}");
		}catch(NumberFormatException ex){
			Matcher m = timePattern.matcher(str);
			if (m.find( )) {
				switch(m.group(2)){
				case "min":
					list.add("try {Thread.sleep(new BigDecimal("+(Long.parseLong(m.group(1)) * 60 *1000)+").intValueExact());}\n  catch (InterruptedException e) {e.printStackTrace();}");
					break;
				case "s":
					list.add("try {Thread.sleep(new BigDecimal("+(Long.parseLong(m.group(1)) * 1000)+").intValueExact());}\n  catch (InterruptedException e) {e.printStackTrace();}");
					break;
				case "h":
					list.add("try {Thread.sleep(new BigDecimal("+(Long.parseLong(m.group(1)) * 60 * 60 * 1000)+").intValueExact());}\n  catch (InterruptedException e) {e.printStackTrace();}");
					break;
				}
			} else {
				list.add("try {Thread.sleep(new BigDecimal("+var+"."+str+").intValueExact());}\n catch (InterruptedException e) {e.printStackTrace();}");
	        }
			
		}
		return list;
	}
	
	public abstract List<String> simpleStateToString(State state, String var) throws ModelToStringException;
	public abstract List<String> callEventToString(Transition transition, CallEvent event, String var) throws ModelToStringException;
	public abstract List<String> signalEventToString(Transition transition, SignalEvent event, String var) throws ModelToStringException;
	public abstract List<String> changeEventToString(Transition transition, ChangeEvent event, String var) throws ModelToStringException;
	public abstract List<String> guardToString(Constraint guard, String var) throws ModelToStringException;

	public abstract List<String> createInstance(String var, InstanceSpecification instance) throws ModelToStringException;
	public abstract List<String> createDeclartion(String var, InstanceSpecification instance) throws ModelToStringException;
	public abstract RegisterAPI getRegisterAPI() throws ModelToStringException;
	
	public UReporterToString getReporter(){
		return this.reEngine;
	}
	
	public String getTimeout(){
		return null;
	}
}
