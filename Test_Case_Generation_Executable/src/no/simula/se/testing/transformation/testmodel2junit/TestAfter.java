/*******************************************************************************
 * Copyright (c) 2016, 2017 Simula Research Laboratory. 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Simula license 
 * which accompanies this distribution, and is available at LICENSE.TXT
 *  
 *  Contributors:
 *        Man Zhang
 *******************************************************************************/
package no.simula.se.testing.transformation.testmodel2junit;

import java.util.List;

public class TestAfter implements TestFunctionTemplate{
	private String name;
	private List<String> content;
	public TestAfter(){
		this.name = "afterHandle";
	}
	
	public TestAfter(List<String> content){
		this.name = "afterHandle";
		this.content = content;
	}
	
	public TestAfter(String name, List<String> content){
		this.name = name;
		this.content = content;
	}
	public TestAfter(String name){
		this.name = name;
	}

	@Override
	public String getAnnoation() {
		return "@After";
	}

	@Override
	public List<String> getContent() {
		return content;
	}

	@Override
	public String getBegin() {
		return generateBegin(name);
	}

}
