/*******************************************************************************
 * Copyright (c) 2016, 2017 Simula Research Laboratory. 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Simula license 
 * which accompanies this distribution, and is available at LICENSE.TXT
 *  
 *  Contributors:
 *        Man Zhang
 *******************************************************************************/
package no.simula.se.testing.transformation.testmodel2junit;

import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.uml2.uml.Transition;

import no.simula.se.testing.utility.belief.BeliefUtility;
import no.simula.se.testing.utility.belief.BeliefUtilityException;

public abstract class UReporterToString {
	
	//<state, transition, state>::boolean
	private String name;
	private String unSour;
	private String unTran;
	private String unTarg;
	
	private Transition transition;
	private EObject uncertainty;
	//indspec <name, constraint, var>::boolean
//	private String[] indspec;
//	
//	private String[] alter;
	
	
	public UReporterToString(){};
	public UReporterToString(String name){this.name = name;}
	
	public abstract List<String> reportUncertainty(String pars, String eobjs, String var, String current);
	public abstract List<String> setUncertainty(String unId, String dyEval, String[] indsId, String[] dyIndsEval);
	public abstract List<String> setAltUncertainty(String unId, String altUnId, String dyEval, String[] indsId, String[] dyIndsEval);
	public abstract List<String> setAnotherUncertainty(String unId, String altUnId, String dyEval, String[] indsId, String[] dyIndsEval);
	public abstract String reportAnotherState(String stName, String sinEval);
	public abstract String reportTestStep(String info);
	public abstract String reportTestStep(String info, String[] runtime);
	public String getName(){
		return name;
	}
	public void setCurDefUnObj(Transition transition){
		try {
			this.transition = transition;
			this.uncertainty = BeliefUtility.getUncertaintiesWithCause(transition);
			this.unSour = transition.getSource().getName();
			this.unTarg = transition.getTarget().getName();
			this.unTran = transition.getName();
		} catch (BeliefUtilityException e) {
			e.printStackTrace();
		}
	}
	
	public EObject getUncertainty(){
		return this.uncertainty;
	}
	public String getUncertaintyId(){
		return String.format("<%1s, %2s, %3s>", this.unSour, this.unTran, this.unTarg);
	}
	public String getIndSpecId(String name, String body, String var){
		if(name == null) name = "";
		return String.format("<%1s, %2s, %3s>", name, body, var);
	}
	
	public String getUnSour() {
		return unSour;
	}
	public String getUnTran() {
		return unTran;
	}
	public String getUnTarg() {
		return unTarg;
	}
	public Transition getTransition(){
		return transition;
	}
	
	public void init(){
		 name= null;
		 unSour= null;
		 unTran= null;
		 unTarg= null;
		
		transition= null;
		uncertainty= null;
		
	}
	
}
