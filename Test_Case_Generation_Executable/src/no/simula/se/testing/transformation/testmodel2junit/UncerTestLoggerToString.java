/*******************************************************************************
 * Copyright (c) 2016, 2017 Simula Research Laboratory. 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Simula license 
 * which accompanies this distribution, and is available at LICENSE.TXT
 *  
 *  Contributors:
 *        Man Zhang
 *******************************************************************************/
package no.simula.se.testing.transformation.testmodel2junit;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class UncerTestLoggerToString extends UReporterToString {

	@Override
	public List<String> reportUncertainty(String pars, String eobjs, String var, String current) {

		return Arrays.asList(getName() + ".reportUncerInfo("+pars+","+eobjs+","+var+","+current+");");
	}

	@Override
	public List<String> setUncertainty(String unId, String dyEval, String[] indsId, String[] dyIndsEval) {
		List<String> details = new ArrayList<String>();
		String parIndsId = "";
		String parDyIndsEval = "";
		if (indsId == null) {
			parIndsId = "null";
			parDyIndsEval = "null";
		} else {
			parIndsId = "Arrays.asList(";
			parDyIndsEval = "MOCLUtil.getInstance().toboolenArray(Arrays.asList(";
			for (int i = 0; i < indsId.length; i++) {
				parIndsId = parIndsId + "\"" + indsId[i] + "\"";
				parDyIndsEval = parDyIndsEval + dyIndsEval[i];
				if (i != indsId.length - 1) {
					parIndsId = parIndsId + ",";
					parDyIndsEval = parDyIndsEval + ",";
				}
			}
			parIndsId = parIndsId + ").toArray(new String["+indsId.length+"])";
			parDyIndsEval = parDyIndsEval + "))";
		}
		details.add(
				getName() + ".setUncerInfo(\"" + unId + "\"," + dyEval + "," + parIndsId + "," + parDyIndsEval + ");");
		return details;
	}

	@Override
	public List<String> setAltUncertainty(String unId, String altUnId, String dyEval, String[] indsId,
			String[] dyIndsEval) {
		List<String> details = new ArrayList<String>();
		String parIndsId = "";
		String parDyIndsEval = "";
		if (indsId == null) {
			parIndsId = "null";
			parDyIndsEval = "null";
		} else {
			parIndsId = "Arrays.asList(";
			parDyIndsEval = "MOCLUtil.getInstance().toboolenArray(Arrays.asList(";
			for (int i = 0; i < indsId.length; i++) {
				parIndsId = parIndsId + "\"" + indsId[i] + "\"";
				parDyIndsEval = parDyIndsEval + dyIndsEval[i];
				if (i != indsId.length - 1) {
					parIndsId = parIndsId + ",";
					parDyIndsEval = parDyIndsEval + ",";
				}
			}
			parIndsId = parIndsId + ").toArray(new String["+indsId.length+"])";
			parDyIndsEval = parDyIndsEval + "))";
		}

		details.add(getName() + ".setAlterUncerInfo(\"" + unId + "\",\"" + altUnId + "\"," + dyEval + "," + parIndsId
				+ "," + parDyIndsEval + ");");
		return details;

	}
	
	@Override
	public List<String> setAnotherUncertainty(String unId, String altUnId, String dyEval, String[] indsId,
			String[] dyIndsEval) {
		List<String> details = new ArrayList<String>();
		String parIndsId = "";
		String parDyIndsEval = "";
		if (indsId == null) {
			parIndsId = "null";
			parDyIndsEval = "null";
		} else {
			parIndsId = "Arrays.asList(";
			parDyIndsEval = "MOCLUtil.getInstance().toboolenArray(Arrays.asList(";
			for (int i = 0; i < indsId.length; i++) {
				parIndsId = parIndsId + "\"" + indsId[i] + "\"";
				parDyIndsEval = parDyIndsEval + dyIndsEval[i];
				if (i != indsId.length - 1) {
					parIndsId = parIndsId + ",";
					parDyIndsEval = parDyIndsEval + ",";
				}
			}
			parIndsId = parIndsId + ").toArray(new String["+indsId.length+"])";
			parDyIndsEval = parDyIndsEval + "))";
		}

		details.add(getName() + ".setAnotherUncerInfo(\"" + unId + "\",\"" + altUnId + "\"," + dyEval + "," + parIndsId
				+ "," + parDyIndsEval + ");");
		return details;

	}

	public String getName() {
		return "UncerTestReporter";
	}

	@Override
	public String reportTestStep(String info) {
		return getName() + ".reportTestStep(\"" + info + "\");";
	}

	@Override
	public String reportTestStep(String info, String[] runtime) {
		String vars = "";
		for(String r: runtime){
			vars = vars+"+\" \"+"+r;
		}
		 return getName() + ".reportTestStep(\"" + info + ":\""+vars+");";
	}

	@Override
	public String reportAnotherState(String stName, String sinEval) {
		 return getName() + ".reportTestStep(\"" + stName + ":\""+sinEval+");";
	}
}
