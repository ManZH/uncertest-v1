/*******************************************************************************
 * Copyright (c) 2016, 2017 Simula Research Laboratory. 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Simula license 
 * which accompanies this distribution, and is available at LICENSE.TXT
 *  
 *  Contributors:
 *        Man Zhang
 *******************************************************************************/
package no.simula.se.testing.utility.belief;

import java.util.Iterator;

import org.jgrapht.GraphPath;
import org.jgrapht.alg.shortestpath.FloydWarshallShortestPaths;

import no.simula.se.testing.strategies.jgraph.SMGraph;
import no.simula.se.testing.strategies.jgraph.UMLCompositeState;
import no.simula.se.testing.strategies.jgraph.UMLRegionDirectedGraph;
import no.simula.se.testing.strategies.jgraph.UMLTransitionWeightEdge;
import no.simula.se.testing.strategies.jgraph.UMLVertex;

public class GraphUtility {
	public static int requestMinLengthOfSM(SMGraph sm){
		int length = -1;
		for(UMLRegionDirectedGraph<UMLVertex, UMLTransitionWeightEdge> graph : sm.getParallels()){
			FloydWarshallShortestPaths<UMLVertex, UMLTransitionWeightEdge> fw =
	                new FloydWarshallShortestPaths<>(graph);
			for(UMLVertex v1 : graph.getEnters()){
				for(UMLVertex v2 : graph.getExits()){
					GraphPath<UMLVertex, UMLTransitionWeightEdge> path = fw.getPath(v1, v2);
					int tmp = path.getLength();
					Iterator<UMLVertex> it = path.getVertexList().iterator();
					int shortest1 = -1;
					while(it.hasNext()){
						UMLVertex inv = it.next();
						if(inv instanceof UMLCompositeState){
							System.out.println("composite state"+inv);
							int tmp1 = requestMinLengthOfSM(((UMLCompositeState)inv).getGraph());
							if(shortest1 == -1 || tmp1 < shortest1) shortest1 = tmp1;
						}
					}
					if(shortest1 != -1){tmp = tmp + shortest1;}
					if(length == -1 || tmp < length){ length = tmp;}
				}
			}
		}
		 
		return length;
	}
}
