/*******************************************************************************
 * Copyright (c) 2016, 2017 Simula Research Laboratory. 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Simula license 
 * which accompanies this distribution, and is available at LICENSE.TXT
 *  
 *  Contributors:
 *        Man Zhang
 *******************************************************************************/
package no.simula.se.testing.utility.belief;

import org.eclipse.uml2.uml.Element;

public class UTP2Utility {
	public final static String TESTCONTEXT = "UTP::TestContext";
	public final static String TESTDESIGNINPUT = "UTP::TestDesignInput";
	public final static String TESTCONFIGURATION = "UTP::TestConfiguration";
	public final static String CHECKPROPERTYACTION = "UTP::CheckPropertyAction";
	//public final static String INDSPEC = "CoreProfile::IndeterminacySpecification";
	
	
	public static UTP2Utility instance;
	public static UTP2Utility getInstance(){
		if(instance == null) instance = new UTP2Utility();
		return instance;
	}
	
	public static boolean isApplyTestContext(Element element){
		return BeliefUtility.isApplyStereotype(element, TESTCONTEXT);
	}
	
	public static boolean isApplyTestConfiguration(Element element){
		return BeliefUtility.isApplyStereotype(element, TESTCONFIGURATION);
	}
	
	public static boolean isApplyTestDesignInput(Element element){
		return BeliefUtility.isApplyStereotype(element, TESTDESIGNINPUT);
	}
	
	public static boolean isApplyCheckPropertyAction(Element element){
		return BeliefUtility.isApplyStereotype(element, CHECKPROPERTYACTION);
	}
}
