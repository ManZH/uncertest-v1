/*******************************************************************************
 * Copyright (c) 2016, 2017 Simula Research Laboratory. 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Simula license 
 * which accompanies this distribution, and is available at LICENSE.TXT
 *  
 *  Contributors:
 *        Man Zhang
 *******************************************************************************/
package no.simula.se.testing.utility.belief;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.management.JMException;

import org.eclipse.uml2.uml.StateMachine;
import org.eclipse.uml2.uml.Transition;

import no.simula.se.testmodel.TestModel.UMSpaceA;
import no.simula.se.testmodel.TestModel.UTestCase;
import no.simula.se.testmodel.TestModel.UTestPath;
import no.simula.se.testmodel.TestModel.UTestSet;

public class UncertaintyInfoCalcautor {

	//private static final long serialVersionUID = 1920436901054634324L;
	
	private UTestSet set;
	private int[] objs;
    private double[] objVals;
    int numOfVar;
	public UncertaintyInfoCalcautor(UTestSet set, int[] objectives){
		this.set = set;
		
		numOfVar  = set.getUtestcase().size();
		int i = 0;
		for(int ob: objectives){
			if(ob == 1) i++;
		}
		objVals=new double[i];
		this.objs = objectives;
		
		
		
	}
	
    // please create file for save the values - objVals
	public double[]  evaluate(int[] solution) throws JMException {
		// 0 - #test case; 1 - #uncertainty; 2 - ratio of uncertainty in terms of total uncertainties; 3 - ration of uncertainty in terms of the length of test cases
		// 4 - all uncertainty; 5 - all transitions
		BeliefUtility.getInstance();
		Set all_u = null;
		Set<Transition> all_t = null;
		Set<Transition> included = null;
		Set included_uncertaintes = null;
        List<Transition> includedT = null;
		
		if(objs[5] == 1 ){included = new HashSet();}
		if(objs[6] == 1) included_uncertaintes = new HashSet<Object>();
        if(objs[7] == 1) {includedT = new ArrayList<Transition>();}
		try {
			all_u = BeliefUtility.getUncertainties((StateMachine)set.getAttachedObject());
			all_t = BeliefUtility.getAllOwnedTransition((StateMachine)set.getAttachedObject());
		
			double total_u = all_u.size();
			double total_t = all_t.size();
			
			double numOfTC = 0;
			double degree = 0.0;
			double numOfUncer = 0;
			double ratioOfUncer = 0.0;
		    //double [] f = new double[numberOfObjectives_];   
			//System.out.println(x.getNumberOfDecisionVariables());
		    for (int i = 1; i < numOfVar;i++){
		        if(solution[i] > 0.5){
		        	numOfTC++;
		        	UTestCase ut = set.getUtestcase().get(i);
		        	if(ut instanceof UTestPath) {
		        		degree = degree + ((UTestPath)ut).getMeasuredValue();
		        	}
		        	if(objs[4]==1)all_u.removeAll(BeliefUtility.getUncertainties(ut));
		        	if(objs[5]==1)included.addAll(BeliefUtility.getAllOwnedTransition(ut));
		        	if(objs[6]==1)included_uncertaintes.addAll(BeliefUtility.getUncertainties(ut));
		        	if(objs[7]==1) includedT.addAll(BeliefUtility.getListOfTransitions(ut));
		        	numOfUncer = numOfUncer + ut.getUinfo().get(0);
		        	ratioOfUncer = ratioOfUncer + ut.getUinfo().get(2);
		        }
		    }
//		    double total = numberOfVariables_;
		    
		   
		    int i = 0;
		    if(objs[0] == 1){
		    	objVals[i] = numOfTC; // number of selected test cases
		    	i++;
		    }
		    if(objs[1] == 1){
		    	objVals[i] = degree/numOfTC; // average of uncertainty measure
		    	i++;
		    }
		    if(objs[2] == 1){
		    	objVals[i] =  numOfUncer; // numebr of uncertainty
		    	i++;
		    }
		    if(objs[3] == 1){
		    	objVals[i] = ratioOfUncer/numOfTC; // average of uncertainty not used (skip)
		    	i++;
		    }
		    if(objs[4] == 1){
		    	double final_u = all_u.size();
		    	objVals[i] = final_u; // number of unique uncertainty
		    	i++;
		    }
		    if(objs[5] == 1){
		    	//double final_t = all_t.size();
		    	double precen_t = BeliefUtility.getPrentageOfTransition(all_t, included);
		    	objVals[i] = precen_t;
		    	i++;
		    }
		    if(objs[6] == 1){
		    	double total_sm = set.getUmspacea().size();
		    	Set<UMSpaceA> totals = new HashSet<UMSpaceA>();
		    	for(Object obj : included_uncertaintes){
		    		for(UMSpaceA sm : set.getUmspacea()){
		    			if(sm.getUms().contains(obj)) totals.add(sm);
		    		}
		    	}
		    	double included_sm = totals.size();
		    	objVals[i] = included_sm;
		    	i++;
		    }
            if(objs[7] == 1){
                //double final_t = all_t.size();
                objVals[i] = includedT.size();
                i++;
            }
		    return objVals;
		} catch (BeliefUtilityException e) {
			e.printStackTrace();
			return null;
		}
	}
}
