/*******************************************************************************
 * Copyright (c) 2016, 2017 Simula Research Laboratory. 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Simula license 
 * which accompanies this distribution, and is available at LICENSE.TXT
 *  
 *  Contributors:
 *        Man Zhang
 *******************************************************************************/
package no.simula.se.testing.utility.belief;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.EcoreEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.uml2.common.util.UML2Util.EObjectMatcher;
import org.eclipse.uml2.uml.AnyReceiveEvent;
import org.eclipse.uml2.uml.CallEvent;
import org.eclipse.uml2.uml.ChangeEvent;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Constraint;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Event;
import org.eclipse.uml2.uml.LiteralString;
import org.eclipse.uml2.uml.OpaqueExpression;
import org.eclipse.uml2.uml.Pseudostate;
import org.eclipse.uml2.uml.PseudostateKind;
import org.eclipse.uml2.uml.Region;
import org.eclipse.uml2.uml.SignalEvent;
import org.eclipse.uml2.uml.State;
import org.eclipse.uml2.uml.StateMachine;
import org.eclipse.uml2.uml.Stereotype;
import org.eclipse.uml2.uml.TimeEvent;
import org.eclipse.uml2.uml.Transition;
import org.eclipse.uml2.uml.Trigger;
import org.eclipse.uml2.uml.UMLPackage;
import org.eclipse.uml2.uml.Vertex;
import org.eclipse.uml2.uml.util.UMLUtil;

import no.simula.se.testing.generator.atc.GenerationConfig;
import no.simula.se.testing.generator.atc.ToUTestModel;
import no.simula.se.testing.strategies.jgraph.GraphsException;
import no.simula.se.testing.strategies.jgraph.SMGraph;
import no.simula.se.testing.strategies.jgraph.UMLParallelPath;
import no.simula.se.testing.utility.ModelUtil;
import no.simula.se.testmodel.TestModel.UTestAction;
import no.simula.se.testmodel.TestModel.UTestCase;
import no.simula.se.testmodel.TestModel.UTestCaseElement;
import no.simula.se.testmodel.TestModel.UTestItem;
import no.simula.se.testmodel.TestModel.UTestModel;
import no.simula.se.testmodel.TestModel.UTestParallel;
import no.simula.se.testmodel.TestModel.UTestPath;
import no.simula.se.testmodel.TestModel.UTestSet;

public class BeliefUtility {
	public final static String BELIEF = "CoreProfile::BeliefElement";
	public final static String CAUSE = "CoreProfile::Cause";
	public final static String INDSOURCE = "CoreProfile::IndeterminacySource";
	public final static String INDSINPUT = "CoreProfile::IndeterminacySourceInput";
	public final static String INDSPEC = "CoreProfile::IndeterminacySpecification";
	
	
	public static BeliefUtility instance;
	public static BeliefUtility getInstance(){
		if(instance == null) instance = new BeliefUtility();
		return instance;
	}
	
	public static int[] modeInfo(org.eclipse.uml2.uml.Package model, boolean includePackage){
		int[] des = {0,0,0,0,0};
		// 0 - number of class, 1 - number of relationship, 2- number of belief, 3- number of indeterminacy source, 4- indeterminacy specification
		for(Element e : model.allOwnedElements()){
			if(e instanceof Classifier){
//				if(((Classifier) e).getName().equals("ButtonState")){
//					System.err.println("get Emu");
//				}
				des[0] ++;
				if(BeliefUtility.isApplyBelief(e)){
					des[2]++;
				}else if(BeliefUtility.isApplyIndSource(e)){
					des[3]++;
				}
			}else if(e instanceof  org.eclipse.uml2.uml.Relationship){
				des[1] ++;
			}else if(e instanceof Constraint && BeliefUtility.isApplyIndSpec(e)){
				des[4]++;
			}else if(e instanceof org.eclipse.uml2.uml.Package && includePackage){
				int[] s = modeInfo((org.eclipse.uml2.uml.Package)e, includePackage);
				for(int i = 0; i < s.length; i++){
					des[i] = des[i]+s[i];
				}
			}
		}
		return des;
	}
	
	public static Set<EObject> getCauses(StateMachine sm){
		Set<EObject> set = new HashSet<EObject>();
		for(Element e : sm.allOwnedElements()){
			if(BeliefUtility.isApplyCause(e)){
				set.add(e);
				if(e instanceof State && ((State)e).isSubmachineState()){
					set.addAll(getCauses(((State)e).getSubmachine()));
				}
			}	
			
		}
		return set;
	}
	
	public static Set<EObject> getBeliefs(StateMachine sm){
		Set<EObject> set = new HashSet<EObject>();
		for(Element e : sm.allOwnedElements()){
			if(e instanceof Vertex){
				if(BeliefUtility.isApplyBelief(e)){
					set.add(e);
					if(e instanceof State && ((State)e).isSubmachineState()){
						set.addAll(getBeliefs(((State)e).getSubmachine()));
					}
				}	
			}
			
		}
		return set;
	}
	
	public static int sizeOfSM(StateMachine sm){
		int i = sm.allOwnedElements().size();
		for(Element e : sm.allOwnedElements()){
			if(e instanceof Vertex){
				if(e instanceof State && ((State)e).isSubmachineState()){
					i = i + sizeOfSM(((State)e).getSubmachine());
				}	
			}
			
		}
		return i;
	}
	
	public static Set<EObject> getIndSource(StateMachine sm) throws BeliefUtilityException{
		Set<EObject> set = new HashSet<EObject>();
		for(EObject uncerObj : getUncertainties(sm)){
			Object value = uncerObj.eGet(uncerObj.eClass().getEStructuralFeature("referredIndeterminacySource"));
			if(value instanceof EcoreEList && ((EList<?>) value).size() > 0){
				EList<?> mlist = (EList<?>) value;
				for(int i = 0; i < mlist.size(); i++){
					set.add((EObject) mlist.get(i));
				}
			}
		}
		return set;
		
	}
	
	public static Set<EObject> getIndSps(StateMachine sm) throws BeliefUtilityException{
//		for(EObject mObj : ){
//			if(mObj instanceof Element){
//				Stereotype inds = ((Element)mObj).getAppliedStereotype(INDSOURCE);
//				Object taggedValue = ((Element)mObj).getValue(inds, "triggeredBy");
//				if(taggedValue != null && taggedValue instanceof EList && ((EList<?>)taggedValue).size() > 0){
//					for(int j = 0; j < ((EList<?>)taggedValue).size(); j++){
//						Object obj = ((EList<?>)taggedValue).get(j);
//						if(obj.equals(ac.getOperation())){
//							af = true;
//						}
//						
//						if(obj.equals(bc.getOperation())){
//							bf = true;
//						}
//					}
//				}
//			}
//		}
		
		Set<EObject> set = new HashSet<EObject>();
		for(EObject uncerObj : getUncertainties(sm)){
			EList<?> inlist = getIndSpecsOfUncertainty(uncerObj);
			if(inlist != null && inlist.size() > 0){
				for(int i = 0; i < inlist.size(); i++){
					set.add((EObject) inlist.get(i));
				}
			}
		}
		return set;
	}
	
	public static Set<EObject> getUncertainties(StateMachine sm) throws BeliefUtilityException{
		Set<EObject> set = new HashSet<EObject>();
		for(Element e : sm.allOwnedElements()){
			if(e instanceof Vertex){
				if(BeliefUtility.isApplyBelief(e)){
					if(getUncertainties(e) != null)
						set.addAll(Arrays.asList(getUncertainties(e)));
					if(e instanceof State && ((State)e).isSubmachineState()){
						//System.err.println(" submachine  "+getUncertainties(((State)e).getSubmachine()).size());
						
						set.addAll(getUncertainties(((State)e).getSubmachine()));
					}
				}	
			}
			
		}
		return set;
	}
	public static Collection<EObject> getAllStates(State state){
		return EcoreUtil.getObjectsByType(state.getOwner().getOwnedElements(), UMLPackage.Literals.STATE);
	}
	
	public static void calculateCoverage(org.eclipse.uml2.uml.Package model, GenerationConfig[] configs) throws GraphsException, BeliefUtilityException{
		ToUTestModel toUTestModel = new ToUTestModel(configs);
		UTestModel umodel =  toUTestModel.converToUTestModel(model);
		reportCoverage(umodel, model, configs);
	}
	
	public static void reportCoverage(UTestModel result, org.eclipse.uml2.uml.Package model, GenerationConfig[] configs) throws BeliefUtilityException{
		System.err.println("=========== overall of the coverage =================");
		for(UTestItem item : result.getUtestitem()){
			for(UTestSet set : item.getUtestset()){
				String name = ((StateMachine)set.getAttachedObject()).getName();
				
				BeliefUtility.printSMDetails((StateMachine)set.getAttachedObject());
				
				GenerationConfig config = findConfig(name, configs);
				if(config != null){
					System.err.println("State Machine: "+name);
					config.setTransitions_coverage(BeliefUtility.printCoverageTransition(set));
					config.setUncertainties_coverage(BeliefUtility.printCoverageUncertainty(set));
					System.err.println("the coverage of transition is "+config.getTransitions_coverage());
					System.err.println("the coverage of uncertainties is "+config.getUncertainties_coverage());
					System.err.println("---------------------");
				}
			}
		}
		System.err.println("===================================================");
	}
	
	public static Map<EObject, Transition> getAlterUncertainties(Transition tran) throws BeliefUtilityException{
		Map<EObject, Transition> maps =  new HashMap<EObject, Transition>();
		if(tran.getSource() instanceof Pseudostate && ((Pseudostate)tran.getSource()).getKind().equals(PseudostateKind.CHOICE_LITERAL)){
			Pseudostate source = (Pseudostate)tran.getSource();
			
			for(Transition t : source.getOutgoings()){
				if(! t.equals(tran)){
					if(isSameTranstion(t, tran)){
						EObject unObj = getUncertaintiesWithCause(t);
						maps.put(unObj, t);
					}
				}
			}
		}else if(tran.getSource() instanceof State){
			State source = (State)tran.getSource();
			
			for(Transition t : source.getOutgoings()){
				if(! t.equals(tran)){
					if(isSameTranstion(t, tran)){
						EObject unObj = getUncertaintiesWithCause(t);
						maps.put(unObj, t);
					}
				}
			}
		}else{
			throw new BeliefUtilityException("new type of state is related to belief:"+tran.getTarget());
		}
		
		
		return maps;
	}
	
	public static Map<EObject, Transition> getAllRestUncertainties(Transition curTrans) throws BeliefUtilityException{
		Map<EObject, Transition> maps =  new HashMap<EObject, Transition>();
		//find region
		if(curTrans.getOwner() instanceof Region){
			Collection<EObject> eTrans = EcoreUtil.getObjectsByType(curTrans.getOwner().getOwnedElements(), UMLPackage.Literals.TRANSITION);
			for(EObject eTran : eTrans){
				if(eTran instanceof Transition && !eTran.equals(curTrans)){
					EObject unObj = getUncertaintiesWithCause((Transition)eTran);
					if(unObj != null){maps.put(unObj, (Transition)eTran);}
				}
			}
		}else{
			throw new BeliefUtilityException("find proper owner");
		}
		
		
//		if(tran.getSource() instanceof Pseudostate && ((Pseudostate)tran.getSource()).getKind().equals(PseudostateKind.CHOICE_LITERAL)){
//			Pseudostate source = (Pseudostate)tran.getSource();
//			
//			for(Transition t : source.getOutgoings()){
//				if(! t.equals(tran)){
//					if(isSameTranstion(t, tran)){
//						EObject unObj = getUncertaintiesWithCause(t);
//						maps.put(unObj, t);
//					}
//				}
//			}
//		}else if(tran.getSource() instanceof State){
//			State source = (State)tran.getSource();
//			
//			for(Transition t : source.getOutgoings()){
//				if(! t.equals(tran)){
//					if(isSameTranstion(t, tran)){
//						EObject unObj = getUncertaintiesWithCause(t);
//						maps.put(unObj, t);
//					}
//				}
//			}
//		}else{
//			throw new BeliefUtilityException("new type of state is related to belief:"+tran.getTarget());
//		}
//		
		
		return maps;
	}
	
	public static GenerationConfig findConfig(String name, GenerationConfig[] configs){
		for(GenerationConfig config : configs){
			if(name.equals(config.getSm_name()))
				return config;
		}
		return null;
	}
	
	public static int reportAbMaxLength(SMGraph sm){
		int max = -1;
		for(UMLParallelPath p : sm.getPaths()){
			if(p.getCurrrentLength() > max){
				max = p.getCurrrentLength();
			}
		}
		return max;
	}
	

	
	public static boolean isApplyBelief(Element element){
		return isApplyStereotype(element, BELIEF);
	}
	
	public static boolean isApplyCause(Element element){
		return isApplyStereotype(element, CAUSE);
	}
	
	public static boolean isApplyIndSource(Element element){
		return isApplyStereotype(element, INDSOURCE);
	}
	
	public static boolean isApplyIndSpec(Element element){
		return isApplyStereotype(element, INDSPEC);
	}
	
	public static boolean isApplyIndSInput(Element element){
		return isApplyStereotype(element, INDSINPUT);
	}
	
	public static boolean isApplyStereotype(Element element, String...strs){
		for(String str : strs){
			if(element.getAppliedStereotype(str) == null)
				return false;
		}
		return true;
	}
	
	public static boolean isPattern(Element[] elems, String[] strs){
		if(elems.length != strs.length) return false;
		int l = elems.length;
		for(int i = 0; i < l; i++){
			if(elems[i].getAppliedStereotype(strs[i]) == null) return false;
		}
		return true;
	}
	
	public static void printUncertainty(org.eclipse.uml2.uml.Package model,Element element) throws BeliefUtilityException{
		EObject[] uncertainties = getUncertainties(element);
		System.out.println("the size of uncertainties is "+uncertainties.length);
		for(int i = 0; i < uncertainties.length; i++){
			System.out.println((i+1)+": referred cause is "+ getPropertyValue(uncertainties[i], "referredCause")[0]+" measuredValue is "+getDoubleMeasurementOfUncertainty(uncertainties[i])[0]);
		}
	}
	

	public static EObject[] getUncertainties(Element element) throws BeliefUtilityException{
		EObject[] list = null;
		if(!isApplyBelief(element)) return list;
		Stereotype stereo = element.getAppliedStereotype(BELIEF);
		Object taggedValue = element.getValue(stereo, "uncertainty");
		if(taggedValue != null && taggedValue instanceof EList && ((EList<?>)taggedValue).size() > 0){
			for(int i = 0; i < ((EList<?>)taggedValue).size(); i++){
				Object obj = ((EList<?>)taggedValue).get(i);
				if(obj instanceof EObject && ((EObject)obj).eClass().getName().equals("Uncertainty")){
					if(list == null) 
						list = new EObject[((EList<?>)taggedValue).size()];
					list[i] = (EObject)obj;
				}else{
					throw new BeliefUtilityException("uncertainty attribute exception!");
				}
			}
		}
		return list;
	}
	
	public static int getSizeOfUncertainties(Element element) throws BeliefUtilityException{
		if(!isApplyBelief(element)) return 0;
		Stereotype stereo = element.getAppliedStereotype(BELIEF);
		Object taggedValue = element.getValue(stereo, "uncertainty");
		if(taggedValue != null && taggedValue instanceof EList){
			return ((EList<?>)taggedValue).size();
		}
		throw new BeliefUtilityException("uncertainty attribute exception!");
	}
	
	public static EObject getUncertaintiesWithCause(Element element, Element referredCause) throws BeliefUtilityException{
		//EObject list = null;
		if(!isApplyBelief(element)) throw new BeliefUtilityException("element does not apply Belief!");
		Stereotype stereo = element.getAppliedStereotype(BELIEF);
		Object taggedValue = element.getValue(stereo, "uncertainty");
		if(taggedValue != null && taggedValue instanceof EList && ((EList<?>)taggedValue).size() > 0){
			for(int i = 0; i < ((EList<?>)taggedValue).size(); i++){
				Object obj = ((EList<?>)taggedValue).get(i);
				if(obj instanceof EObject && ((EObject)obj).eClass().getName().equals("Uncertainty")){
					if(isCausedBy((EObject)obj, referredCause)){
						return (EObject)obj;
					}
				}else{
					throw new BeliefUtilityException("uncertainty attribute exception!");
				}
			}
		}
		return null;
	}
	
	public static EList<?> getIndInput(Element element) throws BeliefUtilityException{
		//EObject list = null;
		if(!isApplyIndSpec(element)) throw new BeliefUtilityException("element does not apply Indspec!");
		Stereotype stereo = element.getAppliedStereotype(INDSPEC);
		Object taggedValue = element.getValue(stereo, "triggeredBy");
		if(taggedValue != null && taggedValue instanceof EList && ((EList<?>)taggedValue).size() > 0){
			return (EList<?>)taggedValue;
		}
		return null;
	}
	
	public static EList<?> getReleaseIndInput(Element element) throws BeliefUtilityException{
		//EObject list = null;
		if(!isApplyIndSpec(element)) throw new BeliefUtilityException("element does not apply Indspec!");
		Stereotype stereo = element.getAppliedStereotype(INDSPEC);
		Object taggedValue = element.getValue(stereo, "releasedBy");
		if(taggedValue != null && taggedValue instanceof EList && ((EList<?>)taggedValue).size() > 0){
			return (EList<?>)taggedValue;
		}
		return null;
	}
	
	public static EObject getUncertaintiesWithCause(Element element) throws BeliefUtilityException{
		if(element instanceof Transition){
			Transition transition = (Transition) element;
			if(isApplyStereotype(transition, CAUSE) && isApplyBelief(transition.getTarget())) 
				return getUncertaintiesWithCause(transition.getTarget(),transition);
		}
		return null;
	}
	
	public static EObject getUncertaintiesOfTransition(Transition element) throws BeliefUtilityException{
		if(isApplyStereotype(element, CAUSE) && isApplyBelief(element.getTarget())) return getUncertaintiesWithCause(element.getTarget(),element);
		return null;
	}
	// parallel is indicated by starting with {, ending with }

	public static List<String> getUncertaintyIdSequence(UTestCase ut) throws BeliefUtilityException{
		if(ut instanceof UTestPath)
			return getUncertaintyIdSequence((UTestPath) ut);
		else return getUncertaintyIdSequence((UTestParallel) ut);
	}
	
	public static List<String> getUncertaintyIdSequence(UTestPath path) throws BeliefUtilityException{
		List<String> list = new ArrayList<String>();
		for(UTestCaseElement ue : path.getSequence()){
			if(ue instanceof UTestAction && ((UTestAction) ue).getAttachedObject() instanceof Transition){
				EObject eUcertObj = getUncertaintiesWithCause((Transition)((UTestAction) ue).getAttachedObject());
				if(eUcertObj != null) list.add(getUncertaintyId((Transition)((UTestAction) ue).getAttachedObject()));
			}if(ue instanceof UTestParallel) list.addAll(getUncertaintyIdSequence((UTestParallel)ue));
		}
		return list;
	}
	
	public static List<String> getUncertaintyIdSequence(UTestParallel parallel) throws BeliefUtilityException{
		List<String> list = new ArrayList<String>();
		for(UTestPath path : parallel.getParalles()){
			list.add("{");
			list.addAll(getUncertaintyIdSequence(path));
			list.add("}");
		}
		return list;
	}
	
	public static List<Object> getUncertaintySequence(UTestCase ut) throws BeliefUtilityException{
		if(ut instanceof UTestPath)
			return getUncertaintySequence((UTestPath) ut);
		else return getUncertaintySequence((UTestParallel) ut);
	}
	
	
	public static List<Object> getUncertaintySequence(UTestPath path) throws BeliefUtilityException{
		List<Object> list = new ArrayList<Object>();
		for(UTestCaseElement ue : path.getSequence()){
			if(ue instanceof UTestAction && ((UTestAction) ue).getAttachedObject() instanceof Transition){
				EObject eUcertObj = getUncertaintiesWithCause((Transition)((UTestAction) ue).getAttachedObject());
				if(eUcertObj != null) list.add(eUcertObj);
			}if(ue instanceof UTestParallel) list.addAll(getUncertaintySequence((UTestParallel)ue));
		}
		return list;
	}
	
	public static List<Object> getUncertaintySequence(UTestParallel parallel) throws BeliefUtilityException{
		List<Object> list = new ArrayList<Object>();
		for(UTestPath path : parallel.getParalles()){
			list.add("{");
			list.addAll(getUncertaintySequence(path));
			list.add("}");
		}
		return list;
	}
	
	
	
	public static Set<EObject> getUncertainties(UTestSet uset)throws BeliefUtilityException{
		Set<EObject> set = new HashSet<EObject>();
		for(UTestCase tc : uset.getUtestcase()){
			set.addAll(getUncertainties(tc));
		}
		return set;
	}
	
	public static Set<EObject> getUncertainties(UTestCase ut)throws BeliefUtilityException{
		if(ut instanceof UTestPath)
			return getUncertainties((UTestPath) ut);
		else return getUncertainties((UTestParallel) ut);
	}
	
	public static Set<EObject> getUncertainties(UTestPath path) throws BeliefUtilityException{
		Set<EObject> uncerSet = new HashSet<EObject>();
		for(UTestCaseElement ue : path.getSequence()){
			if(ue instanceof UTestAction && ((UTestAction) ue).getAttachedObject() instanceof Transition){
				EObject eUcertObj = getUncertaintiesWithCause((Transition)((UTestAction) ue).getAttachedObject());
				if(eUcertObj != null) uncerSet.add(eUcertObj);
			}if(ue instanceof UTestParallel) uncerSet.addAll(getUncertainties((UTestParallel)ue));
		}
		return uncerSet;
	}
	
	public static Set<EObject> getUncertainties(UTestParallel parallel) throws BeliefUtilityException{
		Set<EObject> uncerSet = new HashSet<EObject>();
		for(UTestPath path : parallel.getParalles()){
			uncerSet.addAll(getUncertainties(path));
		}
		return uncerSet;
	}
	

	public static EObject[] getPropertyValue(EObject uncerObj, String propertyName){
		EObject[] result = null;
		Object value = uncerObj.eGet(uncerObj.eClass().getEStructuralFeature(propertyName));
		if(value instanceof EcoreEList && ((EList<?>) value).size() > 0){
			EList<?> mlist = (EList<?>) value;
			result = new EObject[mlist.size()];
			for(int i = 0; i < mlist.size(); i++){
				if(mlist.get(i) instanceof EObject){
					EObject mObj = (EObject) mlist.get(i);
					result[i] = mObj;
				}
			}
		}
		return result;
	}
	
	public static double[] getMeasurementValue(Transition transition) throws BeliefUtilityException{
		if(!isApplyStereotype(transition, CAUSE)) throw new BeliefUtilityException("transition does not apply cause!");
		if(!isApplyBelief(transition.getTarget())) throw new BeliefUtilityException("the target of transition does not apply cause!");
		EObject uncerObj =  getUncertaintiesWithCause(transition.getTarget(), transition);
		if(uncerObj == null){System.out.println(transition+" "+ transition.getTarget()+" "+transition.getSource());}
		return getDoubleMeasurementOfUncertainty(uncerObj);
	}

	
	public static boolean isCausedBy(EObject uncerObj, Element cause) throws BeliefUtilityException{
		if(!isApplyStereotype(cause, CAUSE)) throw new BeliefUtilityException("element does not apply cause!");
		Object value = uncerObj.eGet(uncerObj.eClass().getEStructuralFeature("referredCause"));
		if(value instanceof EcoreEList && ((EList<?>) value).size() > 0){
			EList<?> mlist = (EList<?>) value;
			for(int i = 0; i < mlist.size(); i++){
				if(mlist.get(i) instanceof EObject){
					if(mlist.get(i).equals(cause)) return true;
				}
			}
		}
		return false;
	}
	
	public static EList<?> getIndSpecsOfUncertainty(EObject uncerObj) throws BeliefUtilityException{
		Object value = uncerObj.eGet(uncerObj.eClass().getEStructuralFeature("relatedIndSpecs"));
		if(value instanceof EcoreEList && ((EList<?>) value).size() > 0){
			return (EList<?>) value;
			
		}
		return null;
	}
	
	

	public static double[] getDoubleMeasurementOfUncertainty(EObject uncerObj){
		double[] result;
		Object value = uncerObj.eGet(uncerObj.eClass().getEStructuralFeature("measuredValue"));
		if(value instanceof EcoreEList && ((EList<?>) value).size() > 0){
			EList<?> mlist = (EList<?>) value;
			result = new double[mlist.size()];
			for(int i = 0; i < mlist.size(); i++){
				if(mlist.get(i) instanceof EObject){
					EObject mObj = (EObject) mlist.get(i);
					if(mObj.eClass().getName().equals("Measurement")){
						Object mvalue =mObj.eGet(mObj.eClass().getEStructuralFeature("measurement"));
						if(mvalue == null) System.out.println(uncerObj+" "+uncerObj.eClass().getEStructuralFeature("referredCause"));
						result[i] = Double.parseDouble((String)mvalue);
					}
				}
			}
			return result;
		}
		return null;
	}

	
	public EList<Transition> findEquTranstions(Transition transition, EList<Transition> all){
		EList<Transition> tlist = null;
		for(Transition t: all){
			if(!t.equals(transition)){
				if(isSameConstraint(t.getGuard(), transition.getGuard()) && isSameTrigger(t.getTriggers(), transition.getTriggers())){
					if(tlist == null) tlist = new BasicEList<Transition>();
					tlist.add(t);
				}
			}
		}
		return tlist;
	}
	
	public static boolean isSameTranstion(Transition a, Transition b){
		if(!a.equals(b)){
			if(a.getSource().equals(b.getSource())){
				if(a.getName().equals(b.getName())) return true;
				if(isSameConstraint(a.getGuard(), b.getGuard()) && isSameTrigger(a.getTriggers(), b.getTriggers())){
					return true;
				}
				if(isSameIndOperation(a,b)) return true;
				//FIXME
				if(a.getTriggers().size() > 0 && a.getTriggers().size() == b.getTriggers().size()){
					if(a.getTriggers().get(0).getEvent() instanceof ChangeEvent){
						String aN = a.getName().substring(0, a.getName().lastIndexOf("."));
						String bN = b.getName().substring(0, b.getName().lastIndexOf("."));
						if(aN.equals(bN)) return true;
					}
				}
			}
			
		}
		return false;
	}
	
	public static boolean isSameIndOperation(Transition a, Transition b){
		//System.err.println("debug:"+"isSameIndOperation-0 "+a.getName()+" "+a.getTriggers().get(0)+" "+b.getName()+" "+b.getTriggers().get(0));
		if(a.getTriggers().size() == 1 && a.getTriggers().size() == b.getTriggers().size()){
			if(a.getTriggers().get(0).getEvent() instanceof CallEvent && b.getTriggers().get(0).getEvent() instanceof CallEvent){
				CallEvent ac = (CallEvent) a.getTriggers().get(0).getEvent();
				CallEvent bc = (CallEvent) b.getTriggers().get(0).getEvent();
				boolean af = false;
				boolean bf = false;
				
				if(isApplyIndSInput(ac.getOperation()) && isApplyIndSInput(bc.getOperation())){
					EObject uncerObj;
					try {
						uncerObj = getUncertaintiesWithCause(a);
						Object value = uncerObj.eGet(uncerObj.eClass().getEStructuralFeature("referredIndeterminacySource"));
						
//						Stereotype stereo = element.getAppliedStereotype(BELIEF);
//						Object taggedValue = element.getValue(stereo, "uncertainty");
						
						if(value instanceof EcoreEList && ((EList<?>) value).size() > 0){
							EList<?> mlist = (EList<?>) value;
							for(int i = 0; i < mlist.size(); i++){
								if(mlist.get(i) instanceof EObject){
									EObject mObj = (EObject) mlist.get(i);
									if(mObj instanceof Element){
										Stereotype inds = ((Element)mObj).getAppliedStereotype(INDSOURCE);
										Object taggedValue = ((Element)mObj).getValue(inds, "triggeredBy");
										if(taggedValue != null && taggedValue instanceof EList && ((EList<?>)taggedValue).size() > 0){
											for(int j = 0; j < ((EList<?>)taggedValue).size(); j++){
												Object obj = ((EList<?>)taggedValue).get(j);
												if(obj.equals(ac.getOperation())){
													af = true;
												}
												
												if(obj.equals(bc.getOperation())){
													bf = true;
												}
											}
										}
									}

								}
							}
						}
					} catch (BeliefUtilityException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					//System.err.println(a.getName()+"::"+af +" vs "+b.getName()+"::"+bf);
					return af&&bf;
					

				}
			}
		}
		return false;
	}
	
	public static boolean isSameConstraint(Constraint guard1, Constraint guard2){
		if(guard1 == null && guard2 == null) return true;
		if(guard1 == null || guard2 == null) return false;
		String str1 = ModelUtil.getBodyConstraint(guard1).replaceAll(" ", "");
		String str2 = ModelUtil.getBodyConstraint(guard2).replaceAll(" ", "");
		if(str1!=null && str2 !=null) return str1.equals(str2);
		return false;
	}
	
	public static boolean isSameTrigger(EList<Trigger> t1, EList<Trigger> t2){
		if(t1.size() != t2.size()) return false;
		for(Trigger t : t1){
			if(!existEvent(t.getEvent(), t2)) return false;
		}
		return true;
	}
	
	public static boolean existEvent(Event e, EList<Trigger> t2){
		for(Trigger t : t2){
			if(t.getEvent().equals(e)) return true;
		}
		return false;
	}
	
	public String getStrState(Vertex vertex){
		String str = "";
		if(vertex instanceof State){
			State state = (State)vertex;
			if(state.isSimple()) str = "isSimple:";
			if(state.isComposite()) str = "isComposite:";
			str = str + state.getName();
		}
		return str;
	}
	
	public static String getStrTransition(Transition transition) throws BeliefUtilityException{
		String str=transition.getSource().getName()+"->"+transition.getTarget().getName();
		if(transition.getTriggers().size() == 1){
			Trigger trigger = transition.getTriggers().get(0);	
			Event event = trigger.getEvent();
			if(event instanceof TimeEvent){
				if(((TimeEvent)event).getWhen().getExpr() instanceof LiteralString){
					str = " after:"+ ((LiteralString)((TimeEvent)event).getWhen().getExpr()).getValue(); 
				}
			}else if(event instanceof CallEvent){
				str = " operation:"+((CallEvent)event).getOperation().getName();
			}else if(event instanceof SignalEvent){
				str = " signal:"+((SignalEvent)event).getSignal().toString();
			}else if(event instanceof ChangeEvent){
				if(((ChangeEvent)event).getChangeExpression() instanceof OpaqueExpression)
					str = " change expression:"+  ModelUtil.getBodyOfOperationExpression((OpaqueExpression)((ChangeEvent)event).getChangeExpression());
			}else if(event instanceof AnyReceiveEvent){
				str = "any receive event";
			}else{
				throw new BeliefUtilityException("trigger exception!");
			}
		}
		
		if(transition.getGuard() != null){
			str = str +" guard:"+ModelUtil.getBodyConstraint(transition.getGuard());
		}
		return str;
	}
	
	public static Set<Transition> getAllOwnedTransition(StateMachine sm){
		Set<Transition> set = new HashSet<Transition>();
		for(Element e : sm.allOwnedElements()){
			if(e instanceof Transition) set.add((Transition)e);
			if(e instanceof State){
				if(((State)e).isSubmachineState()){
					set.addAll(getAllOwnedTransition(((State)e).getSubmachine()));
				}
				
			}
		}
		return set;
	}
	
	public static Set<Transition> getAllOwnedTransition(UTestSet uset){
		Set<Transition> set = new HashSet<Transition>();
		for(UTestCase ut : uset.getUtestcase()){
			set.addAll(getAllOwnedTransition(ut));
		}
		return set;
	}
	
	public static double printCoverageTransition(UTestSet uset){
		int total = getAllOwnedTransition((StateMachine)uset.getAttachedObject()).size();
		int included = getAllOwnedTransition(uset).size();
		return (included * 1.0) / total;
	}
	
	public static double printCoverageUncertainty(UTestSet uset) throws BeliefUtilityException {
		int total = getUncertainties((StateMachine) uset.getAttachedObject()).size();
		int included = getUncertainties(uset).size();
		
//		int i = 0;
//		for(EObject u : getUncertainties(uset)){
//			System.out.println((i+1)+": referred cause is "+ getPropertyValue(u, "referredCause")[0]+" measuredValue is "+getDoubleMeasurementOfUncertainty(u)[0]);
//			i++;
//		}
		return (included * 1.0) / total;

	}
	
	public static List<Transition> getListOfTransitions(UTestCase ut){
		if(ut instanceof UTestPath) return getListOfTransitions((UTestPath) ut);
		else return getListOfTransitions((UTestParallel) ut);
	}
	
	public static List<Transition> getListOfTransitions(UTestPath path){
		List<Transition> list = new ArrayList<Transition>();
		for(UTestCaseElement e :path.getSequence()){
			if(e instanceof UTestAction && ((UTestAction) e).getAttachedObject() instanceof Transition) list.add((Transition)((UTestAction) e).getAttachedObject());
			else if(e instanceof UTestParallel) list.addAll(getListOfTransitions((UTestParallel) e));
		}
		return list;
	}
	public static List<Transition> getListOfTransitions(UTestParallel parallel){
		List<Transition> list = new ArrayList<Transition>();
		for(UTestPath path : parallel.getParalles()){
			list.addAll(getListOfTransitions(path));
		}
		return list;
	}
	
	public static List<String> getListOfTransitionsStr(UTestCase ut){
		List<String> trans = new ArrayList<String>();
		List<Transition> tr = getListOfTransitions(ut);
		for(Transition t : tr){
			trans.add(t.getName());
		}
		return trans;
	}
	
	
	public static Set<String> getAllOwnedTransitionStr(UTestCase ut){
		Set<String> trans = new HashSet<String>();
		Set<Transition> tr = getAllOwnedTransition(ut);
		for(Transition t : tr){
			trans.add(t.getName());
		}
		return trans;
	}
	
	public static Set<Transition> getAllOwnedTransition(UTestCase ut){
		if(ut instanceof UTestPath) return getAllOwnedTransition((UTestPath) ut);
		else return getAllOwnedTransition((UTestParallel) ut);
	}
	
	public static Set<Transition> getAllOwnedTransition(UTestPath path){
		Set<Transition> set = new HashSet<Transition>();
		for(UTestCaseElement e :path.getSequence()){
			if(e instanceof UTestAction && ((UTestAction) e).getAttachedObject() instanceof Transition) set.add((Transition)((UTestAction) e).getAttachedObject());
			else if(e instanceof UTestParallel) set.addAll(getAllOwnedTransition((UTestParallel) e));
		}
		return set;
	}
	
	public static Set<Transition> getAllOwnedTransition(UTestParallel parallel){
		Set<Transition> set = new HashSet<Transition>();
		for(UTestPath path : parallel.getParalles()){
			set.addAll(getAllOwnedTransition(path));
		}
		return set;
	}
	
	public static double getPrentageOfTransition(Set<Transition> all, Set<Transition> inclued){
		Set<Transition> removed = new HashSet<Transition>();
		for(Transition inclued_t : inclued){
			if(all.contains(inclued_t)) removed.add(inclued_t);
			//FIXME Man Zhang
			else{
				for(Transition all_t : all){
					if(isSameTranstion(all_t, inclued_t)){removed.add(all_t);}
				}
			}
			
		}
		double total_all = all.size();
		double removed_t = removed.size();
		return removed_t/total_all;
	}
	public static void printSMDetails(StateMachine s) throws BeliefUtilityException{
		// 0 - number of vertex, 1- number of composite state, 2- number of exist, 3- number of terminal, 
		// 4 - number of transition, 5- number of Belief, 6- number of Uncertainty, 7 - number of cause
		int[] details = {0,0,0,0,0, 0, 0, 0,0};

		for(Element e : s.allOwnedElements()){
			if(e instanceof Vertex){
				if(BeliefUtility.isApplyBelief(e)){
					details[5]++;
					details[6] = details[6] + BeliefUtility.getSizeOfUncertainties(e);
				}
				details[0]++;
				if(e instanceof State){
					State s1 = (State)e;
					if(s1.isComposite()){
						details[1]++;
					}
					if(s1.isSubmachineState()){
						System.out.println("--------submachine-----------");
						printSMDetails(s1.getSubmachine());
						details[8]++;
					}
				}else if(e instanceof Pseudostate){
					if(((Pseudostate) e).getKind().getValue() == PseudostateKind.EXIT_POINT) details[2]++;
					if(((Pseudostate) e).getKind().getValue() == PseudostateKind.TERMINATE) details[3]++;
				} 
			}
			if(e instanceof Transition){
				details[4]++;
				if(BeliefUtility.isApplyStereotype(e, BeliefUtility.CAUSE)){
					details[7]++;
				}
			}
		}
		System.out.println("============="+s.getName()+"=================");
		System.out.println(String.format("The number of vertexes: %1$d [ composite: %2$d exit: %3$d terminal: %4$d submachine: %6$d], The number of transitions: %5$d",
				details[0],details[1], details[2], details[3], details[4], details[8]));
		System.out.println(String.format("The number of Belief: %1$d, The number of Uncertainty: %2$d, The number of Cause: %3$d",
				details[5],details[6], details[7]));
	}
	
	
	
	public static String[] getSMDetails(StateMachine s) throws BeliefUtilityException{
		// 0 - number of vertex, 1- number of composite state, 2- number of exist, 3- number of terminal, 
		// 4 - number of transition, 5- number of Belief, 6- number of Uncertainty, 7 - number of cause
		String[] detailStr = new String[3];
		int[] details = {0,0,0,0,0, 0, 0, 0};

		for(Element e : s.allOwnedElements()){
			if(e instanceof Vertex){
				if(BeliefUtility.isApplyBelief(e)){
					details[5]++;
					details[6] = details[6] + BeliefUtility.getSizeOfUncertainties(e);
				}
				details[0]++;
				if(e instanceof State){
					State s1 = (State)e;
					if(s1.isComposite()){
						details[1]++;
					}
				}else if(e instanceof Pseudostate){
					if(((Pseudostate) e).getKind().getValue() == PseudostateKind.EXIT_POINT) details[2]++;
					if(((Pseudostate) e).getKind().getValue() == PseudostateKind.TERMINATE) details[3]++;
				} 
			}
			if(e instanceof Transition){
				details[4]++;
				if(BeliefUtility.isApplyStereotype(e, BeliefUtility.CAUSE)){
					details[7]++;
				}
			}
		}
		detailStr[0] = "============="+s.getName()+"=================";
		detailStr[1] = String.format("The number of vertexes: %1$d [ composite: %2$d exit: %3$d terminal: %4$d], The number of transitions: %5$d",
				details[0],details[1], details[2], details[3], details[4]);
		detailStr[2] = String.format("The number of Belief: %1$d, The number of Uncertainty: %2$d, The number of Cause: %3$d",
				details[5],details[6], details[7]);
		return detailStr;
	}
	
	public static StateMachine getSMByName(org.eclipse.uml2.uml.Package model, String name){
		StateMachine sm = (StateMachine) UMLUtil.findEObject(model.allOwnedElements(), new EObjectMatcher(){
			@Override
			public boolean matches(EObject eObject) {
				return (eObject instanceof StateMachine) && ((StateMachine)eObject).getName().equals(name);
			}});
		return sm;
	}
	
	// the name of uncertainty is <state, transition, state>
	public static String getUncertaintyId(Transition transition){
		if(isApplyCause(transition) && isApplyBelief(transition.getTarget())){
			return String.format("<%1s, %2s, %3s>", transition.getSource().getName(), transition.getName(), transition.getTarget().getName());
		}
		return null;
	}
}
