/*******************************************************************************
 * Copyright (c) 2016, 2017 Simula Research Laboratory. 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Simula license 
 * which accompanies this distribution, and is available at LICENSE.TXT
 *  
 *  Contributors:
 *        Man Zhang
 *******************************************************************************/
package no.simula.se.testing.uncertainty_theory;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.State;
import org.eclipse.uml2.uml.StateMachine;
import org.eclipse.uml2.uml.Transition;
import org.eclipse.uml2.uml.Vertex;

import no.simula.se.testing.utility.belief.BeliefUtility;
import no.simula.se.testing.utility.belief.BeliefUtilityException;
import no.simula.se.testmodel.TestModel.UMSpaceA;
import no.simula.se.testmodel.TestModel.UTestModelFactory;
import no.simula.se.testmodel.TestModel.UTestSet;

public class UncertaintyTheoryUtil {
	
	private static UncertaintyTheoryUtil instance;
	public static UncertaintyTheoryUtil getInstance(){
		if(instance == null) instance = new UncertaintyTheoryUtil();
		return instance;
	}
	
	public static void createUMSpaceA(UTestSet set) throws BeliefUtilityException{
		
//		for(Element element : ((StateMachine)set.getAttachedObject()).allOwnedElements()){
//			if(element instanceof Vertex){
//				if(createUMSpaceA((Vertex)element)!=null) set.getUmspacea().addAll(createUMSpaceA((Vertex)element));
//			}
//		}
		createUMSpaceA(set, (StateMachine)set.getAttachedObject());
	}
	
	public static void createUMSpaceA(UTestSet set, StateMachine sm) throws BeliefUtilityException{
		for(Element element : sm.allOwnedElements()){
			if(element instanceof Vertex){
				if(createUMSpaceA((Vertex)element)!=null) set.getUmspacea().addAll(createUMSpaceA((Vertex)element));
				if(element instanceof State && ((State)element).isSubmachineState()){
					createUMSpaceA(set, ((State)element).getSubmachine());
				}
			}
		}
		
	}
	
	public static EList<UMSpaceA> createUMSpaceA(Vertex v) throws BeliefUtilityException{
		EList<UMSpaceA> list = null;
		for(Transition t: v.getOutgoings()){
			if(BeliefUtility.isApplyStereotype(t, BeliefUtility.CAUSE)){
				if(list == null) list = new BasicEList<UMSpaceA>();
				if(!doesIncluded(list, t)){
					UMSpaceA umsp = findIncludedEqu(list,t);
					if(umsp == null){
						umsp = UTestModelFactory.eINSTANCE.createUMSpaceA();
						list.add(umsp);
						umsp.setSource(v);
					}
					umsp.getTransitions().add(t);
					umsp.getUms().add(BeliefUtility.getUncertaintiesOfTransition(t));
				}
			}
		}
		return list;
	}
	
	public static boolean doesIncluded(EList<UMSpaceA> list, Transition t){
		for(UMSpaceA umsp : list){
			if(umsp.getTransitions().contains(t)) return true;
		}
		return false;
	}
	
	public static UMSpaceA findIncludedEqu(EList<UMSpaceA> list, Transition t){
		for(UMSpaceA umsp : list){
			for(EObject eObj : umsp.getTransitions()){
				Transition included_t = (Transition) eObj;
				if(BeliefUtility.isSameTranstion(included_t, t)) return umsp;
			}
		}
		return null;
	}
}
