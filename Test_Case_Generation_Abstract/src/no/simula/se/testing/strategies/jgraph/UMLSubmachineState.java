/*******************************************************************************
 * Copyright (c) 2016, 2017 Simula Research Laboratory. 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Simula license 
 * which accompanies this distribution, and is available at LICENSE.TXT
 *  
 *  Contributors:
 *        Man Zhang
 *******************************************************************************/
package no.simula.se.testing.strategies.jgraph;

import org.eclipse.uml2.uml.State;

public class UMLSubmachineState extends UMLCompositeState{
private static String simple = "Submachine";
	
	private SMGraph graph;
	
	public UMLSubmachineState(State state, int algo, int combination) throws GraphsException{
		super(state);
		graph = new SMGraph(state.getSubmachine().getRegions(), algo, combination);
		processInsNameInsideElement();
	}
	public UMLSubmachineState(State state, int algo, int combination, boolean isloop, int minus) throws GraphsException{
		super(state);
		graph = new SMGraph(state.getSubmachine().getRegions(), algo, combination, isloop, minus);
		processInsNameInsideElement();
	}
	
	public SMGraph getGraph() {
		return graph;
	}

	public void setGraph(SMGraph graph) {
		this.graph = graph;
	}
	
	public String toString(){
		return super.toString(simple);
	}
	
	public String getInstanceName(){
		return this.getVertex().getName();
	}
	
	public void processInsNameInsideElement(){
		for(UMLRegionDirectedGraph<UMLVertex, UMLTransitionWeightEdge> in_graph:graph.getParallels()){
			for(UMLTransitionWeightEdge t : in_graph.edgeSet()){
				t.setInstanceName(getInstanceName());
			}
			
			for(UMLVertex v : in_graph.vertexSet()){
				v.setInstanceName(getInstanceName());
			}
		}
	}
}
