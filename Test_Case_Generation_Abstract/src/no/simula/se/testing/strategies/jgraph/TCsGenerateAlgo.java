/*******************************************************************************
 * Copyright (c) 2016, 2017 Simula Research Laboratory. 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Simula license 
 * which accompanies this distribution, and is available at LICENSE.TXT
 *  
 *  Contributors:
 *        Man Zhang
 *******************************************************************************/
package no.simula.se.testing.strategies.jgraph;

public final class TCsGenerateAlgo {
	
	public final static int ALL_DIRECTED_PATHS = 0;
	public final static int A_STAR_SHORTEST_PATH = 1;
	public final static int BELLMAN_FORD_SHORTEST_PATH = 2;
	public final static int BIDIRECTIONAL_DIJKSTRA_SHORTEST_PATH = 3; 
	
	
}
