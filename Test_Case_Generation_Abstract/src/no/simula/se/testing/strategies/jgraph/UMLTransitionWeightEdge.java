/*******************************************************************************
 * Copyright (c) 2016, 2017 Simula Research Laboratory. 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Simula license 
 * which accompanies this distribution, and is available at LICENSE.TXT
 *  
 *  Contributors:
 *        Man Zhang
 *******************************************************************************/
package no.simula.se.testing.strategies.jgraph;

import org.eclipse.uml2.uml.Transition;
import org.jgrapht.graph.DefaultWeightedEdge;

import no.simula.se.testing.utility.belief.BeliefUtility;
import no.simula.se.testing.utility.belief.BeliefUtilityException;
import no.simula.se.testmodel.TestModel.UTestAction;
import no.simula.se.testmodel.TestModel.UTestModelFactory;

public class UMLTransitionWeightEdge extends DefaultWeightedEdge {

	private static final long serialVersionUID = -4897188675261436601L;
	private static String simple = "T";
	private Transition attachedTransition;
	private String instanceName;
	public UMLTransitionWeightEdge(){}
	
	public UMLTransitionWeightEdge(Transition t){
		this.attachedTransition = t;
	}
	
	
	public Transition getAttachedTransition() {
		return attachedTransition;
	}
	public void setAttachedTransition(Transition attachedTransition) {
		this.attachedTransition = attachedTransition;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	public double getWeight(){
		BeliefUtility.getInstance();
		if(BeliefUtility.isApplyStereotype(this.getAttachedTransition(), BeliefUtility.CAUSE)){
			double[] ds;
			try {
				ds = BeliefUtility.getMeasurementValue(this.getAttachedTransition());
				if(ds == null) System.out.println(this.getAttachedTransition());
				if(ds.length == 1) return ds[0];
				else{
					throw new BeliefUtilityException("the size of measured value is more than 1");
				}
			} catch (BeliefUtilityException e) {
				e.printStackTrace();
			}
			
		}
		return 1;
		
	}
	
	public UMLVertex getSource(){
		return (UMLVertex)super.getSource();
	}
	
	public UMLVertex getTarget(){
		return (UMLVertex)super.getTarget();
	}
	
	public String toString(){
		return this.toString(simple);
	}
	
	public String toString(String str){
		if(this.getAttachedTransition().getName() == null)
			return str;
		return str+"$"+this.getAttachedTransition().getName();
	}
	
	public UTestAction convertToUTestCaseElement(){
		UTestAction action = UTestModelFactory.eINSTANCE.createUTestAction();
		action.setAttachedObject(this.getAttachedTransition());
		action.setMeasuredValue(getWeight());
		if(this.getInstanceName() != null && this.getInstanceName() != ""){
			action.setInstanceoName(this.getInstanceName());
		}
		return action;
	}

	public static String getSimple() {
		return simple;
	}

	public static void setSimple(String simple) {
		UMLTransitionWeightEdge.simple = simple;
	}

	public String getInstanceName() {
		return instanceName;
	}

	public void setInstanceName(String instanceName) {
		this.instanceName = instanceName;
	}
	
	
}
