/*******************************************************************************
 * Copyright (c) 2016, 2017 Simula Research Laboratory. 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Simula license 
 * which accompanies this distribution, and is available at LICENSE.TXT
 *  
 *  Contributors:
 *        Man Zhang
 *******************************************************************************/
package no.simula.se.testing.strategies.jgraph;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.uml2.uml.PseudostateKind;
import org.eclipse.uml2.uml.Region;
import org.jgrapht.GraphPath;
import org.jgrapht.alg.AllDirectedPaths;
import org.jgrapht.graph.DirectedWeightedPseudograph;

public class UMLRegionDirectedGraph<V, E> extends DirectedWeightedPseudograph<V, E> {

	private static final long serialVersionUID = 2425418612688118844L;
 	private final static int max = 1000;
 	private boolean isloop;
 	private int maxPathLength;
 	
	private Region region;
	private Set<V> enters;
	private Set<V> exits;
 	private List<GraphPath<V, E>> paths;
 	//private Map<GraphPath<V, E>,List<int[]>> deepPathMap;
 	private List<UMLPath<V, E>> deepPaths;
 	
	public UMLRegionDirectedGraph(Class<? extends E> edgeClass) {
		super(edgeClass);
	}
	
	public UMLRegionDirectedGraph(Class<? extends E> edgeClass, Region r) {
		super(edgeClass);
		this.region = r;
		enters = new HashSet<V>();
		exits = new HashSet<V>();
		paths = new ArrayList<GraphPath<V, E>>();
		//deepPathMap = new HashMap<GraphPath<V,E>, List<int[]>>();
		deepPaths = new ArrayList<UMLPath<V, E>>();
		this.setIsloop(true);
	}
	
	public UMLRegionDirectedGraph(Class<? extends E> edgeClass, Region r, boolean isloop) {
		super(edgeClass);
		this.region = r;
		enters = new HashSet<V>();
		exits = new HashSet<V>();
		paths = new ArrayList<GraphPath<V, E>>();
		//deepPathMap = new HashMap<GraphPath<V,E>, List<int[]>>();
		deepPaths = new ArrayList<UMLPath<V, E>>();
		this.setIsloop(isloop);
	}
	
	public UMLRegionDirectedGraph(Class<? extends E> edgeClass, Region r, boolean isloop, int maxPathLength) throws GraphsException {
		super(edgeClass);
		this.region = r;
		enters = new HashSet<V>();
		exits = new HashSet<V>();
		paths = new ArrayList<GraphPath<V, E>>();
		//deepPathMap = new HashMap<GraphPath<V,E>, List<int[]>>();
		deepPaths = new ArrayList<UMLPath<V, E>>();
		this.setIsloop(isloop);
		if(!isloop && maxPathLength > max){
			throw new GraphsException("invali max length of path.");
		}
		this.setMaxPathLength(maxPathLength);
	}
	
	
	// entrance to generate path
	public void generatePaths(int algo) throws GraphsException{
		switch(algo){
			case TCsGenerateAlgo.ALL_DIRECTED_PATHS: 
				paths.addAll(generateAllDirectedPaths());
				break;
			default:
				throw new GraphsException("UMLRegionDirectedGraph->generatePaths()->further algo");
		}
		
	}
	
	public int getLengthOfPath(){
		return deepPaths.size();
	}
	
	//entrance to generate deep path
	public void generatePathMap() throws GraphsException{
		//System.out.println("---generate deep paths in terms of composite state---");
		long time = System.currentTimeMillis();
		for(GraphPath<V, E> path: this.getPaths()){
			generatePathMapItem(path);
		}
//		SMToGraphsUtil.getInstance();
//		SMToGraphsUtil.reportPerformanceFor("result:"+deepPaths.size(), time);
	}
	
	public void generatePathMapItem(GraphPath<V, E> path) throws GraphsException{
		List<UMLCompositeState> composite = null;
		for(V v: path.getVertexList()){
			if(v instanceof UMLCompositeState){
				if(composite == null) composite = new ArrayList<UMLCompositeState>();
				composite.add((UMLCompositeState)v);
			}
		}
		if(composite == null){
			deepPaths.add(new UMLPath(path, null));
			return;
		}
			
		
		List<int[]> list = new ArrayList<int[]>();
		//deepPathMap.put(path, list);
		
		// set the size for each of composite vertex
		int[] distribution = new int[composite.size()];
		for(int i = 0; i < composite.size(); i++){
			distribution[i] = composite.get(i).getGraph().getPaths().size()-1;
		}
		
		boolean flag = true;
		int[] tmps = new int[distribution.length];
		for(int i = 0; i < distribution.length; i++){
			tmps[i] = distribution[i];
		}
		while(flag){
			flag = false;
			int[] items = new int[distribution.length];
			for(int index = 0; index < tmps.length; index++){
				int connected = connectedStatus(path, composite.get(index), tmps[index]);
				if(connected == -1){
					items[index] = connected * tmps[index] - 1;
					if(doesAdd(list, items)){
						list.add(items);
						UMLPath new_ = new UMLPath(path, items, true);
						if(doesAddDeepPath(new_)){
							deepPaths.add(new_);
						}
						
					} 
					break;
				}
				else if(connected == -2) break;
				else{
					items[index] = tmps[index];
					if(index == tmps.length-1){
						list.add(items);
						deepPaths.add(new UMLPath(path, items));
					}
				}
			}
			// distribution -1
			for(int i = tmps.length-1; i >= 0; i--){
				int value = tmps[i];
				if(value -1 >= 0){
					tmps[i] = value -1;
					for(int j = i+1; j<tmps.length; j++){
						tmps[j] = distribution[j];
					}
					flag = true;
					break;
				}
			}
		}
	}
	
	public boolean doesAddDeepPath(UMLPath path){
		//System.out.println("path:"+path.toString());
		for(UMLPath p : this.getDeepPaths()){
			if(p.isEqual(path))return false;
//			if(p.toString().equals(path.toString())){
//				
//			}
		}
		return true;
	}
	
	public boolean doesAdd(List<int[]> list, int[] item) throws GraphsException{
		int index = getIndextTerminal(item);
		if(index == -1) {
			System.out.println(item.toString());
			throw new GraphsException("there is no index refer to termial");
		}
		for(int[] l : list){
//			if(getIndextTerminal(l) != -1 && getIndextTerminal(l)<=index)
//				return false;
			if(l[index] ==item[index])
				return false;
		}
		return true;
	}
	
	public int getIndextTerminal(int[] item){
		for(int i = 0; i < item.length; i++){
			if(item[i] < 0) return i;
		}
		return -1;
	}
	public int connectedStatus(GraphPath<V, E> path, UMLCompositeState v, int i) throws GraphsException{
		// terminal -1
		// not connected -2
		// connected 1
		
		UMLParallelPath<V, E> pa = v.getGraph().getPaths().get(i);
		if(pa.getParallels().size()>1){
			for(UMLPath<V, E> p : pa.getParallels()){ if(!(p.getPath().getEndVertex() instanceof UMLFinalState)) 
				throw new GraphsException("one of region does not end by final state.");}
			return 1;
		}
		UMLPath<V, E> p = pa.getParallels().get(0);
		Object final_ = p.getPath().getEndVertex();
		if(final_ instanceof UMLFinalState) return 1;
		if(final_ instanceof UMLPseudostate){
			UMLPseudostate pv = (UMLPseudostate) final_;
			switch(pv.getVertex().getKind().getValue()){
			case PseudostateKind.TERMINATE:
				return -1;
			case PseudostateKind.EXIT_POINT:
				UMLTransitionWeightEdge result = getNextEdge(path, v);
				//System.out.println(result.getAttachedTransition().getSource());
				if(result.getAttachedTransition().getSource().equals(pv.getVertex()))
					return 1;
				return -2;
			}
		}
		throw new GraphsException("error to check the extended point.");
	}
	
	public UMLTransitionWeightEdge getNextEdge(GraphPath<V, E> path, UMLVertex v) throws GraphsException{
		for(E e: path.getEdgeList()){
			if(e instanceof UMLTransitionWeightEdge){
				UMLTransitionWeightEdge te = (UMLTransitionWeightEdge)e;
				if(te.getSource().equals(v)) return te;
			}
		}
		throw new GraphsException("can not find next edge!");
	}
	
	public void generateEnterExit(){
		if(!enters.isEmpty()) enters.clear();
		if(!exits.isEmpty()) exits.clear();
		
		for(V v : this.vertexSet()){
			if(v instanceof UMLPseudostate){
				UMLPseudostate pv = (UMLPseudostate)v;
				switch(pv.getVertex().getKind().getValue()){
				case PseudostateKind.INITIAL:
					enters.add(v);
					break;
				case PseudostateKind.ENTRY_POINT:
					enters.add(v);
					break;
				case PseudostateKind.TERMINATE:
					exits.add(v);
					break;
				case PseudostateKind.EXIT_POINT:
					exits.add(v);
					break;
				}
			}
			if(v instanceof UMLFinalState)
				exits.add(v);
		}
	}
	// listObject
//	public List<Object> getIndexOfDeepPath(int i){
//		List<Object> list = new ArrayList<Object>();
//		UMLPath path = this.getDeepPaths().get(i);
//		if(path.getMaps() == null){}
//		return list;
//	}
	
	// all directed paths
	public List<GraphPath<V, E>> generateAllDirectedPaths(){
		return generateAllDirectedPaths(this.isloop, this.getMaxPathLength());
	}
	
	public List<GraphPath<V, E>> generateAllDirectedPaths(boolean isloop, int maxPathLength){
//		System.out.println("---generate all direction path---");
//		long time = System.currentTimeMillis();
		List<GraphPath<V, E>> result = null;
		if(isloop)		
			result = new AllDirectedPaths<V, E>(this).getAllPaths(this.getEnters(), this.getExits(), isloop, null);
		else{
			//System.out.println("max"+maxPathLength);
			//System.out.println(this.getExits().size()+" "+this.getEnters().size()+" "+maxPathLength);
			result = new AllDirectedPaths<V, E>(this).getAllPaths(this.getEnters(), this.getExits(), isloop,maxPathLength);
			
		}
			
		
		return result;
	}
	//------all directed paths
	
	
	/*---------------setter and getter-------------------*/
	public Region getRegion() {
		return region;
	}

	public void setRegion(Region region) {
		this.region = region;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public Set<V> getEnters() {
		return enters;
	}

	public void setEnters(Set<V> enters) {
		this.enters = enters;
	}

	public Set<V> getExits() {
		return exits;
	}

	public void setExits(Set<V> exits) {
		this.exits = exits;
	}

	public List<GraphPath<V, E>> getPaths() {
		return paths;
	}

	public void setPaths(List<GraphPath<V, E>> paths) {
		this.paths = paths;
	}
	public List<UMLPath<V, E>> getDeepPaths() {
		return deepPaths;
	}

	public void setDeepPaths(List<UMLPath<V, E>> deepPaths) {
		this.deepPaths = deepPaths;
	}

	public boolean isIsloop() {
		return isloop;
	}

	public void setIsloop(boolean isloop) {
		this.isloop = isloop;
	}

	public static int getMax() {
		return max;
	}

	public int getMaxPathLength() {
		return maxPathLength;
	}

	public void setMaxPathLength(int maxPathLength) {
		this.maxPathLength = maxPathLength;
	}
	

//	//FIXME
//	public List<Object> getIndexOfPath(int i) throws GraphsException{
//		if(i<0) throw new GraphsException("request is out of range "+i);
//		List<Object> list = new ArrayList<Object>();
//		int count = 0;
//		for(GraphPath p : paths){
//			if(deepPathMap.get(p) != null){
//				if(count + deepPathMap.get(p).size() - 1 >=i){
//					int[] loc = deepPathMap.get(p).get(i-count);
//					list.add(p.getStartVertex());
//					int j = 0;
//					for(Object eo : p.getEdgeList()){
//						if(eo instanceof UMLTransitionWeightEdge){
//							list.add(eo);
//							UMLVertex v = ((UMLTransitionWeightEdge) eo).getTarget();
//							if(v instanceof UMLCompositeState){
//								
//								j++;
//							}else{
//								list.add(((UMLTransitionWeightEdge) eo).getTarget());
//							}
//						}
//					}
//					if(j != loc.length) throw new GraphsException("not consistent between map with path");
//					return list;
//				}
//			}
//			else{
//				if(count == i){
//					list.add(p.getStartVertex());
//					for(Object eo : p.getEdgeList()){
//						if(eo instanceof UMLTransitionWeightEdge){
//							list.add(eo);
//							list.add(((UMLTransitionWeightEdge) eo).getTarget());
//						}
//					}
//					return list;
//				}
//				count++;
//			}
//				
//		}
//	}
	
}
