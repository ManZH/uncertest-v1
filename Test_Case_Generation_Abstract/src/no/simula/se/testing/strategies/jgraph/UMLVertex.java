/*******************************************************************************
 * Copyright (c) 2016, 2017 Simula Research Laboratory. 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Simula license 
 * which accompanies this distribution, and is available at LICENSE.TXT
 *  
 *  Contributors:
 *        Man Zhang
 *******************************************************************************/
package no.simula.se.testing.strategies.jgraph;

import org.eclipse.uml2.uml.Vertex;

import no.simula.se.testmodel.TestModel.UTestAction;
import no.simula.se.testmodel.TestModel.UTestModelFactory;

public class UMLVertex{
	
	private Vertex vertex;
	private static String simple = "Vertex";
	
	private String instanceName;
	
	public UMLVertex(){}
	public UMLVertex(Vertex v){
		this.vertex = v;
	}
	public Vertex getVertex() {
		return vertex;
	}
	public void setVertex(Vertex vertex) {
		this.vertex = vertex;
	}
	
	
	public static String getSimple() {
		return simple;
	}
	public static void setSimple(String simple) {
		UMLVertex.simple = simple;
	}
	public String toString(){
		return toString(simple);
	}
	
	public String toString(String str){
		if(this.getVertex().getName() == null)
			return str;
		return str+"$"+this.getVertex().getName();
	}
	
	public UTestAction convertToUTestCaseElement(){
		UTestAction action = UTestModelFactory.eINSTANCE.createUTestAction();
		action.setAttachedObject(getVertex());
		if(this.getInstanceName() != null && this.getInstanceName() != ""){
			action.setInstanceoName(this.getInstanceName());
		}
		return action;
	}
	public String getInstanceName() {
		return instanceName;
	}
	public void setInstanceName(String instanceName) {
		this.instanceName = instanceName;
	}
	
	
}
