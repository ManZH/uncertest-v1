/*******************************************************************************
 * Copyright (c) 2016, 2017 Simula Research Laboratory. 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Simula license 
 * which accompanies this distribution, and is available at LICENSE.TXT
 *  
 *  Contributors:
 *        Man Zhang
 *******************************************************************************/
package no.simula.se.testing.strategies.jgraph;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.jgrapht.GraphPath;

import no.simula.se.testmodel.TestModel.UTestAction;
import no.simula.se.testmodel.TestModel.UTestCaseElement;
import no.simula.se.testmodel.TestModel.UTestModelFactory;
import no.simula.se.testmodel.TestModel.UTestPath;

public class UMLPath<V, E> {
	
	private GraphPath<V, E> path;
	private int[] maps;
	private boolean isTerminal;
	private int length;
	private int[] uncertainties;//0- sizes of uncertainty, uncertainty path....
	private String InstanceName;
	
	public UMLPath(GraphPath<V, E> path, int[] maps){
		this.path = path;
		this.maps = maps;
		this.setTerminal(false);
	}
	
	public UMLPath(GraphPath<V, E> path, int[] maps, boolean isTerminal){
		this.path = path;
		this.maps = maps;
		this.setTerminal(isTerminal());
	}
	
	public UTestAction createUTestAction(UTestPath p){
		UTestAction action = UTestModelFactory.eINSTANCE.createUTestAction();
		p.getSequence().add(action);
		return action;
	}
	public List<Object> listGraphPath(){
		List<Object> list = new ArrayList<Object>();
		list.add(path.getStartVertex());
		for(Object eo : path.getEdgeList()){
			if(eo instanceof UMLTransitionWeightEdge){
				list.add(eo);
				list.add(((UMLTransitionWeightEdge) eo).getTarget());
			}
		}
		return list;
	}
	
	// test path includes state and transitions
	public EList<UTestCaseElement> convertToUTestCaseElements(){
		//UTestPath path = UTestModelFactory.eINSTANCE.createUTestPath();
		EList<UTestCaseElement> elist = new BasicEList<UTestCaseElement>();
		int j = 0;
		elist.add(((UMLVertex)path.getStartVertex()).convertToUTestCaseElement());
		for(Object eo : path.getEdgeList()){
			if(eo instanceof UMLTransitionWeightEdge){
				elist.add(((UMLTransitionWeightEdge) eo).convertToUTestCaseElement());
				UMLVertex v = ((UMLTransitionWeightEdge) eo).getTarget();
				if(v instanceof UMLCompositeState){
					UMLParallelPath inner = null;
					if(maps[j] < 0){
						inner = ((UMLCompositeState) v).getGraph().getPaths().get((maps[j]+1)*(-1));
						if(inner.isParallel()) elist.add(inner.convertToUTestParallel());
						else elist.addAll(inner.convertToUTestCaseElements());
						break;
					}else{
						inner = ((UMLCompositeState) v).getGraph().getPaths().get(maps[j]);
						if(inner.isParallel()) elist.add(inner.convertToUTestParallel());
						else elist.addAll(inner.convertToUTestCaseElements());
					}
					j++;
				}else{
					elist.add(v.convertToUTestCaseElement());
				}
			}
		}
		return elist;
	}
	
	public List<Object> listUMLPath(){
		if(maps == null) return listGraphPath();
		int j = 0;
		List<Object> list = new ArrayList<Object>();
		list.add(path.getStartVertex());
		for(Object eo : path.getEdgeList()){
			if(eo instanceof UMLTransitionWeightEdge){
				list.add(eo);
				UMLVertex v = ((UMLTransitionWeightEdge) eo).getTarget();
				if(v instanceof UMLCompositeState){
					UMLParallelPath inner = null;
					if(maps[j] < 0){
						inner = ((UMLCompositeState) v).getGraph().getPaths().get((maps[j]+1)*(-1));
						list.add(inner.listUMLParallelPath());
						break;
					}else{
						inner = ((UMLCompositeState) v).getGraph().getPaths().get(maps[j]);
						list.add(inner.listUMLParallelPath());
					}
					j++;
				}else{
					list.add(v);
				}
			}
		}
		return list;
	}
	
	public List<Object> listUMLPath2(){
		if(maps == null) return listGraphPath();
		int j = 0;
		List<Object> list = new ArrayList<Object>();
		list.add(path.getStartVertex());
		for(Object eo : path.getEdgeList()){
			if(eo instanceof UMLTransitionWeightEdge){
				list.add(eo);
				UMLVertex v = ((UMLTransitionWeightEdge) eo).getTarget();
				if(v instanceof UMLCompositeState){
					UMLParallelPath inner = null;
					if(maps[j] < 0){
						inner = ((UMLCompositeState) v).getGraph().getPaths().get((maps[j]+1)*(-1));
						list.add(inner);
						break;
					}else{
						inner = ((UMLCompositeState) v).getGraph().getPaths().get(maps[j]);
						list.add(inner.listUMLParallelPath());
					}
					j++;
				}else{
					list.add(v);
				}
			}
		}
		return list;
	}
	
	public String toString(){
		String str = "[";
		str = str + path.getStartVertex().toString()+" ";
		int j = 0;
		for(Object o : path.getEdgeList()){
			if(o instanceof UMLTransitionWeightEdge){
				str = str + o.toString()+" ";
				UMLVertex v = ((UMLTransitionWeightEdge) o).getTarget();
				if(v instanceof UMLCompositeState){
					UMLParallelPath inner = null;
					if(maps[j] < 0){
						inner = ((UMLCompositeState) v).getGraph().getPaths().get((maps[j]+1)*(-1));
						str = str + inner.toString();
						break;
					}			
					else{
						inner = ((UMLCompositeState) v).getGraph().getPaths().get(maps[j]);
						str = str + inner.toString();
					}
					j++;
				}else{
					str = str + v.toString()+" ";
				}
			}
		}
		return str = str + "]";
	}
	
	public boolean isEqual(UMLPath path){
		return this.listUMLPath2().containsAll(path.listUMLPath2());
		//return false;
	}
	
	public void setLength(){
		//this.setLength(0);
		if(maps == null){
			this.setLength(this.getPath().getLength());
			return;
		}
		int l = this.getPath().getLength();
		for(Object eo : listUMLPath2()){
			if(eo instanceof UMLParallelPath){
				if(((UMLParallelPath) eo).getLength() == 0){ ((UMLParallelPath) eo).setLength();}
				l = l + ((UMLParallelPath) eo).getLength()-1;
			}
		}
		this.setLength(length);
	}
	
	/**/
	public GraphPath<V, E> getPath() {
		return path;
	}

	public void setPath(GraphPath<V, E> path) {
		this.path = path;
	}

	public int[] getMaps() {
		return maps;
	}

	public void setMaps(int[] maps) {
		this.maps = maps;
	}

	public boolean isTerminal() {
		return isTerminal;
	}

	public void setTerminal(boolean isTerminal) {
		this.isTerminal = isTerminal;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public int[] getUncertainties() {
		return uncertainties;
	}

	public void setUncertainties(int[] uncertainties) {
		this.uncertainties = uncertainties;
	}

	public String getInstanceName() {
		return InstanceName;
	}

	public void setInstanceName(String instanceName) {
		InstanceName = instanceName;
	}
}
