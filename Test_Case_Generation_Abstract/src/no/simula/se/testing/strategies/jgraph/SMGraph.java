/*******************************************************************************
 * Copyright (c) 2016, 2017 Simula Research Laboratory. 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Simula license 
 * which accompanies this distribution, and is available at LICENSE.TXT
 *  
 *  Contributors:
 *        Man Zhang
 *******************************************************************************/
package no.simula.se.testing.strategies.jgraph;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.eclipse.uml2.uml.Pseudostate;
import org.eclipse.uml2.uml.PseudostateKind;
import org.eclipse.uml2.uml.Region;
import org.eclipse.uml2.uml.State;
import org.eclipse.uml2.uml.StateMachine;
import org.eclipse.uml2.uml.Transition;
import org.eclipse.uml2.uml.Vertex;

import no.simula.se.testing.utility.belief.BeliefUtilityException;
import no.simula.se.testmodel.TestModel.UTestAction;
import no.simula.se.testmodel.TestModel.UTestCaseElement;
import no.simula.se.testmodel.TestModel.UTestItem;
import no.simula.se.testmodel.TestModel.UTestModelFactory;
import no.simula.se.testmodel.TestModel.UTestParallel;
import no.simula.se.testmodel.TestModel.UTestPath;
import no.simula.se.testmodel.TestModel.UTestSet;

public class SMGraph {
	
	private StateMachine sm;
	private EList<Region> regions;
	private List<UMLRegionDirectedGraph<UMLVertex, UMLTransitionWeightEdge>> parallels;
	@SuppressWarnings("rawtypes")
	private List<UMLParallelPath> paths;
	private int algo;
	private int combinations;
	private int minus=0;
	
	@SuppressWarnings("rawtypes")
	public SMGraph(StateMachine sm, int algo, int combinations) throws GraphsException{
		this.sm = sm;
		this.regions = sm.getRegions();
		this.algo = algo;
		this.combinations = combinations;
		parallels = new ArrayList<UMLRegionDirectedGraph<UMLVertex, UMLTransitionWeightEdge>>();
		paths = new ArrayList<UMLParallelPath>();
		generateGraphs(algo, combinations);
		generatePath(algo, combinations);
	}
	
	@SuppressWarnings("rawtypes")
	public SMGraph(EList<Region> regions, int algo, int combinations) throws GraphsException{
		//this.sm = sm;
		this.regions = regions;
		this.algo = algo;
		this.combinations = combinations;
		parallels = new ArrayList<UMLRegionDirectedGraph<UMLVertex, UMLTransitionWeightEdge>>();
		paths = new ArrayList<UMLParallelPath>();
		generateGraphs(algo, combinations);
		generatePath(algo, combinations);
	}
	@SuppressWarnings("rawtypes")
	public SMGraph(StateMachine sm, int algo, int combinations, boolean isloop, int minus) throws GraphsException{
		this.sm = sm;
		this.regions = sm.getRegions();
		this.algo = algo;
		this.combinations = combinations;
		parallels = new ArrayList<UMLRegionDirectedGraph<UMLVertex, UMLTransitionWeightEdge>>();
		paths = new ArrayList<UMLParallelPath>();
		this.minus = minus;
		generateGraphs(algo, combinations, isloop);
		generatePath(algo, combinations);
	}
	@SuppressWarnings("rawtypes")
	public SMGraph(EList<Region> regions, int algo, int combinations, boolean isloop) throws GraphsException{
		//this.sm = sm;
		this.regions = regions;
		this.algo = algo;
		this.combinations = combinations;
		parallels = new ArrayList<UMLRegionDirectedGraph<UMLVertex, UMLTransitionWeightEdge>>();
		paths = new ArrayList<UMLParallelPath>();
		generateGraphs(algo, combinations, isloop);
		generatePath(algo, combinations);
	}
	@SuppressWarnings("rawtypes")
	public SMGraph(EList<Region> regions, int algo, int combinations, boolean isloop, int minus) throws GraphsException{
		//this.sm = sm;
		this.regions = regions;
		this.algo = algo;
		this.combinations = combinations;
		this.minus = minus;
		parallels = new ArrayList<UMLRegionDirectedGraph<UMLVertex, UMLTransitionWeightEdge>>();
		paths = new ArrayList<UMLParallelPath>();
		generateGraphs(algo, combinations, isloop);
		generatePath(algo, combinations);
	}
	
	/*---------------To EMF Element--------------------------*/
	public UTestSet convertToUTestElement() throws BeliefUtilityException{
		return convertToUTestElement(2);
	}
	@SuppressWarnings("rawtypes")
	public UTestSet convertToUTestElement(int theory) throws BeliefUtilityException{
		UTestSet set = UTestModelFactory.eINSTANCE.createUTestSet();
		for(UMLParallelPath path : this.getPaths()){
			set.setAttachedObject(sm);
			set.getUtestcase().add(path.convertToUTestCase(theory));
		}
		return set;
	}
	@SuppressWarnings("rawtypes")
	public void convertToUTestElement(UTestItem item, int theory) throws BeliefUtilityException{
		UTestSet set = UTestModelFactory.eINSTANCE.createUTestSet();
		for(UMLParallelPath path : this.getPaths()){
			set.setAttachedObject(sm);
			set.getUtestcase().add(path.convertToUTestCase(theory));
		}
		item.getUtestset().add(set);
	}
	
	public void convertToUTestElement(UTestItem item) throws BeliefUtilityException{
		convertToUTestElement(item, 2);
	}
	
	public void caculatebd(UTestPath upath){
		double db = 1.0;
		for(UTestCaseElement e : upath.getSequence()){
			if(e instanceof UTestAction && e.getMeasuredValue() != 0){
				db = db * e.getMeasuredValue();
			}else if(e instanceof UTestParallel){
				
			}
		}
	}
	/*-------End--------To EMF Element-----------------------*/
	
	
	public void generateGraphs(int algo, int combincations) throws GraphsException{
		for(Region r : this.getRegions()){
			generateGraph(r, algo, combincations, true);
		}
	}
	
	public void generateGraphs(int algo, int combincations, boolean isloop) throws GraphsException{
		for(Region r : this.getRegions()){
			generateGraph(r, algo, combincations, isloop);
		}
	}
	
	
	public StateMachine getSM(){
		return sm;
	}
	
	public void generateGraph(Region r, int algo, int combincations, boolean isloop) throws GraphsException{
		long time = System.currentTimeMillis();
		UMLRegionDirectedGraph<UMLVertex, UMLTransitionWeightEdge> graph = new UMLRegionDirectedGraph<UMLVertex, UMLTransitionWeightEdge>(UMLTransitionWeightEdge.class, r, isloop);
		parallels.add(graph);
		SMToGraphsUtil.getInstance();
		// add all vertex
		for(Vertex v : r.getSubvertices()){
			graph.addVertex(SMToGraphsUtil.createUMLVertex(v, algo, combincations, isloop, this.minus));
		}
		if(sm == null){
			if(this.getRegions().get(0).getOwner() instanceof State &&  ((State)this.getRegions().get(0).getOwner()).isComposite()){
				for(Pseudostate p :((State)this.getRegions().get(0).getOwner()).getConnectionPoints()){
					if(p.getKind().getValue() == PseudostateKind.EXIT_POINT){
						graph.addVertex(SMToGraphsUtil.createUMLVertex(p, algo, combincations, isloop, this.minus));
					}
				}
			}
		}
		// add all edges
		for(Transition t : r.getTransitions()){
			UMLTransitionWeightEdge edge = new UMLTransitionWeightEdge(t);
			graph.addEdge(SMToGraphsUtil.getSource(graph, edge), SMToGraphsUtil.getTarget(graph, edge), edge);
		}
		if(!isloop){
			//System.out.println(minus);
			graph.setMaxPathLength((graph.edgeSet().size()) + minus);
		}
		graph.generateEnterExit();
		SMToGraphsUtil.reportPerformanceFor("generate graph:", time);
		//System.out.println("the size of edge:"+graph.edgeSet().size()+" the size of vertex:"+graph.vertexSet().size());
	}
	@SuppressWarnings("rawtypes")
	public void generatePath(int algo, int combination) throws GraphsException{
		long time = System.currentTimeMillis();
		if(!paths.isEmpty()) paths.clear();
		SMToGraphsUtil.getInstance();
		for(UMLRegionDirectedGraph graph : this.getParallels()){
			//System.out.println("the size of edge:"+graph.edgeSet().size()+" the size of vertex:"+graph.vertexSet().size());
			graph.generatePaths(algo);
			SMToGraphsUtil.reportPerformanceFor("generate path:"+graph.getPaths().size(), time);
			graph.generatePathMap();
			SMToGraphsUtil.reportPerformanceFor("generate deep map:"+graph.getDeepPaths().size(), time);
		}
		generateAllCombinations();
		if(this.sm!=null) System.out.print("==StateMachine=="+sm.getName());
		SMToGraphsUtil.reportPerformanceFor("generate all combincations:"+this.getPaths().size(), time);
		
	}
	
	
	// all combinations
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void generateAllCombinations(){
		int[] distribution = new int[this.parallels.size()];
		for(int i = 0; i < this.parallels.size() ; i++){
			distribution[i] = this.parallels.get(i).getDeepPaths().size() -1;
		}
		boolean flag = true;
		int[] tmps = new int[distribution.length];
		for(int i = 0; i < distribution.length; i++){
			tmps[i] = distribution[i];
		}
		while(flag){
			flag = false;
			UMLParallelPath parallelPath = new UMLParallelPath();
			paths.add(parallelPath);
			for(int index = 0; index < tmps.length; index++){
				//System.out.println(this.sm);
				//System.out.println("======="+index+" "+this.getParallels().size()+" "+this.parallels.get(index).getDeepPaths().size()+" "+tmps[index]);
				parallelPath.getParallels().add(this.parallels.get(index).getDeepPaths().get(tmps[index]));					
			}
			for(int i = tmps.length-1; i >= 0; i--){
				int value = tmps[i];
				if(value -1 >= 0){
					tmps[i] = value -1;
					for(int j = i+1; j<tmps.length; j++){
						tmps[j] = distribution[j];
					}
					flag = true;
					break;
				}
			}
		}
	}
	@SuppressWarnings("rawtypes")
	public void printPaths(){
		int i = 0;
		for(UMLParallelPath p : this.getPaths()){
			System.out.println("==========="+(i+1)+"=============");
			System.out.println(p.toString());
			System.out.println();
			i++;
		}
	}

	/*--------------setter and getter----------------*/
	public StateMachine getSm() {
		return sm;
	}

	public void setSm(StateMachine sm) {
		this.sm = sm;
	}

	public List<UMLRegionDirectedGraph<UMLVertex, UMLTransitionWeightEdge>> getParallels() {
		return parallels;
	}

	public void setParallels(List<UMLRegionDirectedGraph<UMLVertex, UMLTransitionWeightEdge>> parallels) {
		this.parallels = parallels;
	}
	@SuppressWarnings("rawtypes")
	public List<UMLParallelPath> getPaths() {
		return paths;
	}
	@SuppressWarnings("rawtypes")
	public void setPaths(List<UMLParallelPath> paths) {
		this.paths = paths;
	}

	public int getAlgo() {
		return algo;
	}

	public void setAlgo(int algo) {
		this.algo = algo;
	}

	public int getCombinations() {
		return combinations;
	}

	public void setCombinations(int combinations) {
		this.combinations = combinations;
	}

	public EList<Region> getRegions() {
		return regions;
	}

	public void setRegions(EList<Region> regions) {
		this.regions = regions;
	}
	
	
	
}
