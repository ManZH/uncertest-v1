/*******************************************************************************
 * Copyright (c) 2016, 2017 Simula Research Laboratory. 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Simula license 
 * which accompanies this distribution, and is available at LICENSE.TXT
 *  
 *  Contributors:
 *        Man Zhang
 *******************************************************************************/
package no.simula.se.testing.strategies.jgraph;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.uml2.uml.Element;

import no.simula.se.testing.utility.belief.BeliefUtility;
import no.simula.se.testing.utility.belief.BeliefUtilityException;
import no.simula.se.testmodel.TestModel.UTestAction;
import no.simula.se.testmodel.TestModel.UTestCase;
import no.simula.se.testmodel.TestModel.UTestCaseElement;
import no.simula.se.testmodel.TestModel.UTestModelFactory;
import no.simula.se.testmodel.TestModel.UTestParallel;
import no.simula.se.testmodel.TestModel.UTestPath;

public class UMLParallelPath<V, E> {
	private List<UMLPath<V, E>> parallels;
	private int length;
	
	public UMLParallelPath(){
		parallels = new ArrayList<UMLPath<V, E>>();
	}
	public List<UMLPath<V, E>> getParallels() {
		return parallels;
	}

	public void setParallel(List<UMLPath<V, E>> parallels) {
		this.parallels = parallels;
	}
	
	public boolean isParallel(){
		return (parallels.size() > 1);
	}
	
	// min
	public double caculatedb(UTestParallel parallel, int theory){
		double db = 1.0;
		for(UTestPath p : parallel.getParalles()){
			if(p.getMeasuredValue() == 0) p.setMeasuredValue(caculatedb(p, theory));
			if(p.getMeasuredValue() < db) db = p.getMeasuredValue();
		}
		return db;
	}
	
	public double caculatedb(UTestPath path, int theory){
		double db = 1.0;
		if(theory == 0){
			for(UTestCaseElement e : path.getSequence()){
				if(e instanceof UTestAction && e.getMeasuredValue() != 0.0){
					db = db * e.getMeasuredValue();
				}else if(e instanceof UTestParallel){
					if(e.getMeasuredValue() == 0){
						e.setMeasuredValue(caculatedb((UTestParallel)e, theory));
					}
					db = db * e.getMeasuredValue();
				}
			}
		}else if(theory == 1){
			double c = 0;
			db = 0.0;
			for(UTestCaseElement e : path.getSequence()){
				if(e instanceof UTestAction && e.getMeasuredValue() != 0.0 && e.getMeasuredValue() != 1.0){
					db = db + e.getMeasuredValue();
					c++;
				}else if(e instanceof UTestParallel){
					if(e.getMeasuredValue() == 0){
						e.setMeasuredValue(caculatedb((UTestParallel)e, theory));
					}
					if(e.getMeasuredValue() != 0.0 && e.getMeasuredValue() != 1.0){
						db = db + e.getMeasuredValue();
						c++;
					}
				}
			}
			db = db/c;
		}else if(theory == 2){//min
			db = 1.0;
			for(UTestCaseElement e : path.getSequence()){
				if(e instanceof UTestAction && e.getMeasuredValue() != 0.0){
					if(e.getMeasuredValue() < db)
						db = e.getMeasuredValue();
				}else if(e instanceof UTestParallel){
					if(e.getMeasuredValue() == 0){
						e.setMeasuredValue(caculatedb((UTestParallel)e, theory));
					}
					if(e.getMeasuredValue() < db)
						db = e.getMeasuredValue();
				}
			}
		}else if(theory == 3){//max
			db = 0;
			for(UTestCaseElement e : path.getSequence()){
				if(e instanceof UTestAction && e.getMeasuredValue() != 0.0){
					if(e.getMeasuredValue() != 1.0 && e.getMeasuredValue() > db)
						db = e.getMeasuredValue();
				}else if(e instanceof UTestParallel){
					if(e.getMeasuredValue() == 0){
						e.setMeasuredValue(caculatedb((UTestParallel)e, theory));
					}
					if(e.getMeasuredValue() != 1.0 && e.getMeasuredValue() > db)
						db = e.getMeasuredValue();
				}
			}
		}
		//path.setMeasuredValue(db);
		return db;
	}
	
	
	//0 - #uncertainty; 1- #unique uncertainty; 2 - ratio #uncertainty/size; 3 - the first uncertainty index; 4- risk
	public void processUTestPath(UTestPath path) throws BeliefUtilityException{
		double[] info = new double[3];
		info[0] = 0; 
		Set uncertSet = new HashSet();
		int size = 0;
		int length = 0;
		if(path.getSize() == 0){
			for(UTestCaseElement e : path.getSequence()){
				if(e instanceof UTestParallel){
					if(((UTestParallel) e).getSize() == 0 || ((UTestParallel)e).getLength() == 0)
						processUTestParallel((UTestParallel)e);
					size = size + ((UTestParallel)e).getSize();
					length = length + ((UTestParallel)e).getLength();
					uncertSet.addAll(BeliefUtility.getUncertainties(((UTestParallel)e)));
					info[0] = ((UTestParallel) e).getUinfo().get(0);
				}else if(e instanceof UTestAction){
					EObject unObj = BeliefUtility.getUncertaintiesWithCause((Element)((UTestAction) e).getAttachedObject());	
					if(unObj != null){
						uncertSet.add(unObj);
						info[0]++;
					}
					size++;
					length++;
				}
			}
		}
		//int tmp = ;
		info[1] = uncertSet.size();
		info[2] = info[1]/size;
		path.setLength(length);
		path.setSize(size);
		path.getUinfo().clear();
		path.getUinfo().add(info[0]);
		path.getUinfo().add(info[1]);
		path.getUinfo().add(info[2]);
	}
	
	
	public void processUTestParallel(UTestParallel parallel) throws BeliefUtilityException{
		Set uncertSet = BeliefUtility.getUncertainties(parallel);
		double[] info = new double[3];
		info[0] = 0; info[2] = 0;
		info[1] = uncertSet.size();
		int size = 0;
		int length = 0;
		for(UTestPath p : parallel.getParalles()){
			if(p.getLength() == 0 || p.getSize() == 0) processUTestPath(p);
			size = size + p.getSize();
			if(p.getLength() > length)
				length = p.getLength();
			info[0] = info[0] + p.getUinfo().get(0);
		}
		parallel.setSize(size);
		parallel.setLength(length);
		info[2] = info[1]/size;
		parallel.getUinfo().clear();
		parallel.getUinfo().add(info[0]);
		parallel.getUinfo().add(info[1]);
		parallel.getUinfo().add(info[2]);
	}
	public UTestCase convertToUTestCase(int theory) throws BeliefUtilityException{
		if(this.isParallel()){
			UTestParallel parallel = this.convertToUTestParallel();
			parallel.setMeasuredValue(caculatedb(parallel, theory));
			processUTestParallel(parallel);
			return parallel; 
		}else{
			UTestPath path = UTestModelFactory.eINSTANCE.createUTestPath();
			path.getSequence().addAll(this.convertToUTestCaseElements());
			path.setMeasuredValue(caculatedb(path, theory));
			processUTestPath(path);
			return path;
		}
	}
	
	public UTestCase convertToUTestCase() throws BeliefUtilityException{
		if(this.isParallel()){
			UTestParallel parallel = this.convertToUTestParallel();
			parallel.setMeasuredValue(caculatedb(parallel, 0));
			processUTestParallel(parallel);
			return parallel; 
		}else{
			UTestPath path = UTestModelFactory.eINSTANCE.createUTestPath();
			path.getSequence().addAll(this.convertToUTestCaseElements());
			path.setMeasuredValue(caculatedb(path, 0));
			processUTestPath(path);
			return path;
		}
	}
	
	
	public EList<UTestCaseElement> convertToUTestCaseElements(){
		return parallels.get(0).convertToUTestCaseElements();
	}
	
	public UTestParallel convertToUTestParallel(){
		UTestParallel parallel = UTestModelFactory.eINSTANCE.createUTestParallel();
		for(UMLPath path : parallels){
			UTestPath upath = UTestModelFactory.eINSTANCE.createUTestPath();
			parallel.getParalles().add(upath);
			upath.getSequence().addAll(path.convertToUTestCaseElements());
		}
		return parallel;
	}
	
	public List<Object>[] listUMLParallelPath(){
		List[] list = new List[parallels.size()];
		for(int i = 0; i < this.getParallels().size(); i++){
			list[i] = this.getParallels().get(i).listUMLPath();
		}
		return list;
	}
	
	// the length of parallels is the max one.
	public void setLength(){
		int l = 0;
		for(UMLPath p: this.getParallels()){
			if(p.getLength() == 0) p.setLength();
			if(p.getLength() > l)
				l = p.getLength();
		}
		this.setLength(l);
	}
	
	public int getLength() {
		return length;
	}
	
	public int getCurrrentLength(){
		setLength();
		return getLength();
	}
	public void setLength(int length) {
		this.length = length;
	}
	public void setParallels(List<UMLPath<V, E>> parallels) {
		this.parallels = parallels;
	}
	public String toString(){
		String str = "[ size="+parallels.size()+":";
		for(int i =0 ; i < this.getParallels().size(); i++){
			str = str + i+"-"+this.getParallels().get(i).toString()+";";
		}
		return str = str +"]";
	}
	
}
