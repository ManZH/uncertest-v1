/*******************************************************************************
 * Copyright (c) 2016, 2017 Simula Research Laboratory. 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Simula license 
 * which accompanies this distribution, and is available at LICENSE.TXT
 *  
 *  Contributors:
 *        Man Zhang
 *******************************************************************************/
package no.simula.se.testing.strategies.jgraph;

import org.eclipse.uml2.uml.FinalState;

public class UMLFinalState extends UMLVertex {
	private static String simple = "Final";
	
	public UMLFinalState(FinalState state){
		super(state);
	}
	
	public FinalState getVertex(){
		return (FinalState) super.getVertex();
	}
	
	public String toString(){
		return super.toString(simple);
	}
}
