/*******************************************************************************
 * Copyright (c) 2016, 2017 Simula Research Laboratory. 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Simula license 
 * which accompanies this distribution, and is available at LICENSE.TXT
 *  
 *  Contributors:
 *        Man Zhang
 *******************************************************************************/
package no.simula.se.testing.strategies.jgraph;

import org.eclipse.uml2.uml.State;

public class UMLCompositeState extends UMLSimpleState {
	
	private static String simple = "Composite";
	
	private SMGraph graph;
	public UMLCompositeState(State state){
		super(state);
	}
	public UMLCompositeState(State state, int algo, int combination) throws GraphsException{
		super(state);
		graph = new SMGraph(state.getRegions(), algo, combination);
	}
	public UMLCompositeState(State state, int algo, int combination, boolean isloop, int minus) throws GraphsException{
		super(state);
		graph = new SMGraph(state.getRegions(), algo, combination, isloop, minus);
	}
	
	public SMGraph getGraph() {
		return graph;
	}

	public void setGraph(SMGraph graph) {
		this.graph = graph;
	}
	
	public String toString(){
		return super.toString(simple);
	}
}
