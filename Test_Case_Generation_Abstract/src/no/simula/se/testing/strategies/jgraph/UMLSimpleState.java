/*******************************************************************************
 * Copyright (c) 2016, 2017 Simula Research Laboratory. 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Simula license 
 * which accompanies this distribution, and is available at LICENSE.TXT
 *  
 *  Contributors:
 *        Man Zhang
 *******************************************************************************/
package no.simula.se.testing.strategies.jgraph;

import org.eclipse.uml2.uml.State;

public class UMLSimpleState extends UMLVertex{
	
	private static String simple = "Simple";
	
	public UMLSimpleState(State s){
		super(s);
	}
	
	public State getVertex(){
		return (State)super.getVertex();
	}
	
	public String toString(){
		return super.toString(simple);
	}
}
