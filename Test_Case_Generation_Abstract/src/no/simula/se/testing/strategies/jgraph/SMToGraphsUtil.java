/*******************************************************************************
 * Copyright (c) 2016, 2017 Simula Research Laboratory. 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Simula license 
 * which accompanies this distribution, and is available at LICENSE.TXT
 *  
 *  Contributors:
 *        Man Zhang
 *******************************************************************************/
package no.simula.se.testing.strategies.jgraph;

import org.eclipse.uml2.uml.FinalState;
import org.eclipse.uml2.uml.Pseudostate;
import org.eclipse.uml2.uml.State;
import org.eclipse.uml2.uml.Vertex;

public class SMToGraphsUtil {
	
	private static SMToGraphsUtil instance;
	
	public static SMToGraphsUtil getInstance(){
		if(instance == null) instance = new SMToGraphsUtil();
		return instance;
	}
	
	public static UMLVertex createUMLVertex(Vertex v, int algo, int combincations,boolean isloop, int minus) throws GraphsException{
		if(v instanceof State){
			if(v instanceof FinalState){
				return new UMLFinalState((FinalState)v);
			}
			State s = (State)v;
			if(s.isSimple()){
				return new UMLSimpleState(s);
			}else if(s.isComposite()){
				return new UMLCompositeState(s, algo, combincations, isloop, minus);
			}else if(s.isSubmachineState()){
				return new UMLSubmachineState(s, algo, combincations, isloop, minus);
			}
		}else if(v instanceof Pseudostate){
			return new UMLPseudostate((Pseudostate)v);
		} 
		
		throw new GraphsException("fail to create UMLVertex: kind of State cannot be recoginzed.");
	}
	
	public static UMLVertex getSource(
			UMLRegionDirectedGraph<UMLVertex, UMLTransitionWeightEdge> graph, UMLTransitionWeightEdge edge) throws GraphsException{
		for(UMLVertex v : graph.vertexSet()){
			if(edge.getAttachedTransition().getSource().equals(v.getVertex()))
				return v;
		}
		
		for(UMLVertex v : graph.vertexSet()){
			if(v instanceof UMLCompositeState){
				for(UMLRegionDirectedGraph<UMLVertex, UMLTransitionWeightEdge> g
						: ((UMLCompositeState) v).getGraph().getParallels()){
					for(UMLVertex inner_v : g.vertexSet()){
						if(inner_v.getVertex().equals(edge.getAttachedTransition().getSource())){
							return v;
						}
					}
				}
			}
		}
		
		for(UMLVertex v : graph.vertexSet()){
			System.out.println(v.getVertex());
		}
		// target is exist point
		System.out.println(edge.getAttachedTransition().getSource()+" "+edge.getAttachedTransition().getTarget()+" "+edge.getAttachedTransition().getOwner().getOwner());
		System.out.println(edge.getAttachedTransition());
		throw new GraphsException("SMToGraphsUtil getSource");
	}
	
	public static UMLVertex getTarget(
			UMLRegionDirectedGraph<UMLVertex, UMLTransitionWeightEdge> graph, UMLTransitionWeightEdge edge) throws GraphsException{
		for(UMLVertex v : graph.vertexSet()){
			if(edge.getAttachedTransition().getTarget().equals(v.getVertex()))
				return v;
		}
		for(UMLVertex v : graph.vertexSet()){
			System.out.println(v.getVertex());
		}
		// target is exist point
		System.out.println(edge.getAttachedTransition().getTarget()+" "+edge.getAttachedTransition().getSource()+" "+edge.getAttachedTransition().getOwner().getOwner());
		System.out.println(edge.getAttachedTransition());
		throw new GraphsException("SMToGraphsUtil Target");
	}
	
	public static void reportPerformanceFor(String msg, long refTime)    {
        double time = (System.currentTimeMillis() - refTime) / 1000.0;
        double mem = usedMemory()
            / (1024.0 * 1024.0);
        mem = Math.round(mem * 100) / 100.0;
        System.out.println(msg + " (" + time + " sec, " + mem + "MB)");
    }
	
	 public static long usedMemory(){
        Runtime rt = Runtime.getRuntime();

        return rt.totalMemory() - rt.freeMemory();
    }
	
}
