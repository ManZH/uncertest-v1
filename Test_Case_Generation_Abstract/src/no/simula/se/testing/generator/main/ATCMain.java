/*******************************************************************************
 * Copyright (c) 2016, 2017 Simula Research Laboratory. 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Simula license 
 * which accompanies this distribution, and is available at LICENSE.TXT
 *  
 *  Contributors:
 *        Man Zhang
 *******************************************************************************/
package no.simula.se.testing.generator.main;

import java.util.HashSet;
import java.util.Set;

import org.eclipse.core.resources.IResource;
import org.eclipse.uml2.uml.StateMachine;

import no.simula.se.testing.generator.atc.ToUTestModel;
import no.simula.se.testing.strategies.jgraph.GraphsException;
import no.simula.se.testing.utility.ModelUtil;
import no.simula.se.testing.utility.belief.BeliefUtilityException;
import no.simula.se.testmodel.TestModel.UTestItem;
import no.simula.se.testmodel.TestModel.UTestModel;
import no.simula.se.testmodel.TestModel.UTestSet;

public class ATCMain {
	
	public static ATCMain instance;
	public static ATCMain getInstance(){
		if(instance == null)
			instance = new ATCMain();
		return instance;
	}
	
	/**
	 * Generate abstract tests for all state machines in model using default generation settings
	 * @param model specify the full uml model
	 * @param outputFileName specify the output file name
	 * @param output specify a path where to save the generated file
	 */
	public static void exampleGenerateATCsSave(org.eclipse.uml2.uml.Package model, String outputFileName, String output){
		ToUTestModel toUTestModel = new ToUTestModel(true, 0);
		try {
			ModelUtil.save(model.eResource().getResourceSet(),toUTestModel.converToUTestModel(model), outputFileName, output);
		} catch (GraphsException | BeliefUtilityException e) {
			e.printStackTrace();
		}
		System.out.println("done!");
	}
	
	
	/**
	 * Generate abstract tests for one state machine
	 * @param model specify the full uml model
	 * @param smName specify the name of state machine
	 * @param isSimple whether only generate simple abstract tests
	 * @param mins (default is 0) specify maximum length for generated abstract tests regarding a number of elements in sm, e.g., -2 means that the maximum length is a number of elements -2.
	 * @param file output file which saves the generated abstract tests model as .utestmodel
	 * @return
	 */
	public static String[] exampleGenerateATCsSave(org.eclipse.uml2.uml.Package model, String smName, boolean isSimple, int mins, IResource file){
		Set<String> info = new HashSet<String>();
		long time = System.currentTimeMillis();
		ToUTestModel toUTestModel = new ToUTestModel(isSimple, smName, mins);
		try {
			UTestModel utestModel = toUTestModel.converToUTestModel(model);
			info.add(getPerformance("The performance of test case generation is", time));
			for(UTestItem obj : utestModel.getUtestitem()){
				for(UTestSet set : obj.getUtestset()){
					info.add(String.format("The number of test cases of the %s is %d", ((StateMachine)set.getAttachedObject()).getName(), set.getUtestcase().size()));
				}
			}
			time = System.currentTimeMillis();
			ModelUtil.save(utestModel, file);
			info.add(getPerformance("The performance of emf serialization is", time));

		} catch (GraphsException | BeliefUtilityException e) {
			e.printStackTrace();
		}
		return info.toArray(new String[info.size()]);
	}
	
	public static String getPerformance(String msg, long startTime){
		double time = (System.currentTimeMillis() - startTime) / 1000.0;
        double mem = usedMemory()
            / (1024.0 * 1024.0);
        mem = Math.round(mem * 100) / 100.0;
        return msg+" (" + time + " sec, " + mem + "MB)";
	}
	
	private static long usedMemory(){
	    Runtime rt = Runtime.getRuntime();
	    return rt.totalMemory() - rt.freeMemory();
	}
}
