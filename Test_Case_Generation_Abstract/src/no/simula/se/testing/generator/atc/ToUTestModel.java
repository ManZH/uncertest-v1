/*******************************************************************************
 * Copyright (c) 2016, 2017 Simula Research Laboratory. 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Simula license 
 * which accompanies this distribution, and is available at LICENSE.TXT
 *  
 *  Contributors:
 *        Man Zhang
 *******************************************************************************/
package no.simula.se.testing.generator.atc;

import java.util.HashSet;
import java.util.Set;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.uml2.common.util.UML2Util.EObjectMatcher;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.InstanceSpecification;
import org.eclipse.uml2.uml.StateMachine;
import org.eclipse.uml2.uml.util.UMLUtil;

import no.simula.se.testing.strategies.jgraph.GraphsException;
import no.simula.se.testing.strategies.jgraph.SMGraph;
import no.simula.se.testing.strategies.jgraph.TCsCombinations;
import no.simula.se.testing.strategies.jgraph.TCsGenerateAlgo;
import no.simula.se.testing.uncertainty_theory.UncertaintyTheoryUtil;
import no.simula.se.testing.utility.belief.BeliefUtility;
import no.simula.se.testing.utility.belief.BeliefUtilityException;
import no.simula.se.testing.utility.belief.UTP2Utility;
import no.simula.se.testmodel.TestModel.UTestConfiguration;
import no.simula.se.testmodel.TestModel.UTestItem;
import no.simula.se.testmodel.TestModel.UTestModel;
import no.simula.se.testmodel.TestModel.UTestModelFactory;
import no.simula.se.testmodel.TestModel.UTestSet;

public class ToUTestModel {
	
	private int algo; 
	private int combinations;
	boolean isloop;
	private int theory; // 2- uncertainty theory
	private int minus = 0;
	private String sm_name;
	private String[] sm_names;
	private GenerationConfig[] genConfigs;
	
	public ToUTestModel(){
		this.algo = TCsGenerateAlgo.ALL_DIRECTED_PATHS;
		this.combinations = TCsCombinations.ALL_COMBINATIONS;
		this.isloop = false;
		this.theory = 2;
	}
	public ToUTestModel(boolean isloop, int minus){
		this.algo = TCsGenerateAlgo.ALL_DIRECTED_PATHS;
		this.combinations = TCsCombinations.ALL_COMBINATIONS;
		this.isloop = isloop;
		this.theory = 2;
		this.minus = minus;
	}
	
	public ToUTestModel(boolean isloop, String sm_name, int minus){
		this.algo = TCsGenerateAlgo.ALL_DIRECTED_PATHS;
		this.combinations = TCsCombinations.ALL_COMBINATIONS;
		this.isloop = isloop;
		this.theory = 2;
		this.minus = minus;
		this.sm_name = sm_name;
	}
	
	public ToUTestModel(GenerationConfig[] configs){
		this.algo = TCsGenerateAlgo.ALL_DIRECTED_PATHS;
		this.combinations = TCsCombinations.ALL_COMBINATIONS;
		this.theory = 2;
		this.genConfigs = configs;
	}
	
	public ToUTestModel(boolean isloop, String[] sm_names, int minus){
		this.algo = TCsGenerateAlgo.ALL_DIRECTED_PATHS;
		this.combinations = TCsCombinations.ALL_COMBINATIONS;
		this.isloop = isloop;
		this.theory = 2;
		this.minus = minus;
		this.sm_names = sm_names;
	}
	
	public ToUTestModel(int algo, int combinations, boolean isloop){
		this.algo = algo;
		this.combinations = combinations;
		this.isloop = isloop;
		this.theory = 2;
	}
	
	public UTestModel converToUTestModel(org.eclipse.uml2.uml.Package model) throws GraphsException, BeliefUtilityException{
		UTestModel _tmodel = UTestModelFactory.eINSTANCE.createUTestModel();
		generateSpecifiedTestConfigs(_tmodel, model);
		generateTestItem(_tmodel, model);
		return _tmodel;
	}
	
	
	public UTestModel converToUTestModel(org.eclipse.uml2.uml.Package model, String sm) throws GraphsException, BeliefUtilityException{
		UTestModel _tmodel = UTestModelFactory.eINSTANCE.createUTestModel();
		generateSpecifiedTestConfigs(_tmodel, model);
		generateTestItem(_tmodel, model, sm, minus);
		return _tmodel;
	}
	
	public UTestModel generateConfigs(org.eclipse.uml2.uml.Package model){
		UTestModel _tmodel = UTestModelFactory.eINSTANCE.createUTestModel();
		generateSpecifiedTestConfigs(_tmodel, model);
		return _tmodel;
	}
	
	public UTestModel converToUTestModel(org.eclipse.uml2.uml.Package model, String... setups) throws GraphsException, BeliefUtilityException{
		UTestModel _tmodel = UTestModelFactory.eINSTANCE.createUTestModel();
		generateSpecifiedTestConfigs(_tmodel, model, setups);
		generateTestItem(_tmodel, model);
		return _tmodel;
	}
	
	//FIXME Man Zhang: only package supported
	private void generateSpecifiedTestConfigs(UTestModel _tmodel, org.eclipse.uml2.uml.Package model){
		if(this.genConfigs != null){
			Set<String> overall = new HashSet<String>();
			for(GenerationConfig conf : genConfigs){
				if(conf.getTestConfigs() != null){
					overall.addAll(conf.getTestConfigs());
				}
			}
			if(overall.size() > 0){
				for(String setup_name : overall){
					generateSpecifiedTestConfigs(_tmodel, model, setup_name);
				}
			}
		}else{
			generateSpecifiedTestConfigs(_tmodel, model, "TestSetup");
		}
		
	}

	
	public void generateSpecifiedTestConfigs(UTestModel _tmodel, org.eclipse.uml2.uml.Package model, String... setups){
		for(Element e: model.allOwnedElements()){
			
			if(e instanceof org.eclipse.uml2.uml.Package){
				org.eclipse.uml2.uml.Package _pack = (org.eclipse.uml2.uml.Package)e;
				if(isPart(setups, _pack.getName())){
					for(Element in : _pack.getOwnedElements()){
						if(in instanceof InstanceSpecification){
							UTestConfiguration _tconfig = UTestModelFactory.eINSTANCE.createUTestConfiguration();
							_tconfig.setAttachedObject(in);
							_tmodel.getUtestconfiguration().add(_tconfig);
						}
					}
				}
				
			}
		}
	}
	
	private boolean isPart(String[] all, String one){
		for(String e : all){
			if(e.equals(one)) return true;
		}
		return false;
	}
	
	private void generateTestItem(UTestModel _tmodel, org.eclipse.uml2.uml.Package model) throws GraphsException, BeliefUtilityException{
		for(Element e : model.getOwnedElements()){
			
			if(e instanceof org.eclipse.uml2.uml.Package && UTP2Utility.isApplyTestContext(e)){
				generateTestItem(_tmodel, (org.eclipse.uml2.uml.Package)e);
			}
			
			
			if(e instanceof org.eclipse.uml2.uml.Classifier){
				UTestItem item = null;
				for(Element inside : e.getOwnedElements()){
					if(inside instanceof StateMachine){
						if(this.sm_name!= null){
							StateMachine sm = (StateMachine)inside;
							if(sm.getName().equals(this.sm_name)){
								if(item == null) item = UTestModelFactory.eINSTANCE.createUTestItem();
								SMGraph smGraph = new SMGraph(sm, algo, combinations, isloop,minus);
								UTestSet set = smGraph.convertToUTestElement(theory);
								UncertaintyTheoryUtil.getInstance();
								UncertaintyTheoryUtil.createUMSpaceA(set);
								item.getUtestset().add(set);
							}
							
						}else if(this.sm_names != null){
							for(String name : sm_names){
								StateMachine sm = (StateMachine)inside;
								if(sm.getName().equals(name)){
									if(item == null) item = UTestModelFactory.eINSTANCE.createUTestItem();
									SMGraph smGraph = new SMGraph(sm, algo, combinations, isloop,minus);
									UTestSet set = smGraph.convertToUTestElement(theory);
									UncertaintyTheoryUtil.getInstance();
									UncertaintyTheoryUtil.createUMSpaceA(set);
									item.getUtestset().add(set);
								}
							}
						}else if(this.genConfigs != null){
							for(GenerationConfig conf : genConfigs){
								StateMachine sm = (StateMachine)inside;
								if(sm.getName().equals(conf.getSm_name())){
									if(item == null) item = UTestModelFactory.eINSTANCE.createUTestItem();
									SMGraph smGraph = new SMGraph(sm, algo, combinations, conf.isSimple(),conf.getRelative_length());
									UTestSet set = smGraph.convertToUTestElement(theory);
									UncertaintyTheoryUtil.getInstance();
									UncertaintyTheoryUtil.createUMSpaceA(set);
									item.getUtestset().add(set);
								}
							}
						}
						
						else{
							StateMachine sm = (StateMachine)inside;
							if(item == null) item = UTestModelFactory.eINSTANCE.createUTestItem();
							SMGraph smGraph = new SMGraph(sm, algo, combinations, isloop,minus);
							UTestSet set = smGraph.convertToUTestElement(theory);
							UncertaintyTheoryUtil.getInstance();
							UncertaintyTheoryUtil.createUMSpaceA(set);
							item.getUtestset().add(set);
						}
						
					}
				}
				if(item != null){
					_tmodel.getUtestitem().add(item);
					item.setAttachedObject(e);
				}
			}
			if(e instanceof StateMachine){
				if(this.sm_name != null){
					StateMachine sm = (StateMachine)e;
					if(sm.getName().equals(this.sm_name)){
						UTestItem item = UTestModelFactory.eINSTANCE.createUTestItem();
						item.setAttachedObject(sm.getOwner());//if attached object is model, it is integrated one.
						
						SMGraph smGraph = new SMGraph(sm, algo, combinations, isloop,minus);
						UTestSet set = smGraph.convertToUTestElement(theory);
						UncertaintyTheoryUtil.getInstance();
						UncertaintyTheoryUtil.createUMSpaceA(set);
						//set.setAttachedObject(sm);
						item.getUtestset().add(set);
						_tmodel.getUtestitem().add(item);
					}
				}else if(this.sm_names != null){
					for(String name : sm_names){
						StateMachine sm = (StateMachine)e;
						if(sm.getName().equals(name)){
							boolean exist = false;
							for(UTestItem item : _tmodel.getUtestitem()){
								if(item.getAttachedObject().equals(sm.getOwner())){
									SMGraph smGraph = new SMGraph(sm, algo, combinations, isloop,minus);
									UTestSet set = smGraph.convertToUTestElement(theory);
									UncertaintyTheoryUtil.getInstance();
									UncertaintyTheoryUtil.createUMSpaceA(set);
									//set.setAttachedObject(sm);
									item.getUtestset().add(set);
									exist = true;
									break;
								}
								
							}
							if(!exist){
								UTestItem item = UTestModelFactory.eINSTANCE.createUTestItem();
								item.setAttachedObject(sm.getOwner());//if attached object is model, it is integrated one.
								
								SMGraph smGraph = new SMGraph(sm, algo, combinations, isloop,minus);
								UTestSet set = smGraph.convertToUTestElement(theory);
								UncertaintyTheoryUtil.getInstance();
								UncertaintyTheoryUtil.createUMSpaceA(set);
								//set.setAttachedObject(sm);
								item.getUtestset().add(set);
								_tmodel.getUtestitem().add(item);
							}
							
						}
					}
				}else if(this.genConfigs != null){
					for(GenerationConfig conf : this.genConfigs){
						StateMachine sm = (StateMachine)e;
						if(sm.getName().equals(conf.getSm_name())){
							boolean exist = false;
							for(UTestItem item : _tmodel.getUtestitem()){
								if(item.getAttachedObject().equals(sm.getOwner())){
									SMGraph smGraph = new SMGraph(sm, algo, combinations, conf.isSimple(),conf.getRelative_length());
									UTestSet set = smGraph.convertToUTestElement(theory);
									UncertaintyTheoryUtil.getInstance();
									UncertaintyTheoryUtil.createUMSpaceA(set);
									//set.setAttachedObject(sm);
									item.getUtestset().add(set);
									exist = true;
									break;
								}
								
							}
							if(!exist){
								UTestItem item = UTestModelFactory.eINSTANCE.createUTestItem();
								item.setAttachedObject(sm.getOwner());//if attached object is model, it is integrated one.
								
								SMGraph smGraph = new SMGraph(sm, algo, combinations, conf.isSimple(),conf.getRelative_length());
								UTestSet set = smGraph.convertToUTestElement(theory);
								UncertaintyTheoryUtil.getInstance();
								UncertaintyTheoryUtil.createUMSpaceA(set);
								//set.setAttachedObject(sm);
								item.getUtestset().add(set);
								_tmodel.getUtestitem().add(item);
							}
							
						}
					}
				}else{
					UTestItem item = UTestModelFactory.eINSTANCE.createUTestItem();
					item.setAttachedObject(model);//if attached object is model, it is integrated one.
					StateMachine sm = (StateMachine)e;
					SMGraph smGraph = new SMGraph(sm, algo, combinations, isloop,minus);
					UTestSet set = smGraph.convertToUTestElement(theory);
					UncertaintyTheoryUtil.getInstance();
					UncertaintyTheoryUtil.createUMSpaceA(set);
					//set.setAttachedObject(sm);
					item.getUtestset().add(set);
					_tmodel.getUtestitem().add(item);
				}
				
			}
		}
	}
	
	private void generateTestItem(UTestModel _tmodel, org.eclipse.uml2.uml.Package model, String sm_name, int minus) throws GraphsException, BeliefUtilityException{
		UTestItem item = null;
		
		StateMachine sm = (StateMachine) UMLUtil.findEObject(model.allOwnedElements(), new EObjectMatcher(){
			@Override
			public boolean matches(EObject eObject) {
				return (eObject instanceof StateMachine) && ((StateMachine)eObject).getName().equals(sm_name);
			}});
		if(sm!=null){
			item = UTestModelFactory.eINSTANCE.createUTestItem();
			BeliefUtility.getInstance();
			BeliefUtility.printSMDetails(sm);
			SMGraph smGraph = new SMGraph(sm, TCsGenerateAlgo.ALL_DIRECTED_PATHS, TCsCombinations.ALL_COMBINATIONS, false,minus);
			UTestSet set = smGraph.convertToUTestElement();
			UncertaintyTheoryUtil.getInstance();
			UncertaintyTheoryUtil.createUMSpaceA(set);
			item.getUtestset().add(set);
		}
		if(item != null){
			_tmodel.getUtestitem().add(item);
			item.setAttachedObject(sm.getOwner());
		}
	}
	
	public void reportCoverage(UTestModel result, org.eclipse.uml2.uml.Package model, GenerationConfig[] configs) throws BeliefUtilityException{
		System.err.println("=========== overall of the coverage =================");
		for(UTestItem item : result.getUtestitem()){
			for(UTestSet set : item.getUtestset()){
				String name = ((StateMachine)set.getAttachedObject()).getName();
				GenerationConfig config = findConfig(name, configs);
				if(config != null){
					System.err.println("State Machine: "+name);
					config.setTransitions_coverage(BeliefUtility.printCoverageTransition(set));
					config.setUncertainties_coverage(BeliefUtility.printCoverageUncertainty(set));
					System.err.println("the coverage of transition is "+config.getTransitions_coverage());
					System.err.println("the coverage of uncertainties is "+config.getUncertainties_coverage());
					System.err.println("---------------------");
				}
			}
		}
		System.err.println("===================================================");
	}
	
	public GenerationConfig findConfig(String name, GenerationConfig[] configs){
		for(GenerationConfig config : configs){
			if(name.equals(config.getSm_name()))
				return config;
		}
		return null;
	}
}
