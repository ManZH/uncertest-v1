/*******************************************************************************
 * Copyright (c) 2016, 2017 Simula Research Laboratory. 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Simula license 
 * which accompanies this distribution, and is available at LICENSE.TXT
 *  
 *  Contributors:
 *        Man Zhang
 *******************************************************************************/
package no.simula.se.testing.generator.atc;

import java.util.HashSet;
import java.util.Set;

public class GenerationConfig {
	private String sm_name;
	private boolean isSimple;
	private UncertaintyMeasureTheory theory = UncertaintyMeasureTheory.Uncertainty_Theory;
	private int relative_length;
	private int relative_times;
	private String namespace;
	
	
	private double states_coverage;
	private double transitions_coverage;
	private double uncertainties_coverage;
	
	private Set<String> testConfigs;
	
	public GenerationConfig(){}
	public GenerationConfig(String sm, boolean isSimple){
		this.sm_name = sm;
		this.isSimple = isSimple;
	}
	
	public GenerationConfig(String sm, boolean isSimple, String...testConfigs){
		this.sm_name = sm;
		this.isSimple = isSimple;
		this.testConfigs = new HashSet<String>();
		for(String conf : testConfigs){
			this.testConfigs.add(conf);
		}
	}
	
	public String getSm_name() {
		return sm_name;
	}
	public void setSm_name(String sm_name) {
		this.sm_name = sm_name;
	}
	public boolean isSimple() {
		return isSimple;
	}
	public void setSimple(boolean isSimple) {
		this.isSimple = isSimple;
	}
	public UncertaintyMeasureTheory getTheory() {
		return theory;
	}
	public void setTheory(UncertaintyMeasureTheory theory) {
		this.theory = theory;
	}
	public int getRelative_length() {
		return relative_length;
	}
	public void setRelative_length(int relative_lenght) {
		this.relative_length = relative_lenght;
	}
	public int getRelative_times() {
		return relative_times;
	}
	public void setRelative_times(int relative_times) {
		this.relative_times = relative_times;
	}
	public double getStates_coverage() {
		return states_coverage;
	}
	public void setStates_coverage(double states_coverage) {
		this.states_coverage = states_coverage;
	}
	public double getTransitions_coverage() {
		return transitions_coverage;
	}
	public void setTransitions_coverage(double transitions_coverage) {
		this.transitions_coverage = transitions_coverage;
	}
	public double getUncertainties_coverage() {
		return uncertainties_coverage;
	}
	public void setUncertainties_coverage(double uncertainties_coverage) {
		this.uncertainties_coverage = uncertainties_coverage;
	}
	public Set<String> getTestConfigs() {
		return testConfigs;
	}
	public String getNamespace() {
		return namespace;
	}
	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}
	
	
}
