/*******************************************************************************
 * Copyright (c) 2016, 2017 Simula Research Laboratory. 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Simula license 
 * which accompanies this distribution, and is available at LICENSE.TXT
 *  
 *  Contributors:
 *        Man Zhang
 *******************************************************************************/
package no.simula.se.testing.utility;

import java.util.Map;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.uml2.uml.Pseudostate;
import org.eclipse.uml2.uml.PseudostateKind;
import org.eclipse.uml2.uml.Region;
import org.eclipse.uml2.uml.State;
import org.eclipse.uml2.uml.Transition;
import org.eclipse.uml2.uml.Vertex;

public class SMUtil {
public  static ModelUtil util;
	
	public static ModelUtil getInstance(){
		if(util == null){
			util = new ModelUtil();
		}
		return util;
	}
	public static Vertex findInitialState(Region region){
		 for(Vertex v : region.getSubvertices()){
			 if(v instanceof Pseudostate){
				if(((Pseudostate) v).getKind() == PseudostateKind.INITIAL_LITERAL){
					return v;
				}
			 }
		 }
		 return null;
		 
	 }
	 
	 public static Vertex findFirstState(Region region, EList<EObject> tc){
		 Vertex init = findInitialState(region);
		 if(init != null && init.getOutgoings().size() == 1){
			 tc.add(init);
			 Transition t = init.getOutgoings().get(0);
			 tc.add(t);
			 if(t.getTriggers() != null && t.getTriggers().size() > 0){
				 tc.addAll(t.getTriggers());
			 }
			 if(t.getGuard() != null){
				 tc.add(t.getGuard());	
			 }
			 tc.add(t.getTarget());
			 return t.getTarget();
		 }
		 return null;
	 }
	 

	 
	 
	 public static EList<Transition> getAllOutGoingTransition(State state){
		 return state.getOutgoings();
	 }
	 
	 public static Transition getOneOfOutGoingTransition(State state, Map<Transition, Integer> map, int aslgo){
		 return null;
	 }
}
