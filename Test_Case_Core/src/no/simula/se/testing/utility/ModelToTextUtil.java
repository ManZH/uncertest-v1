/*******************************************************************************
 * Copyright (c) 2016, 2017 Simula Research Laboratory. 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Simula license 
 * which accompanies this distribution, and is available at LICENSE.TXT
 *  
 *  Contributors:
 *        Man Zhang
 *******************************************************************************/
package no.simula.se.testing.utility;

import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.ocl.ecore.Constraint;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.InstanceSpecification;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.uml2.uml.Parameter;
import org.eclipse.uml2.uml.State;
import org.eclipse.uml2.uml.Transition;

public class ModelToTextUtil {
	
	public final static String PRE_COMMENT_SIMPLE="// ";
	public final static String COMMENT_COLON = " : ";
	
	public String generatePreComment(EObject object){
		String text = PRE_COMMENT_SIMPLE + object.eClass().getName() + COMMENT_COLON;
		
		if(object instanceof State){
			
		}else if(object instanceof Transition){
			
		}else if(object instanceof Constraint){
			
		}else if(object instanceof Operation){
			Operation op = (Operation)object;
			text = op.getName();
			if(op.parameterableElements().size() != 0){
				text = text + "parms" + COMMENT_COLON;
				for(Parameter parm :op.getOwnedParameters()){
					text = text + "(" + parm.getDirection().getLiteral() +","+ parm.getName() + "," +parm.getType() +")";
//					switch(parm.getDirection().getValue()){
//					case ParameterDirectionKind.IN: break;
//					case ParameterDirectionKind.INOUT: break;
//					case ParameterDirectionKind.OUT: break;
//					case ParameterDirectionKind.RETURN: break;
//					}
				}
			}
			
		}
		return text;
		 
	}
	
	public List<String> generateCode(InstanceSpecification instance, Model uml_Model){
		return null;//FIXME
	}
	
	public String generateDeclaration(Classifier clazz, String var_name, int i){
		return generateDeclaration(clazz.getName(), var_name, i);
	}
	
	public String generateDeclaration(Classifier clazz, String var_name){
		return generateDeclaration(clazz, var_name, 0);
	}
	public String generateDeclaration(Classifier clazz, String var_name, Classifier clazzImp, int i){
		return generateDeclaration(clazz.getName(), var_name, clazzImp.getName(), i, false);
	}
	
	public String generateDeclaration(Classifier clazz, String var_name, Classifier clazzImp){
		return generateDeclaration(clazz, var_name, clazzImp, 0);
	}
	
	public String generateDeclaration(Classifier clazz, String var_name, Model umlModel, int i){
		return generateDeclaration(clazz.getName(), var_name, umlModel.getName(), i, true);
	}
	public String generateDeclaration(Classifier clazz, String var_name, Model umlModel){
		return generateDeclaration(clazz.getName(), var_name, umlModel.getName(), 0, true);
	}
	
	public String generateDeclaration(String clazz, String var_name, int i){
		return generateDeclaration(clazz, var_name, clazz, i, false);
	}
	
	public String generateDeclaration(String clazz, String var_name, String clazzImp, int i, boolean isEMF){
		String text = "";
		switch(i){
		case 0: break;
		case 1: text = text +"private ";break;
		case 2: text = text +"public ";break;
		case 3: text = text +"protected ";break;
		}
		text = text  + clazz + " " + var_name + " = ";
		if(isEMF){
			text = text + " " + clazzImp+"Factorye.INSTANCE.create" +clazz+"();";
		}else{
			text = text + "new "+ clazzImp +"();";
		}
		return text;
	}
}
