/*******************************************************************************
 * Copyright (c) 2016, 2017 Simula Research Laboratory. 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Simula license 
 * which accompanies this distribution, and is available at LICENSE.TXT
 *  
 *  Contributors:
 *        Man Zhang
 *******************************************************************************/
package no.simula.se.testing.utility;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.ocl.OCL;
import org.eclipse.ocl.ParserException;
import org.eclipse.ocl.Query;
import org.eclipse.ocl.ecore.Constraint;
import org.eclipse.ocl.ecore.EcoreEnvironmentFactory;
import org.eclipse.ocl.expressions.OCLExpression;
import org.eclipse.ocl.helper.OCLHelper;
import org.eclipse.ocl.uml.UMLEnvironmentFactory;
import org.eclipse.uml2.uml.CallOperationAction;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.EnumerationLiteral;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.Parameter;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.SendSignalAction;
import org.eclipse.uml2.uml.State;
import org.eclipse.uml2.uml.Transition;

public final class MOCLUtil {
	
	public static MOCLUtil instance;
	public static MOCLUtil getInstance(){
		if(instance == null)
			instance = new MOCLUtil();
		return instance;
	}

	public boolean validateOCL(String cons){
		OCLExpression<Classifier> query = null;
		try {
		    // create an OCL instance for UML
		    OCL<Package, Classifier, Operation, Property, EnumerationLiteral, Parameter, State, CallOperationAction, SendSignalAction, org.eclipse.uml2.uml.Constraint, org.eclipse.uml2.uml.Class, EObject> ocl;
		    ocl =  OCL.newInstance(new UMLEnvironmentFactory());
		    
		    // create an OCL helper object
		    OCLHelper<Classifier, Operation, Property, org.eclipse.uml2.uml.Constraint> helper = ocl.createOCLHelper();
		    
		    // set the OCL context classifier
		    //helper.setContext(EXTLibraryPackage.Literals.WRITER);
		    //helper.setContext(e2);
		    //query = helper.createQuery("self.books->collect(b : Book | b.category)->asSet()");
		    query = helper.createQuery(cons);
		    return true;
		} catch (ParserException e) {
		    // record failure to parse
		    System.err.println(e.getLocalizedMessage());
		}
		return false;
	}
	public boolean validateOCL(Classifier e2, Operation op, String cons){
		OCLExpression<Classifier> query = null;
		try {
		    // create an OCL instance for UML
		    OCL<Package, Classifier, Operation, Property, EnumerationLiteral, Parameter, State, CallOperationAction, SendSignalAction, org.eclipse.uml2.uml.Constraint, org.eclipse.uml2.uml.Class, EObject> ocl;
		    ocl =  OCL.newInstance(new UMLEnvironmentFactory());
		    
		    // create an OCL helper object
		    OCLHelper<Classifier, Operation, Property, org.eclipse.uml2.uml.Constraint> helper = ocl.createOCLHelper();
		    
		    helper.setOperationContext(e2, op);
		    //query = helper.createQuery("self.books->collect(b : Book | b.category)->asSet()");
		    query = helper.createQuery(cons);
		    return true;
		} catch (ParserException e) {
		    // record failure to parse
		    System.err.println(e.getLocalizedMessage());
		}
		return false;
	}
	
	public boolean validateOCL(Classifier e2, String cons){
		OCLExpression<Classifier> query = null;
		try {
		    // create an OCL instance for UML
		    OCL<Package, Classifier, Operation, Property, EnumerationLiteral, Parameter, State, CallOperationAction, SendSignalAction, org.eclipse.uml2.uml.Constraint, org.eclipse.uml2.uml.Class, EObject> ocl;
		    ocl =  OCL.newInstance(new UMLEnvironmentFactory());
		    
		    // create an OCL helper object
		    OCLHelper<Classifier, Operation, Property, org.eclipse.uml2.uml.Constraint> helper = ocl.createOCLHelper();
		    
		    // set the OCL context classifier
		    //helper.setContext(EXTLibraryPackage.Literals.WRITER);
		    helper.setContext(e2);
		    //query = helper.createQuery("self.books->collect(b : Book | b.category)->asSet()");
		    query = helper.createQuery(cons);
		    return true;
		} catch (ParserException e) {
		    // record failure to parse
		    System.err.println(e.getLocalizedMessage());
		}
		return false;
	}
	
	public boolean evaluateOCL(Classifier e2, String cons, EObject obj){
		OCLExpression<Classifier> query = null;
		Query<Classifier, Class, EObject> eval = null;
		try {
		    // create an OCL instance for UML
		    OCL<Package, Classifier, Operation, Property, EnumerationLiteral, Parameter, State, CallOperationAction, SendSignalAction, org.eclipse.uml2.uml.Constraint, org.eclipse.uml2.uml.Class, EObject> ocl;
		    ocl =  OCL.newInstance(new UMLEnvironmentFactory());
		    
		    // create an OCL helper object
		    OCLHelper<Classifier, Operation, Property, org.eclipse.uml2.uml.Constraint> helper = ocl.createOCLHelper();
		    
		    // set the OCL context classifier
		    helper.setContext(e2);
		    query = helper.createQuery(cons);
		    eval = ocl.createQuery(query);
		    return eval.check(obj);
		} catch (ParserException e) {
		    // record failure to parse
		    System.err.println(e.getLocalizedMessage());
		}
		return false;
	}
	
	public boolean evaluateOCL(EClassifier e2, String cons, EObject obj){
		boolean result = false;
		OCLExpression<EClassifier> query = null;
		try {
			// create an OCL instance for Ecore
			OCL<?, EClassifier, ?, ?, ?, ?, ?, ?, ?, Constraint, EClass, EObject> ocl;
			ocl = OCL.newInstance(EcoreEnvironmentFactory.INSTANCE);
			// create an OCL helper object
			OCLHelper<EClassifier, ?, ?, Constraint> helper = ocl.createOCLHelper();
			// set the OCL context classifier
			helper.setContext(e2);
			query = helper.createQuery(cons);
			Query<EClassifier, EClass, EObject> eval = ocl.createQuery(query);
			result = eval.check(obj);
			} catch (ParserException e) {
				System.err.println(e.getLocalizedMessage());
			}
		return result;
	}
	
	public static void rebuildContext(EObject eObject, org.eclipse.uml2.uml.Constraint constraint){
		if(eObject instanceof State){
			if(!(constraint.getContext() instanceof org.eclipse.uml2.uml.Class)){
				org.eclipse.uml2.uml.Class clazz_ = getClassifierOwner(constraint.getContext());
				constraint.setContext(clazz_);
			}
		}else if(eObject instanceof Transition){
			if(!(constraint.getContext() instanceof org.eclipse.uml2.uml.Class)){
				org.eclipse.uml2.uml.Class clazz_ = getClassifierOwner(constraint.getContext());
				constraint.setContext(clazz_);
			}
		}
	}
	
	public static org.eclipse.uml2.uml.Class getClassifierOwner(EObject object){
		if((((Element)object).getOwner() instanceof org.eclipse.uml2.uml.Class) && !(((Element)object).getOwner() instanceof org.eclipse.uml2.uml.StateMachine)){
			return (org.eclipse.uml2.uml.Class)((Element)object).getOwner();
		}
		return getClassifierOwner(((Element)object).getOwner());
	}

	
}
