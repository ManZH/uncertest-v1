/*******************************************************************************
 * Copyright (c) 2016, 2017 Simula Research Laboratory. 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Simula license 
 * which accompanies this distribution, and is available at LICENSE.TXT
 *  
 *  Contributors:
 *        Man Zhang
 *******************************************************************************/

package no.simula.se.testing.utility;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.URIConverter;
import org.eclipse.emf.ecore.resource.impl.ExtensibleURIConverterImpl;

public class CustomURIConverter extends ExtensibleURIConverterImpl implements
        URIConverter {
    @Override
    public InputStream createInputStream(URI uri, Map<?, ?> options)
            throws IOException {
        System.out.println("Loading " + uri);

        return super.createInputStream(uri, options);
    }
}
