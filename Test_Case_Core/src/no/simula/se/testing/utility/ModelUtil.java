/*******************************************************************************
 * Copyright (c) 2016, 2017 Simula Research Laboratory. 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Simula license 
 * which accompanies this distribution, and is available at LICENSE.TXT
 *  
 *  Contributors:
 *        Man Zhang
 *******************************************************************************/
package no.simula.se.testing.utility;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.eclipse.core.resources.IResource;
import org.eclipse.emf.codegen.ecore.generator.Generator;
import org.eclipse.emf.codegen.ecore.generator.GeneratorAdapterFactory;
import org.eclipse.emf.codegen.ecore.genmodel.GenJDKLevel;
import org.eclipse.emf.codegen.ecore.genmodel.GenModel;
import org.eclipse.emf.codegen.ecore.genmodel.GenModelFactory;
import org.eclipse.emf.codegen.ecore.genmodel.GenModelPackage;
import org.eclipse.emf.codegen.ecore.genmodel.GenPackage;
import org.eclipse.emf.codegen.ecore.genmodel.generator.GenBaseGeneratorAdapter;
import org.eclipse.emf.codegen.ecore.genmodel.generator.GenModelGeneratorAdapterFactory;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.BasicMonitor;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.xmi.XMIResource;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.emf.ecore.xmi.impl.EcoreResourceFactoryImpl;
import org.eclipse.emf.ecore.xmi.impl.URIHandlerImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.uml2.common.util.UML2Util.EObjectMatcher;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Constraint;
import org.eclipse.uml2.uml.InstanceSpecification;
import org.eclipse.uml2.uml.LiteralBoolean;
import org.eclipse.uml2.uml.LiteralInteger;
import org.eclipse.uml2.uml.LiteralNull;
import org.eclipse.uml2.uml.LiteralReal;
import org.eclipse.uml2.uml.LiteralSpecification;
import org.eclipse.uml2.uml.LiteralString;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.OpaqueExpression;
import org.eclipse.uml2.uml.Operation;
import org.eclipse.uml2.uml.PackageImport;
import org.eclipse.uml2.uml.Parameter;
import org.eclipse.uml2.uml.PrimitiveType;
import org.eclipse.uml2.uml.Profile;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.StateMachine;
import org.eclipse.uml2.uml.Stereotype;
import org.eclipse.uml2.uml.UMLPackage;
import org.eclipse.uml2.uml.resources.util.UMLResourcesUtil;
import org.eclipse.uml2.uml.util.UMLUtil;
import org.eclipse.uml2.uml.util.UMLUtil.UML2EcoreConverter;

import no.simula.se.testmodel.TestModel.UTestItem;
import no.simula.se.testmodel.TestModel.UTestModel;
import no.simula.se.testmodel.TestModel.UTestModelPackage;
import no.simula.se.testmodel.TestModel.UTestSet;

public final class ModelUtil {

	public static ModelUtil util;

	public static ModelUtil getInstance() {
		if (util == null) {
			util = new ModelUtil();
		}
		return util;
	}

	public static String getBodyOfOperationExpression(OpaqueExpression oe) {// FIXME
																			// for
																			// the
																			// more
																			// than
																			// one
																			// op
		String str = "";
		for (String s : oe.getBodies()) {
			str = str + s;
		}
		return str;
	}

	public static int getBodyOfLiteralInteger(LiteralInteger oe) {// FIXME for
																	// the more
																	// than one
																	// op
		return oe.getValue();

	}

	public static boolean getBodyOfLiteralBoolean(LiteralBoolean oe) {// FIXME
																		// for
																		// the
																		// more
																		// than
																		// one
																		// op
		return oe.isValue();

	}
	
	public static String getStrOperationExpression(OpaqueExpression oe) {// FIXME
		String str = "";
		for (String s : oe.getBodies()) {
			str = str + s;
			}
			return "\""+str+"\"";
			}
	
	public static String getStrLiteralSpecification(LiteralSpecification vs){
		if(vs instanceof LiteralBoolean){
			return String.valueOf(((LiteralBoolean)vs).isValue());
		}else if(vs instanceof LiteralInteger){
			return String.valueOf(((LiteralInteger)vs).getValue());
		}else if(vs instanceof LiteralNull){
			return "null";
		}else if(vs instanceof LiteralReal){
			return String.valueOf(((LiteralReal)vs).getValue());
		}else if(vs instanceof LiteralString){
			return "\""+String.valueOf(((LiteralBoolean)vs).isValue())+"\"";
		}else {
			return "";
		}
	}

	public static org.eclipse.uml2.uml.Package loadPackage(ResourceSet rs, String url) {

		Resource resource = rs.getResource(createFileURI(url), true);
		System.out.println("Root objects count: " + resource.getContents().size());
		org.eclipse.uml2.uml.Package uml = (org.eclipse.uml2.uml.Package) EcoreUtil
				.getObjectByType(resource.getContents(), UMLPackage.Literals.PACKAGE);
		return uml;
	}

	public static EObject loadUTestModel(String url) throws IOException {
		UTestModelPackage.eINSTANCE.eClass();
		Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap().put(Resource.Factory.Registry.DEFAULT_EXTENSION,
				new XMIResourceFactoryImpl());

		ResourceSet rs = new ResourceSetImpl();
		rs.getPackageRegistry().put(org.eclipse.uml2.uml.UMLPackage.eINSTANCE.getNsURI(),
				org.eclipse.uml2.uml.UMLPackage.eINSTANCE);
		rs.getPackageRegistry().put("http://www.eclipse.org/uml2/3.0.0/UML", org.eclipse.uml2.uml.UMLPackage.eINSTANCE);
		rs.setURIConverter(new CustomURIConverter());
		Resource resource = rs.getResource(createFileURI(url), true);
		EcoreUtil.resolveAll(rs);
		for (Resource r : rs.getResources()) {
			EcoreUtil.resolve(r.getContents().get(0), r);
		}
		return resource.getContents().get(0);
	}

	public static EObject[] loadUTestModels(String uUrl, String tUrl) {
		EObject[] models = new EObject[2];
		ResourceSet rs = new ResourceSetImpl();
		rs.setURIConverter(new CustomURIConverter());
		UMLResourcesUtil.init(rs);

		Resource resource = rs.getResource(createFileURI(uUrl), true);
		EObject umlModel = (EObject) EcoreUtil.getObjectByType(resource.getContents(), UMLPackage.Literals.PACKAGE);
		models[0] = umlModel;
		UTestModelPackage.eINSTANCE.eClass();
		Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap().put(Resource.Factory.Registry.DEFAULT_EXTENSION,
				new XMIResourceFactoryImpl());

		Resource res = rs.getResource(createFileURI(tUrl), true);
		models[1] = res.getContents().get(0);
		EcoreUtil.resolveAll(rs);

		return models;
	}
	
	public static EObject[] loadUTestModels(ResourceSet rs, String uUrl, String uUrl2, String tUrl) {
		EObject[] models = new EObject[3];
		rs.setURIConverter(new CustomURIConverter());
		UMLResourcesUtil.init(rs);

		Resource resource = rs.getResource(createFileURI(uUrl), true);
		EObject umlModel = (EObject) EcoreUtil.getObjectByType(resource.getContents(), UMLPackage.Literals.PACKAGE);
		models[0] = umlModel;

		Resource resource2 = rs.getResource(createFileURI(uUrl2), true);
		EObject umlModel2 = (EObject) EcoreUtil.getObjectByType(resource2.getContents(), UMLPackage.Literals.PACKAGE);
		models[1] = umlModel2;

		UTestModelPackage.eINSTANCE.eClass();
		Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap().put(Resource.Factory.Registry.DEFAULT_EXTENSION,
				new XMIResourceFactoryImpl());

		Resource res = rs.getResource(createFileURI(tUrl), true);
		models[2] = res.getContents().get(0);
		EcoreUtil.resolveAll(rs);

		return models;
	}
	
	public static EObject loadEMFModel(ResourceSet rs, String turl) {
		EObject loaded = null;
		rs.setURIConverter(new CustomURIConverter());
		UMLResourcesUtil.init(rs);

		UTestModelPackage.eINSTANCE.eClass();
		Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap().put(Resource.Factory.Registry.DEFAULT_EXTENSION,
				new XMIResourceFactoryImpl());

		Resource res = rs.getResource(createFileURI(turl), true);
		loaded = res.getContents().get(0);
		EcoreUtil.resolveAll(rs);


		return loaded;
	}
	
	public static EObject loadUMLModel(ResourceSet rs, String uurl) {
		EObject loaded = null;
		rs.setURIConverter(new CustomURIConverter());
		UMLResourcesUtil.init(rs);

		Resource resource = rs.getResource(createFileURI(uurl), true);
		EObject umlModel = (EObject) EcoreUtil.getObjectByType(resource.getContents(), UMLPackage.Literals.PACKAGE);
		Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap().put(Resource.Factory.Registry.DEFAULT_EXTENSION,
				new XMIResourceFactoryImpl());
		loaded = umlModel;
		EcoreUtil.resolveAll(rs);


		return loaded;
	}

	public static EObject[] loadUTestModels(String uUrl, String uUrl2, String tUrl) {
		EObject[] models = new EObject[3];
		ResourceSet rs = new ResourceSetImpl();
		rs.setURIConverter(new CustomURIConverter());
		UMLResourcesUtil.init(rs);

		Resource resource = rs.getResource(createFileURI(uUrl), true);
		EObject umlModel = (EObject) EcoreUtil.getObjectByType(resource.getContents(), UMLPackage.Literals.PACKAGE);
		models[0] = umlModel;

		Resource resource2 = rs.getResource(createFileURI(uUrl2), true);
		EObject umlModel2 = (EObject) EcoreUtil.getObjectByType(resource2.getContents(), UMLPackage.Literals.PACKAGE);
		models[1] = umlModel2;

		UTestModelPackage.eINSTANCE.eClass();
		Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap().put(Resource.Factory.Registry.DEFAULT_EXTENSION,
				new XMIResourceFactoryImpl());

		Resource res = rs.getResource(createFileURI(tUrl), true);
		models[2] = res.getContents().get(0);
		EcoreUtil.resolveAll(rs);

		return models;
	}

	public static org.eclipse.uml2.uml.Package loadPackage(String url) {
		ResourceSet rs = new ResourceSetImpl();
		rs.setURIConverter(new CustomURIConverter());
		UMLResourcesUtil.init(rs);

		Resource resource = rs.getResource(createFileURI(url), true);
		System.out.println("Root objects count: " + resource.getContents().size());
		org.eclipse.uml2.uml.Package uml = (org.eclipse.uml2.uml.Package) EcoreUtil
				.getObjectByType(resource.getContents(), UMLPackage.Literals.PACKAGE);
		return uml;
	}

	public static Model load(String url) {
		ResourceSet rs = new ResourceSetImpl();
		rs.setURIConverter(new CustomURIConverter());
		UMLResourcesUtil.init(rs);

		Resource resource = rs.getResource(createFileURI(url), true);
		System.out.println("Root objects count: " + resource.getContents().size());
		System.out.println("Root objects:");
		for (EObject obj : resource.getContents()) {
			System.out.println(" " + obj);
		}
		Model uml = (Model) EcoreUtil.getObjectByType(resource.getContents(), UMLPackage.eINSTANCE.getModel());
		return uml;
	}

	public static org.eclipse.uml2.uml.Package preDealUMLModel(org.eclipse.uml2.uml.Package model, String... packs) {
		for (String str : packs) {
			if (getPackageByName(model.getImportedPackages(), str) != null) {
				org.eclipse.uml2.uml.Package pack = getPackageByName(model.getImportedPackages(), str);
				preDealUMLModel(pack);
			}
		}

		preDealUMLModel(model);
		saveAll(model);
		return null;
	}

	public static org.eclipse.uml2.uml.Package getPackageByName(EList<org.eclipse.uml2.uml.Package> eList,
			String name) {
		for (org.eclipse.uml2.uml.Package p : eList) {
			if (p.getName().equals(name))
				;
			return p;
		}
		return null;
	}

	public static EList<org.eclipse.uml2.uml.Package> getPackagesByName(EList<org.eclipse.uml2.uml.Package> eList,
			String name, boolean isComplete) {// FIXME
		EList<org.eclipse.uml2.uml.Package> list = new BasicEList<org.eclipse.uml2.uml.Package>();
		for (org.eclipse.uml2.uml.Package p : eList) {
			if (p.getName().startsWith(name) || p.getName().equals(name))
				list.add(p);
		}
		return list;
	}

	public static EList<org.eclipse.uml2.uml.PackageImport> getPackageImportsByName(org.eclipse.uml2.uml.Package model,
			String name, boolean isComplete) {// FIXME
		EList<org.eclipse.uml2.uml.PackageImport> list = new BasicEList<org.eclipse.uml2.uml.PackageImport>();
		for (PackageImport i : model.getPackageImports()) {
			if (i.getImportedPackage().getName().startsWith(name) || i.getImportedPackage().getName().equals(name))
				list.add(i);
		}
		return list;
	}

	public static org.eclipse.uml2.uml.Package preDealUMLModel(org.eclipse.uml2.uml.Package model) {
		PrimitiveType real = (PrimitiveType) model.getMember("Real");
		for (EObject object : model.allOwnedElements()) {
			if (object instanceof Classifier) {
				Classifier clazz = (Classifier) object;
				for (Property p : clazz.allAttributes()) {
					if (p.getType() == null)
						System.err.println("the type is not set for " + p.getName() + " of " + clazz.getName());
					if (!org.eclipse.uml2.uml.util.UMLUtil.isReal(p.getType())
							&& p.getType().getName().equals("Real")) {
						p.setType(real);
					}
				}
			} else if (object instanceof Operation) {
				Operation op = (Operation) object;
				for (Parameter p : op.getOwnedParameters()) {
					if (p.getType() == null)
						System.err.println("the type is not set for " + p.getName() + " of " + op.getName());
					if (!org.eclipse.uml2.uml.util.UMLUtil.isReal(p.getType())
							&& p.getType().getName().equals("Real")) {
						p.setType(real);
					}
				}
			}
		}
		EList<org.eclipse.uml2.uml.PackageImport> marte = getPackageImportsByName(model, "MARTE", true);
		if (marte.size() != 0) {
			for (PackageImport pi : marte) {
				EcoreUtil.delete(pi);
			}
			model.getPackageImports().removeAll(marte);
		}

		return null;
	}

	public static void save(ResourceSet resSet, UTestItem object) {
		Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;
		Map<String, Object> m = reg.getExtensionToFactoryMap();
		m.put("testmodel", new XMIResourceFactoryImpl());
		Resource resource = resSet.createResource(URI.createURI("testcase/My2.testmodel"));
		System.out.println(resource);
		resource.getContents().add(object);
		try {
			resource.save(Collections.EMPTY_MAP);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void save(ResourceSet resSet, EObject object, String name) {
		save(resSet, object, "testcase");
	}

	public static String save(ResourceSet resSet, EObject object, String name, String path) {
		Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;
		Map<String, Object> m = reg.getExtensionToFactoryMap();
		m.put("utestmodel", new XMIResourceFactoryImpl());
		Resource resource = resSet.createResource(URI.createURI(path + "/" + name + ".utestmodel"));
		// System.out.println(resource);
		resource.getContents().add(object);

		try {
			resource.save(Collections.EMPTY_MAP);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return resource.getURI().path();
	}

	public static String save(ResourceSet resSet, EObject object, String name, String path, boolean isRelative) {
		if (isRelative) {
			Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;
			Map<String, Object> m = reg.getExtensionToFactoryMap();
			m.put("utestmodel", new XMIResourceFactoryImpl());
			Resource resource = resSet.createResource(URI.createURI(path + "/" + name + ".utestmodel"));
			// System.out.println(resource);
			resource.getContents().add(object);
			Map<String, Object> options = new HashMap<String, Object>();
			options.put(XMIResource.OPTION_ENCODING, "UTF-8");
			options.put(XMLResource.OPTION_URI_HANDLER, new URIHandlerImpl.AbsoluteCrossBundleAware() {
				@Override
				public URI deresolve(final URI uri) {
					final URI uri2 = URI.createURI(uri.toString().split("/")[uri.toString().split("/").length - 1]);
					return super.deresolve(uri2);
				}
			});
			try {
				resource.save(options);
			} catch (IOException e) {
				e.printStackTrace();
			}
			return resource.getURI().path();
		}
		return save(resSet, object, name, path);

	}

	public static void save(EObject object, IResource file) {

		ResourceSet resourceSet = new ResourceSetImpl();
		Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;
		Map<String, Object> m = reg.getExtensionToFactoryMap();
		m.put("utestmodel", new XMIResourceFactoryImpl());
		Resource resource = resourceSet
				.createResource(URI.createPlatformResourceURI(file.getLocationURI().toString(), true));
		Map<String, String> options = new HashMap<String, String>();
		options.put(XMIResource.OPTION_ENCODING, "UTF-8");
		resource.getContents().add(object); // add current model to resource
		try {
			resource.save(options);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	//
	// public static void save(ResourceSet resSet, UTestModel object, String
	// name){
	// Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;
	// Map<String, Object> m = reg.getExtensionToFactoryMap();
	// m.put("utestmodel", new XMIResourceFactoryImpl());
	// Resource resource = resSet.createResource(URI
	// .createURI("testcase/"+name+".utestmodel"));
	// //System.out.println(resource);
	// resource.getContents().add(object);
	// try {
	// resource.save(Collections.EMPTY_MAP);
	// } catch (IOException e) {
	// e.printStackTrace();
	// }
	// }

	public static void save(org.eclipse.uml2.uml.Package model) {
		try {
			model.eResource().save(Collections.EMPTY_MAP);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void save(UTestModel model, String path, String name,boolean isRelative) {
		try {
			if (isRelative) {
				Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;
				Map<String, Object> m = reg.getExtensionToFactoryMap();
				m.put("utestmodel", new XMIResourceFactoryImpl());
				Resource resource = model.eResource().getResourceSet().createResource(URI.createURI(path + "/" + name + ".utestmodel"));
				// System.out.println(resource);
				resource.getContents().add(model);
				
				Map<String, Object> options = new HashMap<String, Object>();
				options.put(XMIResource.OPTION_ENCODING, "UTF-8");
				options.put(XMLResource.OPTION_URI_HANDLER, new URIHandlerImpl.AbsoluteCrossBundleAware() {
					@Override
					public URI deresolve(final URI uri) {
						final URI uri2 = URI.createURI(uri.toString().split("/")[uri.toString().split("/").length - 1]);
						return super.deresolve(uri2);
					}
				});
				resource.save(options);
			} else
				model.eResource().save(Collections.EMPTY_MAP);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void saveAll(org.eclipse.uml2.uml.Package model) {
		try {
			for (Resource r : model.eResource().getResourceSet().getResources()) {
				r.save(null);
			}
			// model.eResource().save(Collections.EMPTY_MAP);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static URI createFileURI(String relativePath) {
		return URI.createFileURI(new File(relativePath).getAbsolutePath());
	}

	public static Stereotype findStereotype(Model uml, String profile_name, String stereotype_name) {
		for (Profile profile : uml.getAllAppliedProfiles()) {
			if (profile.getName().equals(profile_name)) {
				for (Stereotype st : profile.getOwnedStereotypes()) {
					if (st.getName().equals(stereotype_name)) {
						return st;
					}
				}
			}
		}
		return null;
	}

	public static Resource umlToEcore(Model model, String url) {
		Collection<EPackage> result = UMLUtil.convertToEcore(model, null);
		ResourceSet resourceSet = new ResourceSetImpl();
		resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap()
				.put(Resource.Factory.Registry.DEFAULT_EXTENSION, new EcoreResourceFactoryImpl());
		URI fileURI = createFileURI(url);
		Resource resource = resourceSet.createResource(fileURI);
		resource.getContents().addAll(result);
		try {
			resource.save(Collections.EMPTY_MAP); // the map can pass special
													// saving options to the
													// operation
		} catch (IOException e) {
			/* error handling */

		}
		return resource;
	}

	public static Map<String, String> initUML2EcoreOptionsToProcess() {
		final Map<String, String> options = new HashMap<String, String>();
		options.put(UML2EcoreConverter.OPTION__ECORE_TAGGED_VALUES, UMLUtil.OPTION__PROCESS);
		options.put(UML2EcoreConverter.OPTION__REDEFINING_OPERATIONS, UMLUtil.OPTION__PROCESS);
		options.put(UML2EcoreConverter.OPTION__REDEFINING_PROPERTIES, UMLUtil.OPTION__PROCESS);
		options.put(UML2EcoreConverter.OPTION__SUBSETTING_PROPERTIES, UMLUtil.OPTION__PROCESS);
		options.put(UML2EcoreConverter.OPTION__UNION_PROPERTIES, UMLUtil.OPTION__PROCESS);
		options.put(UML2EcoreConverter.OPTION__DERIVED_FEATURES, UMLUtil.OPTION__PROCESS);
		options.put(UML2EcoreConverter.OPTION__DUPLICATE_OPERATIONS, UMLUtil.OPTION__PROCESS);
		options.put(UML2EcoreConverter.OPTION__DUPLICATE_OPERATION_INHERITANCE, UMLUtil.OPTION__PROCESS);
		options.put(UML2EcoreConverter.OPTION__DUPLICATE_FEATURES, UMLUtil.OPTION__PROCESS);
		options.put(UML2EcoreConverter.OPTION__DUPLICATE_FEATURE_INHERITANCE, UMLUtil.OPTION__PROCESS);
		options.put(UML2EcoreConverter.OPTION__SUPER_CLASS_ORDER, UMLUtil.OPTION__PROCESS);
		options.put(UML2EcoreConverter.OPTION__ANNOTATION_DETAILS, UMLUtil.OPTION__PROCESS);
		options.put(UML2EcoreConverter.OPTION__INVARIANT_CONSTRAINTS, UMLUtil.OPTION__PROCESS);
		options.put(UML2EcoreConverter.OPTION__OPERATION_BODIES, UMLUtil.OPTION__PROCESS);
		options.put(UML2EcoreConverter.OPTION__COMMENTS, UMLUtil.OPTION__PROCESS);
		options.put(UML2EcoreConverter.OPTION__CAMEL_CASE_NAMES, UMLUtil.OPTION__IGNORE);
		return options;
	}

	public static Resource umlToEcore(org.eclipse.uml2.uml.Package umlModel, boolean isOn) {
		// Collection<EPackage> result = UMLUtil.convertToEcore(model, null);
		// ResourceSet resourceSet = model.eResource().getResourceSet();
		// resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap()
		// .put(Resource.Factory.Registry.DEFAULT_EXTENSION, new
		// EcoreResourceFactoryImpl());
		// URI fileURI =
		// resourceSet.getURIConverter().normalize(model.eResource().getURI()).trimFileExtension()
		// .trimSegments(1);

		Map<String, String> options = initUML2EcoreOptionsToProcess();
		Collection<EPackage> ecorePackages = UMLUtil.convertToEcore(umlModel, options);
		Resource umlModelResource = umlModel.eResource();
		ResourceSet resourceSet = umlModelResource.getResourceSet();
		resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap()
				.put(Resource.Factory.Registry.DEFAULT_EXTENSION, new EcoreResourceFactoryImpl());
		URI fileURI = resourceSet.getURIConverter().normalize(umlModelResource.getURI()).trimFileExtension()
				.trimSegments(1);

		Resource resource = resourceSet
				.createResource(fileURI.appendSegment(umlModel.getName()).appendFileExtension("ecore"));
		resource.getContents().addAll(ecorePackages);
		try {
			resource.save(Collections.EMPTY_MAP); // the map can pass special
													// saving options to the
													// operation
		} catch (IOException e) {
			e.printStackTrace();

		}
		return resource;
	}

	public static Resource umlToEcore(org.eclipse.uml2.uml.Package umlModel) {

		Map<String, String> options = initUML2EcoreOptionsToProcess();
		Collection<EPackage> ecorePackages = UMLUtil.convertToEcore(umlModel, options);
		Resource umlModelResource = umlModel.eResource();
		ResourceSet resourceSet = umlModelResource.getResourceSet();
		resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap()
				.put(Resource.Factory.Registry.DEFAULT_EXTENSION, new EcoreResourceFactoryImpl());
		URI uri = resourceSet.getURIConverter().normalize(umlModelResource.getURI()).trimFileExtension()
				.trimSegments(1);
		List<Resource> resources = new ArrayList<Resource>();
		Resource resource = null;
		for (final EPackage ePackage : ecorePackages) {
			String name = ePackage.getName();
			if (name.equals("types")) {
				name = umlModel.getName() + "_" + name;
			}
			ePackage.setName(name);
			resource = resourceSet.createResource(uri.appendSegment(name).appendFileExtension("ecore"));

			resources.add(resource);
			resource.getContents().add(ePackage);
		}

		for (final Resource r : resources) {
			try {
				r.save(null);
			} catch (final Exception e) {
				e.printStackTrace();
			}
		}
		return resources.get(0);// only return the ecore resource not type one
	}

	public static List<Resource> umlToEcore2(org.eclipse.uml2.uml.Package umlModel) {

		Map<String, String> options = initUML2EcoreOptionsToProcess();
		Collection<EPackage> ecorePackages = UMLUtil.convertToEcore(umlModel, options);
		Resource umlModelResource = umlModel.eResource();
		ResourceSet resourceSet = umlModelResource.getResourceSet();
		resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap()
				.put(Resource.Factory.Registry.DEFAULT_EXTENSION, new EcoreResourceFactoryImpl());
		URI uri = resourceSet.getURIConverter().normalize(umlModelResource.getURI()).trimFileExtension()
				.trimSegments(1);
		List<Resource> resources = new ArrayList<Resource>();
		Resource resource = null;
		for (final EPackage ePackage : ecorePackages) {
			String name = ePackage.getName();
			if (name.equals("types")) {
				name = umlModel.getName() + "_" + name;
			}
			ePackage.setName(name);
			resource = resourceSet.createResource(uri.appendSegment(name).appendFileExtension("ecore"));

			resources.add(resource);
			resource.getContents().add(ePackage);
		}

		for (final Resource r : resources) {
			try {
				r.save(null);
			} catch (final Exception e) {
				e.printStackTrace();
			}
		}
		return resources;// only return the ecore resource not type one
	}

	public static GenModel ecoreToGenModel(Resource resource) {
		EPackage pack = (EPackage) resource.getContents().get(0);
		GenModel genModel = GenModelFactory.eINSTANCE.createGenModel();
		genModel.setComplianceLevel(GenJDKLevel.JDK80_LITERAL);
		genModel.setModelDirectory("/" + pack.getName());
		genModel.setModelName(pack.getName());
		genModel.setRootExtendsInterface("org.eclipse.emf.ecore.EObject");
		genModel.getForeignModel().add(resource.getURI().toString());

		// genModel.initialize(Collections.singleton(pack));
		// GenPackage gPackage = genModel.getGenPackages().get(0);
		// gPackage.setBasePackage("no.simula.se.testing.generted");

		for (EObject obj : resource.getContents()) {
			if (obj != null && obj instanceof EPackage) {
				EPackage ePackage = (EPackage) obj;
				String name = "";
				// if(ePackage.getName().equals("types")){
				// name = pack.getName()+"_types";
				// }else{
				//
				// }
				name = ePackage.getName();
				GenPackage genPackage = genModel.createGenPackage();
				genModel.getGenPackages().add(genPackage);
				genPackage.setDisposableProviderFactory(true);
				genPackage.setEcorePackage(ePackage);
				genPackage.setPrefix(name);
				genPackage.setBasePackage("no.simula.se.testing.generted");
			}
		}

		try {
			URI genModelURI = resource.getResourceSet().getURIConverter().normalize(resource.getURI())
					.trimFileExtension().trimSegments(1);
			Resource genModelResource = resource.getResourceSet()
					.createResource(genModelURI.appendSegment(pack.getName()).appendFileExtension("genmodel"));
			genModelResource.getContents().add(genModel);

			genModelResource.save(Collections.EMPTY_MAP);

		} catch (IOException e) {
			e.printStackTrace();
		}
		return genModel;
	}

	public static List<GenModel> ecoreToGenModel(List<Resource> resources) {
		List<GenModel> genModels = new ArrayList<GenModel>();
		for (Resource r : resources) {
			genModels.add(ecoreToGenModel(r));
		}
		return genModels;
	}

	public static void genModelToCode(GenModel genModel, String targetDirectory, String projectName) {
		if (targetDirectory != null) {
			Resource resource = genModel.eResource();
			resource.getResourceSet().getURIConverter().getURIMap().put(
					URI.createURI("platform:/resource/" + genModel.getModelDirectory() + "/src-model"),
					URI.createFileURI(targetDirectory + "/"));
			genModel.setModelDirectory(genModel.getModelDirectory() + "/src-model");
			EcoreUtil.resolveAll(resource.getResourceSet());
		}
		genModel.setCanGenerate(true);
		genModel.reconcile();
		genModel.validate();
		// genModel.setValidateModel(true); // The more checks the better

		GeneratorAdapterFactory.Descriptor.Registry.INSTANCE.addDescriptor(GenModelPackage.eNS_URI,
				GenModelGeneratorAdapterFactory.DESCRIPTOR);
		// Create the generator and set the model-level input object.
		Generator generator = new Generator();
		generator.setInput(genModel);
		// Generator model code.
		System.out.println("generator -- " + generator + "\n genModel" + genModel);
		Diagnostic d = generator.generate(genModel, GenBaseGeneratorAdapter.MODEL_PROJECT_TYPE,
				new BasicMonitor.Printing(System.out));
		System.out.println(d.getMessage());

	}

	public static void genModelToCode(GenModel genModel, String targetDirectory) {
		if (targetDirectory != null) {
			Resource resource = genModel.eResource();
			resource.getResourceSet().getURIConverter().getURIMap()
					.put(URI.createURI("platform:/resource/TargetProject/"), URI.createFileURI(targetDirectory + "/"));
			genModel.setModelDirectory("/TargetProject");
			EcoreUtil.resolveAll(resource.getResourceSet());
		}
		genModel.setCanGenerate(true);
		genModel.reconcile();
		genModel.validate();
		// genModel.setValidateModel(true); // The more checks the better

		GeneratorAdapterFactory.Descriptor.Registry.INSTANCE.addDescriptor(GenModelPackage.eNS_URI,
				GenModelGeneratorAdapterFactory.DESCRIPTOR);
		// Create the generator and set the model-level input object.
		Generator generator = new Generator();
		generator.setInput(genModel);
		// Generator model code.
		System.out.println("generator -- " + generator + "\n genModel" + genModel);
		Diagnostic d = generator.generate(genModel, GenBaseGeneratorAdapter.MODEL_PROJECT_TYPE,
				new BasicMonitor.Printing(System.out));
		System.out.println(d.getMessage());

	}

	public static void genModelToCode2(List<GenModel> genModels, String targetDirectory) {
		for (GenModel g : genModels) {
			genModelToCode(g, targetDirectory);
		}
	}

	public static EList<InstanceSpecification> getTestSetup(org.eclipse.uml2.uml.Package pack_, String name) {
		EList<InstanceSpecification> ins = new BasicEList<InstanceSpecification>();
		org.eclipse.uml2.uml.Package setup = (org.eclipse.uml2.uml.Package) UMLUtil
				.findEObject(pack_.allOwnedElements(), new EObjectMatcher() {

					@Override
					public boolean matches(EObject eObject) {
						return (eObject instanceof org.eclipse.uml2.uml.Package)
								&& (((org.eclipse.uml2.uml.Package) eObject).getName().equals(name));
					}
				});
		for (EObject e : setup.allOwnedElements()) {
			if (e instanceof InstanceSpecification) {
				ins.add((InstanceSpecification) e);
			}
			// else{
			// System.out.println(String.format("Error %1$s is not instance
			// specification in the %2$s", e.toString(), name));
			// }
		}
		return ins;
	}

	public static EList<InstanceSpecification> getSpecifiedConfigs(org.eclipse.uml2.uml.Package pack_,
			EList<InstanceSpecification> ins) {
		for (EObject e : pack_.allOwnedElements()) {
			if (e instanceof InstanceSpecification) {
				ins.add((InstanceSpecification) e);
			}
			// else if(e instanceof org.eclipse.uml2.uml.Package){
			// getSpecifiedConfigs((org.eclipse.uml2.uml.Package)e,ins);
			// }
		}
		return ins;
	}

	public static EList<InstanceSpecification> getSpecifiedConfigs(org.eclipse.uml2.uml.Package pack_) {
		EList<InstanceSpecification> ins = new BasicEList<InstanceSpecification>();
		for (EObject e : pack_.allOwnedElements()) {
			if (e instanceof InstanceSpecification) {
				ins.add((InstanceSpecification) e);
			}
		}
		return ins;
	}

	public static EList<InstanceSpecification> getSpecifiedConfigs(org.eclipse.uml2.uml.Package pack_, String name) {
		EList<InstanceSpecification> ins = new BasicEList<InstanceSpecification>();
		org.eclipse.uml2.uml.Package setup = (org.eclipse.uml2.uml.Package) UMLUtil
				.findEObject(pack_.allOwnedElements(), new EObjectMatcher() {

					@Override
					public boolean matches(EObject eObject) {
						return (eObject instanceof org.eclipse.uml2.uml.Package)
								&& (((org.eclipse.uml2.uml.Package) eObject).getName().equals(name));
					}
				});
		for (EObject e : setup.allOwnedElements()) {
			if (e instanceof InstanceSpecification) {
				ins.add((InstanceSpecification) e);
			}
		}
		return ins;
	}

	public static String getBodyConstraint(Constraint constraint) {
		if (constraint.getSpecification() instanceof OpaqueExpression) {
			if (((OpaqueExpression) constraint.getSpecification()).getBodies().size() == 1){
				String body = ((OpaqueExpression) constraint.getSpecification()).getBodies().get(0);
				if(body.split("--").length >=2) body = body.split("--")[0];
				return body.replaceAll("\n", " ");
				}
		}
		return null;
	}

	public static String getBodyOpaqueExpression(OpaqueExpression oe) {
		return oe.getBodies().get(0);
	}

	public static EList<StateMachine> getStateMachineWithOwner(EObject object) {
		EList<StateMachine> sms = null;
		if (object instanceof Classifier) {
			Classifier clazz = (Classifier) object;
			for (EObject e : clazz.allOwnedElements()) {
				if ((e instanceof StateMachine) && ((StateMachine) e).getOwner().equals(clazz)) {
					if (sms == null)
						sms = new BasicEList<StateMachine>();
					sms.add((StateMachine) e);
				}
			}
		}
		return sms;
	}

	public static boolean checkContextOfConstraint(Constraint constraint) {
		return (constraint.getContext() instanceof org.eclipse.uml2.uml.Class
				|| constraint.getContext() instanceof Operation); // FIXME
	}

	public static UTestSet getUTestSet(EObject eObject, String name) {
		if (eObject instanceof UTestModel) {
			UTestModel pac = (UTestModel) eObject;
			Iterator<EObject> all = pac.eAllContents();
			while (all.hasNext()) {
				EObject next = all.next();
				if (next instanceof UTestSet) {
					if (((UTestSet) next).getAttachedObject() instanceof StateMachine
							&& ((StateMachine) ((UTestSet) next).getAttachedObject()).getName().equals(name)) {
						return (UTestSet) next;
					}
				}
			}
		} else if (eObject instanceof UTestSet) {
			if (((UTestSet) eObject).getAttachedObject() instanceof StateMachine
					&& ((StateMachine) ((UTestSet) eObject).getAttachedObject()).getName().equals(name)) {
				return (UTestSet) eObject;
			}
		}
		return null;
	}

	public EObject resolve(InternalEObject proxy, EObject objectContext, Resource resourceContext, ResourceSet rsSet) {
		// final URI uri = proxy.eProxyURI();
		return EcoreUtil.resolve(proxy, rsSet);
	}

}
